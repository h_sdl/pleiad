# Pleiad
You generally know in advance the tasks you want to complete in a day with electrical appliances (heating your home, your water, washing your clothes, cooking...), as well as the cost of your electrical power.
But can you make the most of this in order to save money by softening your hard operational constraints?
Let's take one example: do you really need to launch your dishwasher at 1pm after your lunch? Generally not: it may run whenever it wants between the end of your lunch and the dinner. 
By automatically letting your dishwasher choose its start time when power cost is low, you may both save money and help improve the integration of renewable energies by letting higher-priority loads run instead of your dishwasher when the offer is low.

This program could be called a "flexibility planner" or "flexibility explorer": it optimizes your power costs over a predefined horizon (eg. one day) by making the most of the "soft constraints" you define on your electrical appliances.
It also has extra features like:
* it enables to aggregate several actors and make them optimize their costs together
* it integrates solar forecasts using the pvlib library in order to optimally schedule your consumption according to the solar PV output of your equipments
* it enables to easily change the mathematical constraints of your appliancesto study model changes

Pleiad was developed by Hugues de Lavoreille, Juan Gomez Herrera and Miguel Anjos, and it is based on previous works of Luce Brotcorne, Martine Labbé and Maria I Restrepo.

The complete documentation is available [here](docs/documentation.pdf) and a comprehensive presentation given at EDF Labs (Dec. 2019) can be found [here](docs/Presentation_PGMO_Pleiad_handout.pdf)

# Installation instructions
## Requirements
You need a few things before being able to work with Pleiad

### Python
You need Python 3.7 or higher to run the code of the program. It can be downloaded and installed from https://www.python.org/downloads/
If working on a GERAD computer, Python is already installed by default.

### A solver
You need at least one of the following LP-solvers in order for the module to be able to find the optimal schedulings.
- Cplex (https://www.ibm.com/developerworks/community/blogs/jfp/entry/CPLEX_Is_Free_For_Students?lang=en)
- Gurobi (http://www.gurobi.com/)

Both of them are free for academic research. You should be able to install them using your academic e-mail address.  
  
If working on a GERAD computer, just type `module load cplex` or `module load gurobi` to load the preinstalled modules

### Connecting Python and the solvers
You need to install the Python libraries of the solvers you want to use.
The Python libraries are usually available in a directory of the solver's installation folder, it contains a `setup.py` file.
Just open a terminal, navigate to this folder and type `python setup.py install` in order to register the library.  
For instance, on a GERAD computer:  
`cd /home/ibm/cplex-studio/12.9.0.0/python`   
`python setup.py install`  
  
You should ensure you are always using the most recent versions of the solver compatible with the version of Python you are using (eg. 3.7)

### Git
You need git on your computer in order to easily download and update Pleiad. Git is a free version control software which can be downloaded at https://git-scm.com/downloads  
If working on a GERAD computer, just type `module load git` in a terminal

## First steps for configuring Pleiad using Git
In this section, we will be downloading and installing the Pleiad components using Git, and see how to switch form a version of Pleiad to another, as well as how to update it

### Installing Pleiad
You first need to open a terminal and navigate to the folder where you want to install Pleiad, using `cd` (change directory). You can create a custom directory using `mkdir`, where we will install Pleiad.  
For example:  
`cd /home/me/`  
  
We will now use Git to download Pleiad  
`git clone https://gitlab.com/h_sdl/pleiad.git`  
`cd pleiad`  
And finally register the Pleiad library for Python  
`python setup.py develop`  

### Updating Pleiad
Updating can be done by navigating to the root folder of the pleiad repository :  
`cd /home/me/pleiad/`  
And executing  
`git pull`  
  
The most recent version of Pleiad on the server will be fetched, pulled and replacing the oldest files on your computer

### Switching to another Pleiad version
The default installed version of Pleiad is the stable one, referenced as `master` on the repository. But you may want to use new functionalities which may not be in the stable branch yet. But on another branch (the list of branches is available here : https://gitlab.com/h_sdl/pleiad/-/branches)  
  
Let's assume you want to switch to branch `develop`. Two cases are possible:
- You've never been using this branch before : `git checkout --track origin/develop` to download it and switch to it
- You've already been using it: `git checkout develop` to switch to the local copy of `develop` you used last time and then `git pull` to update this copy

If you want to come back to the stable version:  
`git checkout master`  
`git pull`  

# Getting started
The best way to understand how Pleiad works is to use it! After the setup is complete, open the [pleiad_main_example.py](bin/pleiad_main_example.py) script and run it using Python to get your first network optimized!
A comprehensive description of the script is given at section 2.3 of the [documentation](docs/documentation.pdf).