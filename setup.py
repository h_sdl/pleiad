#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 15:30:02 2019

@author: Hugues Souchard de Lavoreille
"""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pleiad",
    version="0.0.1",
    author="Hugues Souchard de Lavoreille, Juan A. Gomez-Herrera, Miguel F. Anjos",
    author_email="hugues.souchard_de_lavoreille@mines-paristech.fr",
    description="PLanner for Electrical and Intelligent Appliances in Dwellings",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="http://osg.polymtl.ca",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)