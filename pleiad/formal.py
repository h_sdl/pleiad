#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 10:49:31 2019

@author: Hugues Souchard de Lavoreille
"""

import operator
from datetime import timedelta
import numpy as np
import pleiad.quantity

class Variable:
    """
    Class containing variables of the optimization problem.
    Variables can be continuous or integers, have lower and upper bounds and have names.
    Their values can also be set.
    If their values are set, they should not be modified anymore
    """
    
    def __init__(self, name, integer_or_continuous, quantity_type=float, mini=None, maxi=None):
        """
        Constructor for Variables
        Quantity types can be numeric (int/float), timedelta, or Quantities (inheriting from pleiad.quantity.Quantity)
        """
        
        # Type checking for quantity type
        if not(quantity_type in (int, float, timedelta) or issubclass(quantity_type, pleiad.quantity.Quantity)):
            raise Exception("Quantity type '" + type(quantity_type).__name__ + "' not supported")
        
        # Type checking for the variable type (continuous or integer)
        if not(integer_or_continuous in ["I", "C", "B"]): raise Exception("Variable type should be 'B', 'I' or 'C'")
        
        if integer_or_continuous == "B":
            if quantity_type not in (int, float):
                raise Exception("Variable type '" + str(quantity_type) + "' not compatible with a binary variable ('" + name + "')")
                
            if mini is None:
                mini = quantity_type(0)
            elif mini != 0:
                raise Exception("Minimum '" + str(mini) + "' not compatible with a binary variable ('" + name + "')")
               
            if maxi is None:
                maxi = quantity_type(1)
            elif maxi != 1:
                raise Exception("Minimum '" + str(maxi) + "' not compatible with a binary variable ('" + name + "')")
                
        if integer_or_continuous == "I" and quantity_type not in (int, float):
            raise Exception("Variable type '" + str(quantity_type) + "' not compatible with an integer variable ('" + name + "')")
        
        if quantity_type is float and isinstance(mini, int): mini = float(mini)
        if quantity_type is float and isinstance(maxi, int): maxi = float(maxi)
        
        # Type checking for lower and upper bounds, according to the defined quantity type 
        if mini is not None and not(isinstance(mini, quantity_type)): raise Exception(str(mini) + " should be a " + quantity_type.__name__)
        if maxi is not None and not(isinstance(maxi, quantity_type)): raise Exception(str(maxi) + " should be a " + quantity_type.__name__)
        
        # Initialization
        self.name = name
        self.integer_or_continuous = integer_or_continuous
        self.mini = mini
        self.maxi = maxi
        self.quantity_type = quantity_type
        
        self.value = None # Filled after solve
            
    def get_upper_bound_numeric(self):
        """
        Function used by the solver in order to get the upper bound of the variable, AS A NUMERIC TYPE
        Returns None if no upper bound has been set
        Similar to Constraint::get_rhs_numeric
        """
        if self.maxi is None:
            return None
        elif self.quantity_type in (int, float):
            return self.maxi
        elif self.quantity_type is timedelta:
            return float(self.maxi.total_seconds())/86400
        else:
            return self.maxi.val
        
    def get_lower_bound_numeric(self):
        """
        Function used by the solver in order to get the lower bound of the variable, AS A NUMERIC TYPE
        Returns None if no lower bound has been set
        Similar to Constraint::get_rhs_numeric
        """
        if self.mini is None:
            return None
        elif self.quantity_type in (int, float):
            return self.mini
        elif self.quantity_type is timedelta:
            return float(self.mini.total_seconds())/86400
        else:
            return self.mini.val
        
    def set_value(self, v):
        """
        Function called after the optimization problem has been solved, in order to store the found values of variables
        """
        if self.value is not None:
            raise Exception("Variable value has already been set")
        
        x = self.quantity_type(v)
        if (self.mini is not None and x < self.mini) or (self.maxi is not None and x > self.maxi):
            raise Exception("Value outside of bounds")
            
        self.value = x
        
    def __repr__(self):
        return self.name + (" = " + str(self.value) if self.value is not None else ("[" + (self.quantity_type.display_unit if self.quantity_type.str_display_unit is None else self.quantity_type.str_display_unit) + "]" if issubclass(self.quantity_type, pleiad.quantity.Quantity) else ("[d]" if self.quantity_type is timedelta else "")))
    
    # Functions transforming Variables into CoefficientVariablePair objects
    def __rmul__(self, coefficient):
        """
        Implementation of coefficient * variable
        Returns a CoefficientVariablePair
        """
        return CoefficientVariablePair(coefficient, self)
    
    def __neg__(self):
        """
        Implementation of - Variable
        Returns a CoefficientVariablePair
        """
        return (-1) * self
    
    def __pos__(self):
        """
        Implementation of + Variable
        Returns a CoefficientVariablePair
        Shortcut to create a pair composed of the Variable only, with scalar == +1
        """
        return (+1) * self
    
    
    # Functions transforming Variables directly into LeftHandSide objects
    def __add__(self, x):
        return (1 * self) + x
        
    def __sub__(self, x):
        return self + (-x)
    
    # Functions transforming Variables directly into ConstraintContent objects
    def __eq__(self, x):
        return (1 * self).__eq__(x)
    
    def __le__(self, x):
        return (1 * self).__le__(x)
    
    def __ge__(self, x):
        return (1 * self).__ge__(x)
        

class CoefficientVariablePair:
    """
    Container for tuples (coefficient * Variable)
    """
    
    def __init__(self, coefficient, variable):
        if not(isinstance(coefficient, (int, float, timedelta, pleiad.quantity.Quantity))):
            raise Exception("Coefficient type '" + type(coefficient).__name__ + "' not supported")
        self.coefficient = coefficient
        self.variable = variable
        
        self.product_type = type(self.coefficient * self.variable.quantity_type(0))
        if self.product_type is int: self.product_type = float # Considering ints are floats (easier for later comparisons)
        
        self.product_value = None

        
    def calculate_product_value(self):
        """
        Returns the type of the product of coefficient * Variable
        Uses cached version of the product value
        If nothing can be computed (eg. Variable has no value), returns None
        """
        if self.product_value is not None:
            return self.product_value
        
        if self.variable.value is not None:
            self.product_value = self.coefficient * self.variable.value
        return self.product_value
    
    value = property(calculate_product_value)
        
    def get_coefficient_numeric(self):
        """
        Returns the numerical value of the coefficient associated to the Variable in the appropriate unit
        such that the numerical value found for the variable is equal to the value of the variable in its default unit
        
        Example:
            if the equation is 2 [$/Mwh] * E [kWh] = 5 [c$]
            then the equation can be rewritten as 0.002 [$/kWh]  * E [kWh] = 5 [c$]
                                   and finally as 0.2   [c$/kWh] * E [kWh] = 5 [c$]
            and the current function returns 0.2
        
        Idea:
            let U, V, W be 3 compatible quantities units such that the product U * V = W is possible (eg. P * T = E)
            let u, v, w be 3 units for these quantities, such that (w/(u*v)) * u * v = w where l = (w/(u*v)) is the conversion factor to make the formula valid
            
            if x is a value of U and z is a value of W, one can compute y (the corresponding value of V using the formula) :
                y = w / u / l
            one can also compute a naive value of y (let's call it ywrong) forgetting the conversion factor :
                ywrong = w / u
                
            in order to find l, one just needs to compute two instances of y and ywrong from the sames u and w before applying the following formula :
                l = ywrong / y
            
            In the following code, x = 1 [u] and z = 1 [w]
        """
        if type(self.coefficient) in (int, float):
            return self.coefficient
        elif type(self.coefficient) is timedelta:
            multiplier = 1#self.variable.quantity_type(1) / (self.product_type(1) / type(self.coefficient)(1))
            return multiplier * self.coefficient.total_seconds() / 86400
        else:
            multiplier = 1# self.variable.quantity_type(1) / (self.product_type(1) / type(self.coefficient)(1))
            return multiplier * self.coefficient.val
        
    def __repr__(self):
        val_coeff = self.coefficient.val if isinstance(self.coefficient, pleiad.quantity.Quantity) else (self.coefficient.total_seconds() if type(self.coefficient) is timedelta else self.coefficient)
        sign = " + " if val_coeff >= 0 else " - "
        return sign + str(abs(self.coefficient)) + " * " + self.variable.__repr__()
    
    # Functions acting on the CoefficientVariablePair object
    def __rmul__(self, x):
        """
        Implementation of coefficient * CoefficientVariablePair
        Returns a CoefficientVariablePair
        """
        return CoefficientVariablePair(x * self.coefficient, self.variable)
    
    def __neg__(self):
        return (-1) * self
    
    # Function implicitely converting to LeftHandSide objects
    def __add__(self, x):
        return LeftHandSide(pairs=[self]).__add__(x)

    def __sub__(self, x):
        return LeftHandSide(pairs=[self]).__sub__(x)
        
    # Functions converting to ConstraintContent objects
    def __eq__(self, x):
        return LeftHandSide(pairs=[self]).__eq__(x)
        
    def __le__(self, x):
        return LeftHandSide(pairs=[self]).__le__(x)
    
    def __ge__(self, x):
        return LeftHandSide(pairs=[self]).__ge__(x)
        
    

class LeftHandSide:
    """
    Container for linear combinations of variables
    """
    
    def __init__(self, coefficients = [], variables = [], pairs = []):
        if len(coefficients) != len(variables):
            raise Exception("Failure while initializing left-hand side object (not the same number of coefficients and variables)")
        
        if len(coefficients) > 0 and len(pairs) > 0:
            raise Exception("Failure while initializing left-hand side object (cannot initialize with pairs and lists at the same time)")
        
        self.coefficients = []
        self.variables = []
        self.pairs = []
        self.quantity_type = None
        
        self.lhs_value = None
        
        if len(coefficients) > 0: pairs = [coefficients[i] * variables[i] for i in range(len(coefficients))]
        for p in pairs: self.add_pair(p)
    
    def copy(self):
        x = LeftHandSide()
        x.quantity_type = self.quantity_type
        x.pairs = self.pairs.copy() # Copy but not deep copy because we want to keep the variables and coefficients but be able to change the list
        x.coefficients = self.coefficients.copy()
        x.variables = self.variables.copy()
        
        return x
    
    def check_compatibility_pair(self, pair):
        """
        Checks whether a given pair is compatible with the current LHS object
        """
        
        if self.quantity_type is not None:
            q_type = pair.product_type
            if self.quantity_type is not q_type:
                raise Exception("(" + pair.__repr__() + ") is of type " + q_type.__name__ + " whereas LHS object is of type " + self.quantity_type.__name__ + ". You should consider checking the dimensionality of your equations.")
        
    def add_pair(self, pair):
        """
        Adds the given pair of coefficient/Variable to the current LHS object
        """
        if isinstance(pair, Variable): pair = 1 * pair
        if not(isinstance(pair, CoefficientVariablePair)): raise Exception("Can only add CoefficientVariablePair objects")
        self.check_compatibility_pair(pair)
        if self.quantity_type is None:
            self.quantity_type = pair.product_type
        self.pairs.append(pair)
        self.coefficients.append(pair.coefficient)
        self.variables.append(pair.variable)
    
    def add_lhs(self, o_lhs):
        """
        Combines the current LHS with another LHS
        """
        if len(o_lhs) == 0:
            return
        elif len(o_lhs) == 1:
            self.add_pair(o_lhs.pairs[0])
        else:
            # TODO Could be integrated with the current add_pair function ? Generalize or LeftHandSide(pairs=[pair])
            self.check_compatibility_pair(o_lhs.pairs[0])
            if self.quantity_type is None:
                self.quantity_type = o_lhs.quantity_type
            self.pairs.extend(o_lhs.pairs)
            self.coefficients.extend(o_lhs.coefficients)
            self.variables.extend(o_lhs.variables)
    
    def calculate_lhs_value(self):
        """
        Returns the type of the sum contained in the lhs
        Uses cached version of the lhs sum value if it exists
        If nothing can be computed (eg. Variables have no value), returns None
        """
        if self.lhs_value is not None:
            return self.lhs_value
        
        v = self.quantity_type(0)
        
        for p in self.pairs:
            pv = p.calculate_product_value()
            if pv is None:
                return None
            v += pv
            
        self.lhs_value = v
        return self.lhs_value
    value = property(calculate_lhs_value)
    
    def __len__(self):
        return len(self.pairs)
    
    def __add__(self, x):
        """
        Addition of an object to the current LeftHandSide object (using the in-place addition because we usually do not want to reuse the original LHS object afterwards)
        """
        y = self.copy()
        y += x
        return y
    
    def __iadd__(self, x):
        """
        Adds a pair to the current LHS object in place
        """
        if isinstance(x, LeftHandSide):
            self.add_lhs(x)
            return self
        elif isinstance(x, CoefficientVariablePair):
            self.add_pair(x)
            return self
        elif isinstance(x, Variable):
            self.add_pair(1 * x)
            return self
        else:
            return NotImplemented
        
    def __sub__(self, x):
        """
        Subtraction of an object to the current LeftHandSide object (using the in-place addition because we usually do not want to reuse the original LHS object afterwards)
        """
        return self.__add__(-x)
        
    def __isub__(self, x):
        """
        Adds a pair to the current LHS object in place
        """
        return self.__iadd__(-x)
    
    def __neg__(self):
        """
        Negates the current LHS
        """
        x = LeftHandSide()
        x.quantity_type = self.quantity_type
        x.pairs = [-p for p in self.pairs]
        x.coefficients = [-c for c in self.coefficients]
        x.variables = self.variables.copy()
        return x
    
    def __repr__(self):
        s = ""
        for p in self.pairs:
            s += p.__repr__()
        return s
    
    def __eq__(self, x):
        if isinstance(x, (int, float, timedelta, pleiad.quantity.Quantity)):
            return Constraint(constraint_type="E", lhs=self, rhs=x)
        else:
            # Error because otherwise (with NotImplemented), Python tries to compare the objects using the usual '=='
            raise NotImplementedError()
        
    def __le__(self, x):
        if isinstance(x, (int, float, timedelta, pleiad.quantity.Quantity)):
            return Constraint(constraint_type="L", lhs=self, rhs=x)
        else:
            return NotImplemented
    
    def __ge__(self, x):
        if isinstance(x, (int, float, timedelta, pleiad.quantity.Quantity)):
            return Constraint(constraint_type="G", lhs=self, rhs=x)
        else:
            return NotImplemented
        
class Constraint:
    """
    Numerical content of a constraint (excluding description, name, ...)
    Easily created with shorthand notations like "v <= 5"
    """
    
    counter = 0
    
    def __init__(self, constraint_type, lhs, rhs, name=None):
        # Checking constraint type, LHS type and RHS type
        if not(constraint_type in ["E", "L", "G"]): raise Exception("Constraint type should be 'E', 'L' or 'G'")
        if not(isinstance(lhs, LeftHandSide)): raise Exception("Left hand side type should be 'LeftHandSide'")
        if not(isinstance(rhs, (int, float, timedelta, pleiad.quantity.Quantity))): raise Exception("Right hand side type should be int, float, timedelta or Quantity")
        
        self.id = Constraint.counter
        Constraint.counter += 1
        
        # Checking dimensionality of LHS and RHS
        typerhs = float if type(rhs) is int else type(rhs) # Changing int to float in order to have only one scalar type (float)
        if lhs.quantity_type is None: # If no quantity type for LHS has been set, set the quantity type of the RHS
            lhs.quantity_type = typerhs
        if lhs.quantity_type is not typerhs: raise Exception("LHS and RHS are dimensionally incompatible")
        
        self.constraint_type = constraint_type
        self.lhs = lhs
        self.rhs = rhs
        self.name = name if name is not None else "Constraint #" + str(self.id)
        
    def __repr__(self):
        ctypes = {"E":"=", "G":">=", "L":"<="}
        return self.lhs.__repr__() + " " + ctypes[self.constraint_type] + " " + self.rhs.__repr__() + "  (" + self.name + ")"
    
    def __mod__(self, name):
        """
        Overloaded operator to name the constraint
        """
        self.name = name
        return self
    
    def get_rhs_numeric(self):
        """
        Function used to access the unitless numerical value of the RHS, used for the computation.
        Similar to Variable::get_upper_bound_numeric and Variable::get_lower_bound_numeric
        """
        
        if type(self.rhs) in (int, float):
            return self.rhs
        elif type(self.rhs) is timedelta:
            return float(self.rhs.total_seconds())/86400
        else:
            return self.rhs.val

    
class FormalArray:
    """
    Data structure enabling symbolic calculations with 2-dimensional matrices of variables
    """
    
    def __init__(self, h, w, nterms):
        """
        h : number of rows
        w : number of columns
        nterms : number of symbolic variables in every cell of the matrix (eg. 2 for "2x + 2y", 1 for "z")
        """
        self.h = h
        self.w = w
        self.nterms = nterms
        self.retrieved = None # Flag telling whether the content of the formal araray has already been computed and cached
        self.lhs = None # Flag telling whether the lhs (sum of terms) has already been computed and cached
        if type(self) == FormalArray:
            raise Exception("FormalArray type should not be initialized")
        
    def transpose(self):
        """
        Transpose matrix of self
        """
        return Transpose(self)
    T = property(transpose)
        
    def dot(self, other):
        """
        Matrix-wise product of two FormalArrays
        """
        return DotProduct(self, other)
    
    def __add__(self, other):
        """
        Element-wise sum
        """
        return Sum(self, other)
    
    def __sub__(self, other):
        """
        Element-wise subtraction
        """
        return Subtraction(self, other)
    
    def __mul__(self, other):
        """
        Element-wise multiplication
        """
        return Product(self, other)
    
    def __getitem__(self, args):
        """
        Slicing on arrays
        """
        return Extraction(self, args)
    
    def sum(self, axis=(0,1)):
        """
        Sum along the requested axes
        """
        return ArraySum(self, axis)
    
    def shape(self):
        """
        Returns (number of rows, number of columns)
        """
        return (self.h, self.w)
    
    def __rmul__(self, l):
        """
        Multiplication by a scalar (int, float, timedelta or Quantity)
        """
        return ScalarProduct(self, l)
    
    def __neg__(self):
        """
        Opposite of self
        """
        return ScalarProduct(self, -1)
    
    def __eq__(self, rhs):
        """
        Builds equality constraints from self and rhs
        """
        return self.compare(rhs, operator.eq)
    
    def __le__(self, rhs):
        """
        Builds <= inequality constraints from self and rhs
        """
        return self.compare(rhs, operator.le)
    
    def __ge__(self, rhs):
        """
        Builds >= inequality constraints from self and rhs
        """
        return self.compare(rhs, operator.ge)
    

    def compare(self, rhs, op):
        """
        Compares each element of self with the corresponding element of rhs, using the corresponding operator in op
        If op is not an Array, the same operator is used for every comparison
        If rhs is not an Array, the same right-hand side is used for every comparison
        Otherwise, self, rhs and op should have the same size
        """
        
        # Checks whether lhs already built (ie. lhs = sum of coefficient-variable pairs)
        if self.lhs is None:
            self.lhs = self.build()
        
        # Checks dimensions of Arrays if rhs or op are actually Arrays
        if isinstance(rhs, FormalArray):
            rhs_r = rhs.retrieve()
            if not(self.h == rhs.h and self.w == rhs.w): raise Exception("Dimension error")
        if isinstance(op, FormalArray):
            op_r = op.retrieve()
            if not(self.h == op.h and self.w == op.w): raise Exception("Dimension error")
        
        # If rhs is array
        if isinstance(rhs, FormalArray):
            # And if op is Array
            if isinstance(op, FormalArray):
                # Then element-wise constraints from comparisons
                return ConstraintArray([op_r[i][j][0](self.lhs[i][j], rhs_r[i][j][0]) for j in range(self.w)] for i in range(self.h))
            # Else element-wise constraints with the same operator
            return ConstraintArray([op(self.lhs[i][j], rhs_r[i][j][0]) for j in range(self.w)] for i in range(self.h))
        # If only op is Array
        if isinstance(op, FormalArray):
            # then element-wise constraints with the same rhs
            return ConstraintArray([op_r[i][j][0](self.lhs[i][j], rhs) for j in range(self.w)] for i in range(self.h))
        # Else constraints with the same rhs and operators
        return ConstraintArray([op(self.lhs[i][j], rhs) for j in range(self.w)] for i in range(self.h))
    
    def build(self):
        """
        Assembles LHS from coefficient-variable pairs without using the + and - operators but only add_pair for performance reasons
        """
        
        # Checks whether already retrieved (ie. the coefficient-variable pairs have been computed)
        if self.retrieved is None:
            self.retrieved = self.retrieve()
        
        constraints = [[LeftHandSide() for j in range(self.w)] for i in range(self.h)]
        for i in range(self.h):
            for j in range(self.w):
                for p in self.retrieved[i][j]:
                    constraints[i][j].add_pair(p)
        
        return constraints

class ConstraintArray(list):
    """
    Special type of lists designed to contain constraints generated from an Array.
    It is natively 2-dimensional
    """
    
    def __mod__(self, names):
        """
        Function used to name constraints of the current ConstraintArray
        """
        
        # Detecting sizes
        shape_names = np.shape(names)
        shape_csts = np.shape(self)
        
        # If the 2 arguments are 2-dimensional: size check and element-wise naming
        if len(shape_names) == 2 and shape_names == shape_csts:
            for i in range(shape_csts[0]):
                for j in range(shape_csts[1]):
                    self[i][j] % names[i][j]
        # If the constraint array is 1 dimensional and the name list too
        elif len(shape_names) == 1 and shape_csts[0] == 1 and shape_csts[1] == shape_names[0]:
            for i in range(shape_names[0]):
                self[0][i] % names[i]
        elif len(shape_names) == 1 and shape_csts[1] == 1 and shape_csts[0] == shape_names[0]:
            for i in range(shape_names[0]):
                self[i][0] % names[i]
        # If they are 1x1
        elif len(shape_names) == 0 and shape_csts == (1,1):
            self[0][0] % names
        else:
            raise Exception()
        return self
    
    def flatten(self):
        """
        Function used to get a 1-dimensional list of all the constraints
        """
        return [v for row in self for v in row]

class Array(FormalArray):
    """
    Container for 2-dimensional lists of Variables or Scalars (int/float/timedelta/Quantity)
    """
    def __init__(self, content=None):
        """
        Arrays can be initialized with:
            - scalars (int, float, Quantity) -> will be of size 1x1
            - lists of those -> will be of size len(list)x1
            - lists of lists of those -> same size
            
        """
        if isinstance(content, (int, float, timedelta, pleiad.quantity.Quantity, pleiad.formal.Variable)):
            content = [[content]]
            h = 1
            w = 1
        elif isinstance(content, list):
            h = len(content)
            types = set([type(x) for x in content])
            if len(types) == 0 or len(types) > 2 or (len(types) == 2 and types != set({int, float})):
                raise Exception("Wrong types in array")
                
            # List of lists
            if types == set({list}):
                w = len(content[0])
                if w == 0:
                    raise Exception("Empty array")
                for row in content:
                    if len(row) != w: raise Exception("Non-rectangular array")
                    
            # List of scalars
            else:
                content = [[x] for x in content]
                w = 1
        elif content is None:
            content = [[]]
            h = 0
            w = 0
        else:
            raise Exception("Wrong types in array")
        super().__init__(h, w, 1)
        
        self.content = [[v for v in row] for row in content]
        
    def __repr__(self):
        """
        Only displays one element because otherwise, the output could be enormous
        """
        return ("[[]]" if self.w == 0 and self.h == 0 else "[[" + self.content[0][0].__repr__() + ", ...], ...]")
    
    def retrieve(self):
        """
        Retrieving a scalar Array (each element containing only one term) means returning a list of lists of lists, each last list being of size 1 and containing the element at the same position
        """
        return [[[v] for v in row] for row in self.content]
    
    def flatten(self):
        """
        Function used to get all the elements in the Array, whatever order they may have
        """
        return [v for row in self.content for v in row]
    
    def get(self, *args):
        """
        Function used to return the element of self at the requested position
        args may be :
            int --> self[1][i]/self[i][1] if one of the dimensions of self is 1 or the row self[i] otherwise
            (int, int) --> self[i][j]
        """
        if type(args) is int:
            args = (args,)
        if len(args) == 1 and self.w > 1 and self.h > 1:
            return self.content[args[0]]
        elif len(args) == 1 and self.w == 1:
            return self.content[args[0]][0]
        elif len(args) == 1 and self.h == 1:
            return self.content[0][args[0]]
        else:
            return self.content[args[0]][args[1]]
        
    def concat(self, other, axis=0):
        if self.h == 0 and self.w == 0:
            return other
        if other.h == 0 and other.w == 0:
            return self
        if axis == 0:
            if self.w != other.w:
                raise Exception("Dimensions are incompatible")
            return Array(self.content + other.content)
        elif axis == 1:
            if self.h != other.h:
                raise Exception("Dimensions are incompatible")
            
            n = []
            for i in range(self.h):
                n.append([])
                for j in range(self.w):
                    n[-1].append(self.content[i][j])
                for j in range(other.w):
                    n[-1].append(other.content[i][j])
                    
            return Array(n)
            
        else:
            raise Exception("Unknown axis for concatenation")

class ArraySum(FormalArray):
    """
    Symbolic representation of the sum of an array along requested axes
    Example :
        if A = [1 2]
               [3 4]
               
    A.sum(0) will be [4 6]
    A.sum(1) will be [3]
                     [7]
    A.sum((0,1)) will be [10]
    """
    def __init__(self, x, ax=(0,1)):
        if ax not in [0, 1, (0,1), (1,0), (0,), (1,)]: raise Exception("Axis not understood")
        if ax == 0 or ax == (0,):
            h = 1
            w = x.w
            self.x = x
            self.ax = ax
            super().__init__(h, w, x.nterms * x.h)
        elif ax == 1 or ax == (1,):
            w = 1
            h = x.h
            self.x = x
            self.ax = ax
            super().__init__(h, w, x.nterms * x.w)
        elif ax == (0,1):
            w = 1
            h = 1
            self.x = x
            self.ax = ax
            super().__init__(h, w, x.nterms * x.w * x.h)
        
    def __repr__(self):
        return "SUM(" + self.x.__repr__() + ")"
            
    def retrieve(self):
        """
        Going down the tree and assembling results to get a list of lists of lists of coefficient-variable pairs whose sum will be the value of retrieved[i][j]
        """
        if self.retrieved is None:
            rx = self.x.retrieve()
            if self.ax == 0 or self.ax == (0,):
                self.retrieved = [[[k for i in range(self.x.h) for k in rx[i][j]] for j in range(self.x.w)]]
            elif self.ax == 1 or self.ax == (1,):
                self.retrieved = [[[k for j in range(self.x.w) for k in rx[i][j]]] for i in range(self.x.h)]
            else:
                self.retrieved = [[[k for j in range(self.x.w) for i in range(self.x.h) for k in rx[i][j]]]]
        return self.retrieved
        
class Extraction(FormalArray):
    """
    Symbolic representation of Array slicing/subscription
    Example:
        if A = [1 2]
               [3 4]
        then A[1:,:] is [3 4]
    """
    def __init__(self, x, args):
        # Only one argument : creating a second one to treat it as a 2d slicing
        if isinstance(args, (slice, int)):
            sl1 = args
            sl2 = slice(None, None, None)
        elif len(args) == 2:
            sl1 = args[0]
            sl2 = args[1]
        else:
            raise Exception("Invalid slice")
            
        # If indexes and not slices, converting to (i,i+1) slices
        # Particular case for -1
        if not(isinstance(sl1, slice)): sl1 = slice(sl1, sl1 + 1, 1) if sl1 != -1 else slice(sl1, None, 1)
        if not(isinstance(sl2, slice)): sl2 = slice(sl2, sl2 + 1, 1) if sl2 != -1 else slice(sl2, None, 1)
        
        # Extracting indices from slice
        self.x = x
        self.slh = sl1.indices(x.h)
        self.slw = sl2.indices(x.w)
        h = len(range(*self.slh))
        w = len(range(*self.slw))
        
        super().__init__(h, w, x.nterms)
        
    def __repr__(self):
        return self.x.__repr__() + "[" + self.repr_slice(self.slh) + "," + self.repr_slice(self.slw) + "]"
    
    def repr_slice(self, sl):
        """
        Shortcut function for representing slices
        """
        s = str(sl[0]) + ":" + str(sl[1]) + ":" + str(sl[2])
        return s
    
    def retrieve(self):
        """
        Going down the tree and assembling results to get a list of lists of lists of coefficient-variable pairs whose sum will be the value of retrieved[i][j]
        """
        if self.retrieved is None:
            rx = self.x.retrieve()
            self.retrieved = [k[self.slw[0]:self.slw[1]:self.slw[2]] for k in rx[self.slh[0]:self.slh[1]:self.slh[2]]]
        return self.retrieved

class ScalarProduct(FormalArray):
    """
    Symbolic representation of the product of self by a scalar (int, float, timedelta or Quantity)
    """
    def __init__(self, x, l):
        # Simply storing the array and the scalar
        self.x = x
        self.l = l
        super().__init__(self.x.h, self.x.w, self.x.nterms)
        
    def __repr__(self):
        return "(" + str(self.l) + ") * " + self.x.__repr__()
    
    def retrieve(self):
        """
        Going down the tree and assembling results to get a list of lists of lists of coefficient-variable pairs whose sum will be the value of retrieved[i][j]
        """
        if self.retrieved is None:
            rx = self.x.retrieve()
            self.retrieved = [[[self.l * w for w in rx[i][j]] for j in range(self.w)] for i in range(self.h)]
            
        return self.retrieved
    
class DotProduct(FormalArray):
    """
    Matrix-wise product of two arrays
    """
    def __init__(self, a, b):
        # Must be compatible
        if a.w == b.h and a.nterms == 1:
            h = a.h
            w = b.w
            self.a = a
            self.b = b
            self.d = a.w
            super().__init__(h, w, b.nterms * self.d)
        else:
            raise Exception("Incompatible dimensions")
            
    def __repr__(self):
        return self.a.__repr__() + "·" + self.b.__repr__()
    
    def retrieve(self):
        """
        Going down the tree and assembling results to get a list of lists of lists of coefficient-variable pairs whose sum will be the value of retrieved[i][j]
        """
        if self.retrieved is None:
            ra = self.a.retrieve()
            rb = self.b.retrieve()
            
            self.retrieved = [[[ra[i][k][0] * w for k in range(self.d) for w in rb[k][j]] for j in range(self.w)] for i in range(self.h)]
        return self.retrieved

class Transpose(FormalArray):
    """
    Symbolic representation of the transpose of an array
    """
    def __init__(self, x):
        self.x = x
        super().__init__(x.w, x.h, x.nterms)
        
    def __repr__(self):
        return "(" + self.x.__repr__() + ")T"
    
    def retrieve(self):
        """
        Going down the tree and assembling results to get a list of lists of lists of coefficient-variable pairs whose sum will be the value of retrieved[i][j]
        """
        if self.retrieved is None:
            rx = self.x.retrieve()
            self.retrieved = [[rx[j][i] for j in range(self.w)] for i in range(self.h)]
        return self.retrieved
        
class Product(FormalArray):
    """
    Symbolic representation of the element-wise product of two arrays
    """
    def __init__(self, a, b):
        # Must be compatible and have only one term (else very hard to make a product because we have linear problems and several elements, for whose we need to know the type (scalar or variable))
        if a.w == b.w and a.h == b.h and a.nterms == 1:
            self.a = a
            self.b = b
            super().__init__(a.h, a.w, b.nterms)
        else:
            raise Exception("Incompatible dimensions")
            
    def __repr__(self):
        return self.a.__repr__() + "*" + self.b.__repr__()
    
    def retrieve(self):
        """
        Going down the tree and assembling results to get a list of lists of lists of coefficient-variable pairs whose sum will be the value of retrieved[i][j]
        """
        if self.retrieved is None:
            ra = self.a.retrieve()
            rb = self.b.retrieve()
            
            self.retrieved = [[[ra[i][j][0] * w for w in rb[i][j]] for j in range(self.w)] for i in range(self.h)]
        return self.retrieved
            
class Sum(FormalArray):
    """
    Symbolic representation of the element-wise sum of two arrays
    """
    def __init__(self, a, b):
        # Must be compatible
        if a.w == b.w and a.h == b.h:
            self.a = a
            self.b = b
            super().__init__(a.h, a.w, a.nterms + b.nterms)
        else:
            raise Exception("Incompatible dimensions")
            
    def __repr__(self):
        return self.a.__repr__() + "+" + self.b.__repr__()
    
    def retrieve(self):
        """
        Going down the tree and assembling results to get a list of lists of lists of coefficient-variable pairs whose sum will be the value of retrieved[i][j]
        """
        if self.retrieved is None:
            ra = self.a.retrieve()
            rb = self.b.retrieve()
            self.retrieved = [[ra[i][j] + rb[i][j] for j in range(self.w)] for i in range(self.h)]
        return self.retrieved
    
class Subtraction(FormalArray):
    """
    Symbolic representation of the element-wise subtraction of two arrays
    """
    def __init__(self, a, b):
        # Mist be compatible
        if a.w == b.w and a.h == b.h:
            self.a = a
            self.b = b
            super().__init__(a.h, a.w, a.nterms + b.nterms)
        else:
            raise Exception("Incompatible dimensions")
            
    def __repr__(self):
        return self.a.__repr__() + "-" + self.b.__repr__()
    
    def retrieve(self):
        """
        Going down the tree and assembling results to get a list of lists of lists of coefficient-variable pairs whose sum will be the value of retrieved[i][j]
        """
        if self.retrieved is None:
            ra = self.a.retrieve()
            rb = self.b.retrieve()
            self.retrieved = [[ra[i][j] + [-w for w in rb[i][j]] for j in range(self.w)] for i in range(self.h)]
        return self.retrieved