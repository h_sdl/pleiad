#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 17:03:54 2019

@author: Hugues Souchard de Lavoreille
"""

class TaskOverflowWarning(Warning): pass
class WrongArgumentError(Exception): pass
class ModelNotSolvedError(Exception): pass
class EmptyTaskError(Exception): pass
class ImpossibleTaskError(Exception): pass