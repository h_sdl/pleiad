#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 6 10:40:11 2019

@author: Hugues Souchard de Lavoreille
"""

#import datetime
import pandas as pd
import pleiad.quantity
from datetime import timedelta

class PriceStructure:
    """
    Superclass of price structure
    The minimum elements of a subclass are written above in this class sketch
    """
    
    def pricing_function(self, t):
        """
        This function should return a tuple of energyprices at time t
        """
        pass
    
    def get_prices_vect(self, ts):
        """
        This function should return a pandas dataframe of energyprices (one column per tariff)
        and one line per t in ts
        """
        pass

class TLOUPriceStructure(PriceStructure):
    """
    Time and Level of Use structure
    This structure embeds two TOU price structures (a low tariff and a high tariff) and a power limit.
    The power consumed under the power limit will be charged at the low tariff
    and the power excess (over p_lim) will be charged at the high tariff
    
    For example if one has p_lim = 3kW, price_low(t) = 1$/kWh and price_high(t) = 2$/kWh
    if one has a steady consumption of 4kW during 1 hour, 3kWh will be charged at low tariff 
    (3$ * 1kWh = 3$) and the last kWh will be charged at high price (2$ * 1kWh = 2$)
    """
    
    def __init__(self, pricing_function_low, pricing_function_high, p_lim):
        self.pricing_function_low = pricing_function_low
        self.pricing_function_high = pricing_function_high
        self.p_lim = p_lim
    
    def pricing_function(self, t):
        """
        Returns a tuple of the low and high tariffs at time t
        """
        return (self.pricing_function_low(t), self.pricing_function_high(t))
    
    def get_prices_vect(self, ts):
        """
        Returns a dataframe containing the electricity prices of each level at several times t in ts
        """
        d = pd.DataFrame(columns=["low TLOU", "high TLOU"], index=ts)
        for t in ts:
            (a,b) = self.pricing_function(t)
            d.loc[t, "low TLOU"] = a
            d.loc[t, "high TLOU"] = b
        return d    
    
    def __str__(self):
        """
        Returns a string representation of the price structure
        """
        return "TLOU price structure"
    
class TOUPriceStructure(PriceStructure):
    """
    Time of Use structure
    This structure features an electricity price that varies with time.
    A kWh consumed at 7am in peak time will be charged more than a kWh consumed at 2am
    """
    
    def __init__(self, pricing):
        self.pricing = pricing
        
    def pricing_function(self, t):
        """
        Returns a tuple containing the electricity price at time t
        """
        return (self.pricing(t),)
    
    def get_prices_vect(self, ts):
        """
        Returns a dataframe containing the electricity prices at several times t in ts
        """
        d = pd.DataFrame(columns=["TOU"], index=ts)
        for t in ts:
            (a,) = self.pricing_function(t)
            d.loc[t, "TOU"] = a
        return d
    
    def __str__(self):
        """
        Returns a string representation of the price structure
        """
        return "TOU price structure"
    
class ConstantPriceStructure(PriceStructure):
    """
    Price structure which allows power trade at constant price (eg. 1$/kWh) 
    """
    
    def __init__(self, price):
        self.price = price
    
    def pricing_function(self, t):
        """
        Returns a tuple containing the electricity price at time t
        """
        return (self.price,)
    
    def get_prices_vect(self, ts):
        """
        Returns a dataframe containing the electricity prices at several times t in ts
        """
        d = pd.DataFrame(columns=["constant"], index=ts)
        for t in ts:
            (a,) = self.pricing_function(t)
            d.loc[t, "constant"] = a
        return d
    
    def __str__(self):
        """
        Returns a string representation of the price structure
        """
        return "Constant price structure"
        
class EmptyPriceStructure:
    """
    Price structure which does not allow any exchange: no tariff is defined -> no exchange
    """
    
    def pricing_function(self, t):
        """
        Returns a tuple containing the electricity price at time t
        Here an empty tuple (no price)
        """
        return ()
    
    def get_prices_vect(self, ts):
        """
        Returns a dataframe containing the electricity prices at several times t in ts
        Here an empty dataframe because there is no price
        """
        return pd.DataFrame()
    
    def __str__(self):
        """
        Returns a string representation of the price structure
        """
        return "Empty price structure"
    
class BlockPriceStructure:
    """
    To be written
    """
    
    def __init__(self, _amount, _p1, _p2, _duration):
        self.amount = _amount
        self.price1 = _p1
        self.price2 = _p2
        self.duration = timedelta(days = 1)
        
        if _duration is not None: raise NotImplementedError()
        
    def get_prices_vect(self, ts):
        """
        Returns a dataframe containing the electricity prices at several times t in ts
        Here an empty dataframe because there is no price
        """
        d = pd.DataFrame(columns=["below " + str(self.amount), "above " + str(self.amount)], index=ts)
        for t in ts:
            d.loc[t, "below " + str(self.amount)] = self.price1
            d.loc[t, "above " + str(self.amount)] = self.price2
        return d    
