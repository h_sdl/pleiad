#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 11:19:38 2019

@author: Hugues Souchard de Lavoreille
"""

#import importlib
import datetime
import warnings
import pleiad.error

class ConnectableTask:
    """
    Container for tasks of connectables
    """
    
    counter = 0 # Number of tasks already created
    entities = [] # Storage of these tasks
    
    def __init__(self, connectable, start, end):
        self.connectable = connectable
        self.start = None
        self.end = None
        self.set_start_end(start, end)
        
        # Updating counter and storing current object
        self.id = ConnectableTask.counter
        ConnectableTask.counter += 1
        ConnectableTask.entities.append(self)
        
    def set_start_end(self, start=None, end=None):
        if start is not None: self.start = start
        if end is not None: self.end = end
        if self.start is not None and self.end is not None and self.start == self.end:
            raise pleiad.error.EmptyTaskError()
        if self.start is not None and self.end is not None and self.start > self.end:
            raise pleiad.error.ImpossibleTaskError()
    
    @staticmethod
    def parse_start_end(x):
        # Extracts starting and ending dates
        if x == "":
            return (None,None)
        times = x.split("/")
        start = datetime.datetime.strptime(times[0], "%Y-%m-%dT%H:%M")
        end = datetime.datetime.strptime(times[1], "%Y-%m-%dT%H:%M")
        return (start, end)
    
    def concerned_by_snapshot_at(self, t):
        return (self.start is None or self.start <= t) and (self.end is None or t <= self.end)
    
    def __str_time_range__(self):
        return (self.start.strftime("%Y-%m-%dT%H:%M") if not(self.start is None) else "-") + "/" + (self.end.strftime("%Y-%m-%dT%H:%M") if not(self.end is None) else "-")

class Connectable:
    """
    Container for connectable objects (Appliances, Storages, Power sources)
    """
    
    counter = 0 # Number of created connectables
    entities = [] # All created connectables : entities[c.id] = c
    names = {} # Dictionary associating a connectable's id to a connectable name
    
    def __init__(self, name):
        if name in Connectable.names.keys():
            raise Exception("This name has already been chosen (names must be unique)")
        if name == "datetime":
            raise Exception("datetime is a reserved name and cannot be chosen for a connectable name")
        
        self.id = Connectable.counter
        Connectable.counter += 1
        Connectable.entities.append(self)
        
        self.name = name
        Connectable.names[name] = self.id
        
        self.parent = None
        self.last_task = None # Last added task
        
        self.tasks = [] # Tasks of the connectable object
    
    def get_tasks_and_bounds_inside_ts(self, ts):
        """
        Returning all the tasks that have at least a small instant contained in ts
        Checks for compatibility errors
        """
        
        selected_tasks = []
        for task in self.tasks:
            if task.start is None:
                # In this case, the task's start date should be the simulation's start date
                task.set_start_end(ts.start, None)
            if task.end is None:
                # In this case, the task's end date should be the simulation's end date
                task.set_start_end(None, ts.end)
            
            task.check_compatibility_with_ts(ts) # Checks that the temporal resolution allows to handle all the input datetime objects
            
            # If completely oustide the simulation's horizon, we skip the task
            if (task.start >= ts.start and task.start < ts.end) or (task.end > ts.start and task.end <= ts.end):
                if task.start < ts.start or task.end > ts.end:
                    # If not completely inside... we skip it but warn
                    warnings.warn("Connectable '" + self.name + "' operates partly (" + task.__str_time_range__() + ") outside of time horizon (" + ts.__str_time_range__() + "). It was thus removed. To avoid this, change simulation horizon.", pleiad.error.TaskOverflowWarning)
                else:
                    # Else we store it
                    selected_tasks.append(task)
        
        return selected_tasks
    
    @staticmethod
    def get(cid):
        """
        Returns the connectable with unique id=cid
        """
        return Connectable.entities[cid]
    
    def find_task_for_snapshot_at(self, t):
        """
        Fetches and returns the task(s) at time t if there is any
        """
        selected_tasks = []
        for task in self.tasks:
            if task.concerned_by_snapshot_at(t):
                selected_tasks.append(task)
        if len(selected_tasks) > 1: raise Exception() # Must be handled # TODO
        return selected_tasks
    
#    def load_snapshot(self, snapshot):
#        """
#        Dispatches the snapshot to the right tasks for loading
#        """
#        t = datetime.datetime.strptime(snapshot["datetime"], "%Y-%m-%dT%H:%M")
#        concerned = self.find_task_for_snapshot_at(t)
#        if len(concerned) > 0:
#            if self.name not in snapshot.keys():
#                raise Exception("Object '" + str(self.name) + "' is running at time " + str(t) + " but not in the snapshot")
#            else:
#                for task in concerned: 
#                    try:
#                        task.load_snapshot(snapshot[self.name], t)
#                    except pleiad.error.EmptyTaskError:
#                        self.tasks.remove(task) # TODO Suppose qu'il y a une seule tâche renvoyée par find_task, sinon vider une liste avec une boucle for ne marchera pas
#                return True
#        else:
#            return False
#        
    def take_snapshot(self, t):
        """
        Fetches the snapshot from the right tasks
        Returns a fully formatted snapshot, which MAY be used to make a bigger snapshot
        """
        concerned = self.find_task_for_snapshot_at(t)
        ltmp = []
        for task in concerned:
            ltmp.append(task.take_snapshot(t))
        if len(ltmp) > 1: raise Exception("Must be handled")
        return ltmp[0] if len(ltmp) == 1 else None
    
    def __str_tasks__(self, t=0):
        """
        Returns a representation of a connectable composed of all of its tasks
        """
        s = ""
        if len(self.tasks) == 0:
            s += "[no task defined]"
        else:
            s += "\n"
            for task in self.tasks:
                s += task.__str__(t + 1) + "\n"
        
        return s.rstrip("\n")
        
class ConnectableNode:   
    """
    Container for nodes (containing several connectables)
    """
    
    counter = 0 # Number of created nodes
    
    def __init__(self, name):
        if name in Connectable.names.keys():
            raise Exception("This name has already been chosen (names must be unique)")
        if name == "datetime":
            raise Exception("datetime is a reserved name and cannot be chosen for a connectable name")
        
        self.id = ConnectableNode.counter
        ConnectableNode.counter += 1
        
        self.name = name
        self.children = []
    
    def add(self, *args):
        """
        Adds orphan connectables to a connectable node
        """
        
        for c in args:
            if not(isinstance(c, Connectable)):
                raise Exception("Children must be Connectable objects...")
            
            if c.parent is not None:
                raise Exception("Connectable " + str(c.id) + " already has a parent (" + str(c.parent.id) + ") and cannot have a new parent (" + str(self.id) + ")")
                
            # Setting the connectable's parent (the current node) and adding it to the current node
            c.parent = self
            self.children.append(c.id)
    
    def remove(self, c):
        """
        Removes a connectable from the current connectable node
        """
        if not(isinstance(c, Connectable)):
            raise Exception("Children must be Connectable objects...")
            
        if c.parent.id != self.id:
            raise Exception("Connectable " + self.id + " is not parent of Connectable " + c.id)
        
        # We drop it
        self.children.remove(c.id)
        # and it becomes orphan
        c.parent = None
    
    def get_connectables(self):
        """
        Returns all connectables of the current node
        """
        for c in self.children:
            yield Connectable.get(c)
            
    def nb_connectables(self):
        """
        Returns the number of connectables of the current node
        """
        return len(self.children)
    
#    def load_snapshot(self, snapshot):
#        """
#        Dispatches the snapshot to every leaf
#        """
#        snapshot_names = set(snapshot.keys())
#        snapshot_names.remove("datetime")
#        for l in self.get_leaves_recursive():
#            if l.load_snapshot(snapshot):
#                snapshot_names.remove(l.name)
#                
#        if len(snapshot_names) > 0:
#            raise Exception("Some objects in the snapshot do not exist in '" + self.name + "'")
#    
#    def take_snapshot(self, str_t, model):
#        """
#        Builds a snapshot from every leaf
#        """
#        obj = {"datetime": str_t}
#        
#        for s in self.get_leaves_recursive():
#            name = s.name
#            snapshot = s.take_snapshot(str_t, model)
#            
#            if snapshot is not None:
#                obj[name] = snapshot[name]
#            
#        return obj
    
    def __str__(self, t=0):
        """
        Returns the string representation of the connectable node
        """
        
        s = "\t" * t + "| " + type(self).__name__ + " Object '" + self.name + "' containing "
        
        s += str(self.nb_connectables()) + " children.\n"
        if self.nb_connectables() > 0:
            s += "\t" * t + "| Children:\n"
            for c in self.get_connectables():
                s += c.__str__(t + 1) + "\n"
            
        return s.rstrip() # Removing final trailing \n