#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 14:35:59 2019

@author: Hugues Souchard de Lavoreille
"""

from . import ConnectableNode
import pleiad.utils
        
class ExchangeNode:
    """
    Structure enabling electricity exchanges between connectable nodes
    Currently, a price structure is associated to the exchange node and each 
    exchange node reaches an equilibrium between demand and offer at each time, 
    for this price
    """
    
    def __init__(self, name, price_structure):
        """
        Initializes an exchange node with a name and a price structure
        """
        self.name = name
        self.price_structure = price_structure
        self.connectables = []
        
    def add(self, *args):
        """
        Adds nodes to the exchange node
        
        Parameters
        ----------
            n1, n2, ...: any number of nodes
        """
        
        for n in args:
            if not(isinstance(n, ConnectableNode)):
                raise Exception()
                
            # Actually an exchange node is modelled as if a power source was added to the node
            # But with some constraints of course! Those are defined later, in the model
            ps = pleiad.physicalobject.PowerSource("Cnx_" + n.name + "_" + self.name, price_structure_purchases=self.price_structure, price_structure_sales=self.price_structure)
            
            # Adding it to the node
            n.add(ps)
            
            # And to the exchange node
            self.connectables.append(ps)
    
    def __str__(self, t=0):
        """
        Returns the string representation of the exchange node
        """
        s = "\t" * t + "Exchange node '" + self.name + "' linking " + str(len(self.connectables)) + " together.\n"
        s += "\t" * t + "| Connected nodes:\n"
        for c in self.connectables:
            s += "\t" * (t + 1) + " " + c.parent.name + "\n"
        
        return s.rstrip("\n")