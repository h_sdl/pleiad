#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 14:51:49 2019

@author: Hugues Souchard de Lavoreille
"""

class LocalNetwork:
    """
    Container for a local network object containing nodes, exchange nodes and connectables
    """
    def __init__(self, nodes=(), exchange_nodes=()):
        """
        Must be initialized by specifying nodes and exchange nodes
        """
        self.nodes = nodes
        self.exchange_nodes = exchange_nodes

    def get_all_connectables(self):
        """
        Returns all the connectables of all nodes
        """
        for n in self.nodes:
            yield from n.get_connectables()
            
    def get_all_nodes_and_connectables(self):
        """
        Returns all nodes and all connectables of those nodes
        """
        for n in self.nodes:
            yield n
            yield from n.get_connectables()
            
    def get_nodes(self):
        """
        Returns all nodes
        """
        return self.nodes
            
    def nb_nodes(self):
        """
        Returns the number of nodes
        """
        return len(self.nodes)
    
    def get_exchange_nodes(self):
        """
        Returns all the exchange nodes
        """
        return self.exchange_nodes
    
    def nb_exchange_nodes(self):
        """
        Returns the number of exchange nodes
        """
        return len(self.exchange_nodes)        
    
    def __str__(self):
        """
        String representation of the local network
        """
        s = "LocalNetwork object containing " + str(self.nb_nodes()) + " nodes and " + str(self.nb_exchange_nodes()) + " exchange nodes.\n"
        if self.nb_nodes() > 0:
            s += "| Nodes:\n"
            for c in self.get_nodes():
                s += c.__str__(1) + "\n"
        if self.nb_exchange_nodes() > 0:
            s += "| Exchange nodes:\n"
            for c in self.get_exchange_nodes():
                s += c.__str__(1) + "\n"
            
        return s.rstrip()