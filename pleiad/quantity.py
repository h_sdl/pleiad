#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 16:36:05 2019

@author: Hugues Souchard de Lavoreille
"""
import numpy as np
from datetime import timedelta
import re

class QuantityBuilder(type):
    def __new__(cls, name, bases, dct):
        c = super(QuantityBuilder, cls).__new__(cls, name, bases, dct)
        if not name.endswith("Quantity"):
            c.build_class()
        return c

class Quantity(metaclass=QuantityBuilder):
    """
    Metaclass for physical quantities like Energy, Power, Costs, ...
    """
    
    # Other possible units that will be converted from and to the base unit
    units = {}
    # The unit used for default display, must be among units
    display_unit = None
    # Representation of the display unit (string) - optional
    str_display_unit = None
    # Dictionary containing the default unit and the other units as keys and their conversion ratios to the default unit as values
    all_units = None
    
    # Dictionaries containing all quotient / product classes whose numerator / multiplier is the current class, in order to automatically make the appropriate division / product
    registered_dividers = {}
    registered_multipliers = {}
    
    def __init__(self, q=None, **kwargs):
        """
        Default constructor for Quantities
        """
        
        # Impossible to define a quantity as such
        if type(self) == Quantity:
            raise Exception("Quantity type must be overloaded")
        
        # If no unit has been specified, using an arbitrary unit called "val". Can be used for internal operations but not recommended for outside manipulations
        if len(kwargs) == 0 and q is not None:
            self.val = q
        # Else using the specified unit if it is in the list of the possible units
        elif len(kwargs) == 1 and list(kwargs.keys())[0] in type(self).all_units.keys():
            self.val = list(kwargs.values())[0] / type(self).all_units[list(kwargs.keys())[0]]
        else:
            raise Exception("Cannot create " + str(type(self)) + " with following arguments : " + str(q) + ", " + str(kwargs))
    
    def __abs__(self):
        """ Returning a quantity whose value is the absolute value of the value of the input quantity """
        return type(self)(abs(self.val))
    
    def __mul__(self, l):
        """ Multiplies the given quantity with a scalar factor. Returns a quantity of the same type """
        if isinstance(l, (int, float)): return type(self)(self.val * l)
        elif type(l) in self.registered_multipliers.keys():
            l2 = TimedeltaCapsule(l) if type(l) is timedelta else l
            return self.registered_multipliers[type(l)](self.val * l2.val)
        else: return NotImplemented
    
    def __rmul__(self, l):
        """ Multiplies the given quantity with a scalar factor. Returns a quantity of the same type """
        if isinstance(l, (int, float)): return type(self)(self.val * l)
        elif type(l) in self.registered_multipliers.keys():
            l2 = TimedeltaCapsule(l) if type(l) is timedelta else l
            return self.registered_multipliers[type(l)](self.val * l2.val)
        else: return NotImplemented
    
    def __truediv__(self, l):
        """ Divides the given quantity by a scalar (then returns a quantity of the same type) or by another quantity of the same type (then returns a scalar) """
        if isinstance(l, (int, float)): return type(self)(self.val / l)
        elif isinstance(l, type(self)): return self.val / l.val
        elif type(l) in self.registered_dividers.keys():
            l2 = TimedeltaCapsule(l) if type(l) is timedelta else l
            return self.registered_dividers[type(l)](self.val / l2.val)
        else: return NotImplemented
        
    def __floordiv__(self, l):
        """ Integrally divides the given quantity by a quantity of the same type, returns an integer """
        if isinstance(l, type(self)): return int(self.val // l.val)
        else: return NotImplemented
        
    def __mod__(self, l):
        """ Returns the remainder (quantity of the same type) of the Euclidean division of the given quantity by another quantity of the same type """
        if isinstance(l, type(self)): return type(self)(self.val % l.val)
        else: return NotImplemented
    
    def __add__(self, q2):
        """ Returns a quantity whose value is the sum of the values of the two quantities of the same type given as input """
        if not(type(q2) is type(self)): return NotImplemented
        return type(self)(self.val if np.isnan(q2.val) else (q2.val if np.isnan(self.val) else self.val + q2.val))
    
    def __neg__(self):
        """ Returns a quantity whose value is the opposite of the value of the input quantity """
        return type(self)(-self.val)
    
    def __sub__(self, q2):
        """ Returns a quantity whose value is the subtraction of the two values of the quantities given as input """
        return self + (-q2)
    
    def __repr__(self):
        """ Displays a string containing the value (in the display unit) of the quantity followed by its default unit """
        u = type(self).display_unit if type(self).str_display_unit is None else type(self).str_display_unit
        return str(self.getv(type(self).display_unit)) + u
    
    def __str__(self):
        return self.__repr__()
    
    def __eq__(self, q2):
        """ Tells whether the two quantities of the same type are equal (1e-6 precision) """
        if not(type(q2) is type(self)):return NotImplemented
        return abs(self.val - q2.val) < 1e-6
    
    def __lt__(self, q2):
        """ Tells whether the first quantity is not greater than the second one (1e-6 precision) """
        if not(type(q2) is type(self)):return NotImplemented
        return q2.val - self.val > 1e-6
    
    def __le__(self, q2):
        """ Tells whether the first quantity is smaller than the second one (~1e-6 precision) """
        if not(type(q2) is type(self)):return NotImplemented
        return q2.val - self.val >= 0
    
    def __gt__(self, q2):
        """ Tells whether the first quantity is not smaller than the second one (~1e-6 precision) """
        if not(type(q2) is type(self)):return NotImplemented
        return self.val - q2.val > 1e-6
    
    def __ge__(self, q2):
        """ Tells whether the first quantity is greater than the second one (1e-6 precision) """
        if not(type(q2) is type(self)):return NotImplemented
        return self.val - q2.val >= 0
    
    def __round__(self, n, u=None):
        if u == None:
            u = type(self).display_unit
        return type(self)(**{u:round(self.getv(u), n)})
    
    def is_null(self):
        """ Tells whether the quantity is zero (1e-6 precision) """
        return self == type(self)(0.0)
    
    def getv(self, u):
        """ Returns the value of the quantity in the desired unit """
        if not(u in type(self).all_units.keys()):
            raise Exception("Unknown unit " + str(u) + " for quantity " + str(type(self)))
        else:
            return self.val * type(self).all_units[u]
        
    def setv(self, u, v):
        """ Sets the value of the quantity using the desired unit """
        if not(u in type(self).all_units.keys()):
            raise Exception("Unknown unit " + str(u) + " for quantity " + str(type(self)))
        else:
            self.val = v / type(self).all_units[u]
    
    @classmethod
    def nulls(cls, size):
        """ Creates a list of size 'size' containing Quantity(0) """
        l = []
        for i in range(size):
            l.append(cls(0))
        return l
    
    @classmethod
    def build_class(cls):
        """ Initializes the getters / setters enabling to get or set the value of the quantity using different units """
        cls.all_units = cls.units.copy()
        
        for u in cls.all_units:
            setattr(cls, u, property((lambda u: lambda self:self.getv(u))(u), (lambda u: lambda self,v:self.setv(u, v))(u)))
    
    @classmethod
    def from_string(cls, s):
        rp = re.compile("^([\d.e+\-]{1,})(" + "|".join(list(cls.all_units.keys())) + ")$")
        resp = rp.findall(s)
        
        # Checks that the values have been understood
        if len(resp) != 1:
            raise Exception("Did not understand formula " + s)
            
        return cls(**{resp[0][1]:float(resp[0][0])})
    
    def copy(self):
        return type(self)(self.val)

class TimedeltaCapsule:
    """
    Container for timedeltas, so that they behave the same way as quantities
    """
    all_units = {"s": 60, "m": 1, "h":1.0/60}
    display_unit = "m"
    str_display_unit = None
    
    def __init__(self, x):
        if isinstance(x, timedelta):
            self.val = x.total_seconds() / 60
        elif isinstance(x, (int, float)):
            self.val = x
        else:
            raise Exception()
            
    def get_timedelta(self):
        return timedelta(minutes=self.val)
    

class QuotientQuantity(Quantity):
    """
    Generic class for units deriving from other predefined units
    To be defined, such a unit needs a numerator and a denominator (both being Quantity class names)
    A display unit and a string display unit can also be defined (optional)
    Everything is done automatically (initialization, conversions, operations with the numerator and the denominator)
    """
    numerator = None # May not be a timedelta
    denominator = None # May be a timedelta
    
    def __mul__(self, l):
        # self * denominator = numerator
        if isinstance(l, self.denominator):
            if self.denominator is timedelta: l = TimedeltaCapsule(l)
            return self.numerator(self.val * l.val)
        else:
            return super().__mul__(l)
        
    def __rmul__(self, l):
        # denominator * self = numerator
        if isinstance(l, self.denominator):
            if self.denominator is timedelta: l = TimedeltaCapsule(l)
            return self.numerator(self.val * l.val)
        else:
            return super().__rmul__(l)
        
    def __rtruediv__(self, l):
        # numerator / self = denominator
        if isinstance(l, self.numerator):
            return TimedeltaCapsule(l.val / self.val).get_timedelta() if self.denominator is timedelta else self.denominator(l.val / self.val)
        else:
            return super().__rtruediv__(l)
    
    @classmethod
    def build_class(cls):
        dict_units = {}
        
        if cls.denominator is timedelta:
            denom = TimedeltaCapsule
        else:
            denom = cls.denominator
        
        for (un, cn) in cls.numerator.all_units.items():
            for (ud, cd) in denom.all_units.items():
                dict_units[un + "p" + ud] = cn / cd
        cls.units = dict_units
        
        if cls.display_unit is None:
            cls.display_unit = cls.numerator.display_unit + "p" + denom.display_unit
            cls.str_display_unit = (cls.numerator.str_display_unit if cls.numerator.str_display_unit is not None else cls.numerator.display_unit) + "/" + (denom.str_display_unit if denom.str_display_unit is not None else denom.display_unit)
        
        cls.numerator.registered_dividers[cls.denominator] = cls
        
        super(QuotientQuantity, cls).build_class()
        
class ProductQuantity(Quantity):
    """
    Generic class for units deriving from other predefined units by a product formula
    To be defined, such a unit needs two factors (both being Quantity class names)
    A display unit and a string display unit can also be defined (optional)
    Everything is done automatically (initialization, conversions, operations with the numerator and the denominator)
    """
    factor1 = None # May not be a timedelta
    factor2 = None # May be a timedelta
    
    def __truediv__(self, l):
        # self / factor1 = factor2 or self / factor2 = factor1
        if isinstance(l, self.factor1):
            return TimedeltaCapsule(self.val / l.val).get_timedelta() if self.factor2 is timedelta else self.factor2(self.val / l.val)
        elif isinstance(l, self.factor2):
            if self.factor2 is timedelta: l = TimedeltaCapsule(l)
            return self.factor1(self.val / l.val)
        else:
            return super().__truediv__(l)
    
    @classmethod
    def build_class(cls):
        dict_units = {}
        
        if cls.factor2 is timedelta:
            fact2 = TimedeltaCapsule
        else:
            fact2 = cls.factor2
        
        for (u1, c1) in cls.factor1.all_units.items():
            for (u2, c2) in fact2.all_units.items():
                dict_units[u1 + u2] = c1 * c2
        cls.units = dict_units
        
        if cls.display_unit is None:
            cls.display_unit = cls.factor1.display_unit + fact2.display_unit
            cls.str_display_unit = (cls.factor1.str_display_unit if cls.factor1.str_display_unit is not None else cls.factor1.display_unit) + (fact2.str_display_unit if fact2.str_display_unit is not None else fact2.display_unit)
        
        cls.factor1.registered_multipliers[cls.factor2] = cls
        
        super(ProductQuantity, cls).build_class()

class Power(Quantity):
    display_unit = "W"
    units = {"W": 1000.0, "kW":1.0, "MW": 0.001}

class Energy(ProductQuantity):
    factor1 = Power
    factor2 = timedelta
    display_unit = "kWh"

class Price(Quantity):
    str_display_unit = "$"
    display_unit = "USD"
    units = {"USD": 1.0, "cUSD": 100, "EUR": 0.9, "cEUR": 90.0, "CAD": 1.32, "cCAD": 132.0}
        
class EnergyPrice(QuotientQuantity):
    numerator = Price
    denominator = Energy
    display_unit = "cUSDpkWh"
    str_display_unit = "US¢/kWh"
    
class DurationPrice(QuotientQuantity):
    numerator = Price
    denominator = timedelta
    display_unit = "USDph"
    str_display_unit = "US$/h"