#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 16:39:09 2019

@author: Hugues Souchard de Lavoreille
"""

import re
import pleiad.quantity
import pleiad.structure

class FlexibleApplianceTask(pleiad.structure.ConnectableTask):
    def __init__(self, connectable, start, end, definition):
        """
        Parsing the definition (x) of a task for a flexible appliance and storing it
        - start
        - end
        - definition
        """
        
        self.preferred_price = pleiad.quantity.DurationPrice(0)
        self.preferred_start = None
        self.preferred_end = None
        self.preferred_power = None
        
        self.parse_definition(definition)
        
        super().__init__(connectable, start, end)
        
    
    def parse_definition(self, str_def):
        # RE to extract the total energy consumed
        r = re.compile("^(.+)=([A-Za-z0-9.\-+]+)$")
        res = r.findall(str_def)
        
        if len(res) != 1 and len(res[0]) != 2:
            raise Exception("Did not understand formula " + str_def)
        
        # Preparing the RE for the definition of the flexible powers
        r2 = re.compile("([\d.]{1,})(kW|W)(?:\||$)")
        res2 = r2.findall(res[0][0])
            
        # Checks that the powers have been understood
        # The second condition checks that one of the powers has not been forgotten (because if not understood, if is skipped by the RE engine)
        if len(res2) == 0 or len(res2) != len(res[0][0].split("|")):
            raise Exception("Did not understand formula " + res[0][0])
        
        # Extracts the information of the powers
        self.possible_powers = []
        for r in res2:
            self.possible_powers.append(pleiad.quantity.Power(**{r[1]:float(r[0])}))
            
        self.e_sum = pleiad.quantity.Energy.from_string(res[0][1])
    
    def generate_preferred_scheme(self, ts):
        sch = [pleiad.quantity.Power(0)] * int((self.end - self.start) / ts.delta)
        
        if self.preferred_start is None or self.preferred_end is None or self.preferred_power is None:
            return sch
        
        for i in range(int((self.preferred_start - self.start) / ts.delta), int((self.preferred_end - self.start) / ts.delta)):
            sch[i] = self.preferred_power
        return sch
    
    def generate_preferred_operation_binaries(self, ts):
        sch = [0] * int((self.end - self.start) / ts.delta)
        
        if self.preferred_start is None or self.preferred_end is None or self.preferred_power is None:
            return sch
        
        for i in range(int((self.preferred_start - self.start) / ts.delta), int((self.preferred_end - self.start) / ts.delta)):
            sch[i] = 1
        return sch
    
    def set_preferred(self, defin):
        startend,power,price = defin.split(",")
        start,end = self.parse_start_end(startend)
        
        power = pleiad.quantity.Power.from_string(power)
        
        if start < self.start or end > self.end or power * (end - start) != self.e_sum:
            raise Exception("Incompatible preferred operating time '" + startend + "'")
            
        self.preferred_start = start
        self.preferred_end = end
        self.preferred_power = power
        self.preferred_price = pleiad.quantity.DurationPrice.from_string(price)
    
    def load_snapshot(self, snapshot, t):
        cons = snapshot["consumed"]
        self.e_sum -= pleiad.quantity.Energy.from_string(cons)
        
        try:
            self.set_start_end(start=t)
        except pleiad.error.EmptyTaskError:
            if self.e_sum == pleiad.quantity.Energy(0):
                raise pleiad.error.EmptyTaskError()
            else:
                raise pleiad.error.ImpossibleTaskError()
    
    def take_snapshot(self, model, t):
        """
        Returns a snapshot for a flexible task
        Specification:
            {
                    "appliance name": {
                        "consumed": "8kWh"
                    },
                    "datetime": "2017-01-01T10:00"
            }
        """
        e_consumed = model.mt(self).energy_consumed_at(t)
        return {self.connectable.name : {"consumed": str(e_consumed)}, "datetime": t.strftime("%Y-%m-%dT%H:%M")}
    
    def check_compatibility_with_ts(self, ts):
        if abs((self.start - ts.start) % ts.delta).total_seconds() != 0:
            raise Exception() # Début de la sous-tâche mal calé
        if abs((self.end - self.start) % ts.delta).total_seconds() != 0:
            raise Exception() # Fin de la sous-tâche mal calée
    
    def __str__(self, t=0):
        return "\t" * t + "[" + self.__str_time_range__() + "]" + self.__str_definition__()
        
    
    def __str_definition__(self):
        return "|".join([str(i) for i in self.possible_powers]) + "=" + str(self.e_sum)

class FlexibleAppliance(pleiad.structure.Connectable):
    """
    Constructor for any flexible appliance
    An flexible appliance can have several tasks, each of them containing a target energy consumption, several power levels and a time window
    eg.
        Task 1 : 2017-01-01 16h-19h (Sum 8kWh with possible powers 4kW or 0kW)
        Task 2 : 2017-01-02 20h-22h -» (Sum 2kWh with possible powers 2kW of 0kW)
    """
    
    def add_task(self, str_def):
        """
        Adds a task to the current appliance, using the definition given as argument
        Returns the current object
        """
        
        r1 = re.compile("^\[([\d\-:/T]{33})\](.+)$")
        res1 = r1.findall(str_def)
        
        # Checks that the full formula with date has been understood
        if len(res1) != 1 and len(res1[0]) != 2:
            raise Exception("Did not understand formula " + str_def)
        
        self.last_task = FlexibleApplianceTask(self, *pleiad.structure.ConnectableTask.parse_start_end(res1[0][0]), res1[0][1])
        self.tasks.append(self.last_task)
        return self
    
    def add_similar_task(self, when, which=-1):
        if len(self.tasks) == 0:
            raise Exception("No task to copy, please first add a task to the connectable")
        self.last_task = FlexibleApplianceTask(self, *pleiad.structure.ConnectableTask.parse_start_end(when), self.tasks[which].__str_definition__())
        self.tasks.append(self.last_task)
        return self
    
    def set_preferred(self, defin):
        if self.last_task is None:
            raise Exception("No task to set")
        self.last_task.set_preferred(defin)
        return self
    
    def __str__(self, t=0):
        return "\t" * t + "| (Flexible) " + self.name + ": " + super().__str_tasks__(t)