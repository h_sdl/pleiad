#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 16:38:06 2019

@author: Hugues Souchard de Lavoreille
"""

import pleiad.quantity
import pleiad.structure
from datetime import datetime


class ThermalApplianceTask(pleiad.structure.ConnectableTask):
    def __init__(self, connectable, x) :
        """
        Parsing the definition (x) of a task for a thermal appliance and storing it
        - start
        - end
        - definition
        """
        
        patterns = x["patterns"]
        self.definition = []
        
        # Start and end date of the task (max/min of the dates of all the patterns of a task)
        start = None
        end = None
        
        for pat_name,pat in patterns.items():
            res = []
            for k,p in pat.items():
                
                # Extracts the start and end time of each subtask in every pattern
                times = k.split("/")
                kstart = datetime.strptime(times[0], "%Y-%m-%dT%H:%M")
                kend = datetime.strptime(times[1], "%Y-%m-%dT%H:%M")
                
                # Checking that dates are consistent
                if kstart > kend:
                    raise Exception("Beginning of task if after its end")
                    
                # Finding the start and end dates of the task
                if start is None:
                    start = kstart
                else:
                    start = min(start, kstart)
                    
                if end is None:
                    end = kend
                else:
                    end = max(end, kend)
                    
                power = pleiad.quantity.Power.from_string(p)
                res.append({'start':kstart, 'end':kend, 'power':power})
            
            res.sort(key=lambda x:x["start"]) # Ensuring the power list is sorted
            self.definition.append({"name": pat_name, "pattern": res})
        
        
        
        super().__init__(connectable, start, end)
        
        self.preferred = None
        self.preferred_price = pleiad.quantity.DurationPrice(0)
    
    def check_compatibility_with_ts(self, ts):
        if abs((self.start - ts.start) % ts.delta).total_seconds() != 0:
            raise Exception() # Début de la sous-tâche mal calé
        if abs((self.end - self.start) % ts.delta).total_seconds() != 0:
            raise Exception() # Fin de la sous-tâche mal calée
    
    def generate_schemes(self, ts):
        """
        Returns a list of ts-adapted lists of powers 
        (one list per possible pattern, each of these having one power per timestamp)
        
        Parameters
        ----------
            ts: pleiad.TimeDivision object
            
        Returns
        -------
            list of list of powers
        """
        
        # Goal: generate all the patterms, under a compatible format (time unit: multiples of timesteps)
        
        n = (self.end - self.start) // ts.delta
        schemes = []
        names = []
        binary_is_operating = []
        
        for p0 in self.definition:
            pname = p0["name"]
            p = p0["pattern"]
            s = [pleiad.quantity.Power(0) for _ in range(n)]
            
            imini_operating = n # Index of the start of operation of the pattern
            imaxi_operating = 0 # Index of the end of operation of the pattern
            b = [0 for _ in range(n)]
            
            for el in p:
                elstart = el['start']
                elstartn = (elstart - self.start) // ts.delta
                elend = el['end']
                elendn = (elend - self.start) // ts.delta
                elpower = el['power']
                
                for i in range(elstartn, elendn):
                    s[i] = elpower
                    
                imini_operating = min(imini_operating, elstartn)
                imaxi_operating = max(imaxi_operating, elendn)
            
            schemes.append(s)
            names.append(pname)
            for i in range(imini_operating, imaxi_operating): b[i] = 1
            binary_is_operating.append(b)
            
        return (names, schemes, binary_is_operating, self.preferred)
    
    def set_preferred(self, defin):
        """
        Sets the mentioned pattern as the preferred one.
        A penalty will be set if this pattern is not used
        
        Parameters
        ----------
            string containing the name of the preferred pattern, a comma and the penalty
            eg. "comfort,1Dph"
            
        Returns
        -------
            None
        """
        
        name,price = defin.split(",")
        for d in self.definition:
            if d["name"] == name:
                self.preferred = name
                self.preferred_price = pleiad.quantity.DurationPrice.from_string(price)
                return
        raise Exception("Pattern not found: '" + defin + "'")
    
    def load_snapshot(self, snapshot, t):            
        sn = snapshot["used_pattern_name"]
        
        new_definition = []
        for p in self.definition:
            if p["name"] == sn:
                new_definition.append(p)
                break
            
        if len(new_definition) == 0:
            raise Exception("Could not find pattern '" + sn + "' in the definition of appliance '" + self.connectable.name + "'")
        
        
        # Now erasing all what happened before t in the pattern
        p = new_definition[0]["pattern"]
        new_p = []
        for el in p:
            if el["start"] >= t:
                new_p.append(el)
            elif el["start"] < t and el["end"] > t:
                new_el = {"start": t, "end": el["end"], "power": el["power"]}
                new_p.append(new_el)
        
        new_definition[0]["pattern"] = new_p
        
        self.definition = new_definition
                
        self.set_start_end(start=t)
        
    def take_snapshot(self, model, t):
        """
        Returns a snapshot for a thermal task
        Specification:
            {
                    "appliance name": {
                        "used_pattern_name": "comfort"
                    }
                    "datetime": "2017-01-01T10:00"
            }
        """
        pattern_name = model.mt(self).used_pattern_name()
        return {self.connectable.name : {"used_pattern_name": pattern_name}, "datetime": t.strftime("%Y-%m-%dT%H:%M")}
    
    def __str__(self, t=0):
        return "\t" * t + "[" + self.__str_time_range__() + "] (" + str(len(self.definition)) + " possible patterns : " + (" or ".join(["'" + str(i["name"]) + "'" for i in self.definition])) + ")"
        

class ThermalAppliance(pleiad.structure.Connectable):
    """
    Constructor for any thermal appliance
    A thermal appliance can have several tasks, each of them containing several possible operation patterns
    eg.
        Task 1 : 2017-01-01 16h-17h@1kW, 17h-18h@2kW OR 16h-18h@1.5kW
        Task 2 : 2017-01-02 20h-22h@3kW
    """
    
    def add_task(self, defin):
        """
        Adds a task to the current appliance, using the definition given as argument
        Returns the current object
        """
        self.last_task = ThermalApplianceTask(self, defin)
        self.tasks.append(self.last_task)
        return self
    
    def set_preferred(self, defin):
        """
        Function setting the preferred pattern on the last set task
        
        Parameters
        ----------
            same as ThermalApplianceTask.set_preferred
            
        Returns
        -------
        ThermalAppliance object (self)
        """
        
        if self.last_task is None:
            raise Exception("No task to set")
        self.last_task.set_preferred(defin)
        return self
    
    def __str__(self, t=0):
        """
        Representation of the thermal appliance
        
        Parameters
        ----------
            t : int
                number of tabs used before printing the appliance
                
        Returns
        -------
            string containing the representation
        """
        
        return "\t" * t + "| (Thermal) " + self.name + ": " + super().__str_tasks__(t)