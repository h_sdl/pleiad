#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 16:39:58 2019

@author: Hugues Souchard de Lavoreille
"""

import re
import pleiad.structure
import pleiad.quantity
import datetime

class ActivityBasedApplianceTask(pleiad.structure.ConnectableTask):
    
    time_conversion = {'h':'hours', 'm':'minutes'}
    
    def __init__(self, connectable, start, end, definition) :
        """
        Parsing the definition (x) of a task for an activity-based appliance and storing it
        - start
        - end
        - definition
        """
        self.parse_definition(definition)
        self.preferred_price = pleiad.quantity.DurationPrice(0)
        self.preferred_start = None
        self.preferred_end = None
        
        super().__init__(connectable, start, end)
       
    def parse_definition(self, str_def):
        """
        Parses the string definition of an Activity-based appliance and
        initializes the current object with the information contained in the
        parsed data
        
        Parameters
        ----------
        str_def : string definition of the task
                  
        
        Returns
        -------
        
        """
        
        # Detect (or not) an initial preemption
        r = re.compile("^((?:\([0-9.e+mh\-]+\))|>)?(.+)$")
        res = r.findall(str_def)
        
        if len(res) != 1 and len(res[0]) != 2:
            raise Exception("Did not understand formula '" + str_def + "'")
        
        # If an initial preemption has been set, store it
        min_first_preemption, max_first_preemption = self.parse_preemption(res[0][0])
        
        # Preparing the RE for the definition of the activity
        r2 = re.compile("(\d{1,})(m|h)@([A-Za-z0-9.\-+]+)((?:\([0-9.e+mh\-]+\))|$|>)")
        res2 = r2.findall(res[0][1])
        
        # Checks that the activities have been understood
        # The second condition checks that one of the definitions has not been forgotten (because if not understood, if is skipped by the RE engine)
        if len(res2) == 0:
            raise Exception("Did not understand formula " + res[0][1])
        
        # Extracts the information of the activities
        defin = []
        for r in res2:
            delta1 = datetime.timedelta(**{self.time_conversion[r[1]]:int(r[0])})
            delta2, delta3 = self.parse_preemption(r[3])
            power = pleiad.quantity.Power.from_string(r[2])
            defin.append((power, delta1, delta2, delta3))
        
        self.definition = defin
        self.max_first_preemption = max_first_preemption
        self.min_first_preemption = min_first_preemption
    
    @classmethod
    def parse_preemption(cls, str_pre):
        """
        This function directly parses a preemption string
        
        Parameters
        ----------
        str_pre : string definition of a preemption. Can either be "", ">" or
                  "(d1-d2)" where d1 or d2 are two durations under one of the 
                  following syntaxes: "30m", "1h", ""
                  
        Returns
        -------
        Tuple of datetime.timedelta objects containing the minimum and maximum
        sizes of the preemption
        
        Examples
        --------
        "" -> (None, None)
            [No preemption defined, whatever preemption duration can be chosen]
        ">" -> (timedelta(0), timedelta(0))
            [No preemption possible, minimum and maximum sizes are 0, which 
            means that no preemption is possible]
        "(-)" -> (None, None)
            [Same as ""]
        "(30m-)" -> (timedelta(minutes=30), None)
            [A minimum break of 30 minutes will be done]
        "(-1h)" -> (None, timedelta(hours=1))
            [A maximum break of 1h will be done. The task may also not pause]
                  
        """
        if str_pre == "":
            return None,None
        if str_pre == ">":
            return datetime.timedelta(0), datetime.timedelta(0)
        
        r = re.compile("^\((?:(\d{1,})(m|h))?-(?:(\d{1,})(m|h))?\)$")
        res = r.findall(str_pre)
        
        if len(res) != 1 or len(res[0]) != 4:
            raise Exception("Did not understand preemption '" + str_pre + "'")
            
        if res[0][0] == "":
            pre1 = None
        else:
            pre1 = datetime.timedelta(**{cls.time_conversion[res[0][1]]:int(res[0][0])})
            
        if res[0][2] == "":
            pre2 = None
        else:
            pre2 = datetime.timedelta(**{cls.time_conversion[res[0][3]]:int(res[0][2])})
            
        return (pre1, pre2)
    
    def __str_timedelta__(self, td):
        return str(int(td.total_seconds()//60)) + "m"
    
    def __str_preemption__(self, pre):
        pre1, pre2 = pre
        s = "("
        if pre1 is not None:
            s += self.__str_timedelta__(pre1)
        s += "-"
        if pre2 is not None:
            s += self.__str_timedelta__(pre2)
        s += ")"    
        return s
        
    def copy_definition(self):
        defin_2 = []
        for d in self.definition:
            defin_2.append((d[0].copy(), d[1], d[2], d[3]))
        
        return {"definition": defin_2, "max_first_preemption": self.max_first_preemption, "min_first_preemption": self.min_first_preemption}
        
    
    def minimum_size_without_preemptions(self):
        """
        Computes the minimum duration of an activity-based task (if all 
        preemptions have a zero length)
        
        Returns
        -------
        datetime.timedelta object containing the minimum size of the task
        """
        res = datetime.timedelta(0)
        for (_, duration, _, _) in self.definition:
            res = duration + res
        return res
    
    def check_compatibility_with_ts(self, ts):
        for (_, duration, min_dur_pre, max_dur_pre) in self.definition:
            if abs(duration % ts.delta).total_seconds() != 0:
                raise Exception() # Durée sous-tâche pas nb entiers de pas
            if min_dur_pre is not None and abs(min_dur_pre % ts.delta).total_seconds() != 0:
                raise Exception() # Durée de préemption min = même chose
            if max_dur_pre is not None and abs(max_dur_pre % ts.delta).total_seconds() != 0:
                raise Exception() # Durée de préemption max = même chose
        if abs((self.start - ts.start) % ts.delta).total_seconds() != 0:
            raise Exception() # Début de la sous-tâche mal calé
        if abs((self.end - self.start) % ts.delta).total_seconds() != 0:
            raise Exception() # Fin de la sous-tâche mal calée
            
        if self.minimum_size_without_preemptions() > self.end - self.start:
            raise Exception("Task '" + self.connectable.name + "'[" + str(self.minimum_size_without_preemptions()) + "] is too long for its time interval (" + self.__str_time_range__() + ")")
    
    def set_preferred(self, defin):
        startend,price = defin.split(",")
        start,end = self.parse_start_end(startend)
        
        if start < self.start or end > self.end or (end - start) != self.minimum_size_without_preemptions():
            raise Exception("Incompatible preferred operating time '" + startend + "'")
            
        self.preferred_start = start
        self.preferred_end = end
        self.preferred_price = pleiad.quantity.DurationPrice.from_string(price)
    
    def generate_preferred_scheme(self, ts):
        """
        Generates the preferred scheme if it exists, as a list containing powers on ts.delta timesteps
        """
        if self.preferred_start is None or self.preferred_end is None:
            return None
        
        list_powers = [pleiad.quantity.Power(0)] * int((self.end - self.start) / ts.delta)
        idx_cursor = int((self.preferred_start - self.start) / ts.delta)
        for (power, duration, _, _) in self.definition:
            for i in range(idx_cursor,idx_cursor + int(duration/ts.delta)):
                list_powers[i] = power
            idx_cursor += int(duration/ts.delta)
        return list_powers
    
    def generate_schemes(self, ts):
        """
        Generates all the schemes whose length is equal to the possible operation length of the appliance
        """
        
        longest_possible_preemption = self.end - self.start - self.minimum_size_without_preemptions()
        
        # Maximum and minimum sizes of the first preemption task
        if self.max_first_preemption is not None:
            max_first_preemption = self.max_first_preemption
        else:
            max_first_preemption = longest_possible_preemption
        if self.min_first_preemption is not None:
            min_first_preemption = self.min_first_preemption
        else:
            min_first_preemption = datetime.timedelta(0)
        
        # Conversion of the definition into a definition with indexes instead of dates
        definition_n = []
        for i in range(len(self.definition)-1, -1, -1):
            (power_per_delta, duration, min_preem, max_preem) = self.definition[i]
            if min_preem is None: min_preem = datetime.timedelta(0)
            if max_preem is None: max_preem = longest_possible_preemption
            definition_n.append((power_per_delta, ts.delta_to_idxr(duration), ts.delta_to_idxr(min_preem), ts.delta_to_idxr(max_preem)))
            
        # Adding first preemption
        definition_n.append((pleiad.quantity.Power(0.0), 0, ts.delta_to_idxr(min_first_preemption), ts.delta_to_idxr(max_first_preemption)))
        
        schemes = ActivityBasedApplianceTask.generate_recursive(definition_n.copy(), [], [], ts.delta_to_idxr(self.end - self.start))
        
        return schemes
        
    @staticmethod
    def generate_recursive(definition, start, start_b, max_size):
        """
        Takes in input a definition, for example "2 timesteps"@60W("0 timestep"-"2 timesteps")|"4 timesteps"@100W("0 timestep" - "0 timestep")
                       a sequence of powers already generated for every timestep, for example [1W, 0W]
                       a maximum number of timesteps, for example 8
                       
        Generates the list of the possible sequence of powers starting from the sequence already generated
        for example here : [[1W, 0W, 60W, 60W, 100W, 100W, 100W, 100W]] (zero possible preemption to match the maximal size)
        
        """
        
        if len(start) > max_size:
            # This case prevents from exploring too many branches of the recursion tree
            return [],[]
        if len(definition) < 1:
            if len(start) == max_size:
                return [start], [start_b]
            else:
                return [],[]
        elif len(definition) == 1:
            # This case is not necessary but is useful in Python to prevent from changing too many times of context
            # The case <1 will not be used anymore when this case == 1 is activated
            (power_per_delta, nb_time_periods, min_preem, max_preem) = definition.pop()
            if max_size - len(start) - nb_time_periods <= max_preem and max_size - len(start) - nb_time_periods >= min_preem:
                return [start + nb_time_periods * [power_per_delta] + pleiad.quantity.Power.nulls(max_size - len(start) - nb_time_periods)], [start_b + nb_time_periods * [1] + (max_size - len(start) - nb_time_periods) * [0]]
            else:
                return [],[]
        else:
            (power_per_delta, nb_time_periods, min_preem, max_preem) = definition.pop()
            tous_schemas = []
            tous_schemes_binaires = []
            for i in range(min_preem, max_preem + 1):
                start_power = start + nb_time_periods * [power_per_delta] + pleiad.quantity.Power.nulls(i)
                start_binary = start_b + (nb_time_periods + i) * [0 if power_per_delta.is_null() else 1]
                sch, binaire = ActivityBasedApplianceTask.generate_recursive(definition.copy(), start_power, start_binary, max_size)
                tous_schemas = tous_schemas + sch
                tous_schemes_binaires = tous_schemes_binaires + binaire
            return tous_schemas, tous_schemes_binaires
    
    def load_snapshot(self, snapshot, t):
        elapsed_snapshot = datetime.timedelta(minutes=float(snapshot["elapsed_operating_time"].rstrip("m")))
        
        new_definition = []
        elapsed_definition = datetime.timedelta(0)
        init_preemp_min = None
        init_preemp_max = None
        
        for (power_per_delta, duration, min_preem, max_preem) in self.definition:
            if elapsed_definition + duration < elapsed_snapshot:
                elapsed_definition += duration
                continue
            elif elapsed_definition + duration == elapsed_snapshot:
                init_preemp_min = min_preem
                init_preemp_max = max_preem
                elapsed_definition += duration
            elif elapsed_definition + duration > elapsed_snapshot:
                if elapsed_definition < elapsed_snapshot:
                    init_preemp_min = datetime.timedelta(0)
                    init_preemp_max = datetime.timedelta(0)
                    new_definition.append((power_per_delta, duration - (elapsed_snapshot - elapsed_definition), min_preem, max_preem))
                else:
                    new_definition.append((power_per_delta, duration, min_preem, max_preem))
                
        try:
            self.set_start_end(start=t)
        except pleiad.error.EmptyTaskError:
            if elapsed_snapshot == self.minimum_size_without_preemptions():
                raise pleiad.error.EmptyTaskError()
            else:
                raise pleiad.error.ImpossibleTaskError()
            
        self.definition = new_definition
        self.min_first_preemption = init_preemp_min
        self.max_first_preemption = init_preemp_max
    
    def take_snapshot(self, model, t):
        """
        Returns a snapshot for an activity-based task
        Specification:
            {
                    "appliance name" : {
                            "elapsed_operating_time": "30m"
                    },
                    "datetime": "2017-01-01T10:00"
            }
        """        
        time_elapsed = model.mt(self).elapsed_operating_time_at(t)
        return {self.connectable.name : {"elapsed_operating_time": str(int(time_elapsed.total_seconds()//60)) + "m"}, "datetime": t.strftime("%Y-%m-%dT%H:%M")}
    
    def __str__(self, t=0):
        return "\t" * t + "[" + self.__str_time_range__() + "]" +  self.__str_definition__()    
    
    def __str_definition__(self):
        s = ""
        s += self.__str_preemption__((self.min_first_preemption, self.max_first_preemption))
        for t in self.definition:
            s += self.__str_timedelta__(t[1])
            s += "@"
            s += str(t[0])
            s += self.__str_preemption__((t[2], t[3]))
        return s

class ActivityBasedAppliance(pleiad.structure.Connectable):
    """
    Activity Based appliance
    An activity based appliance can have several tasks, each of them containing several subtasks that can be separated by preemptions
    eg.
        Task 1 : 2017-01-01 16h-19h -» 1h at 1kW and 30min at 2kW, possible preemption of 1h max
        Task 2 : 2017-01-02 20h-22h -» 1h at 1kW and 30min at 2kW, possible preemption of 1h max
        Task 3 : 2017-01-03 15h-17h -» 30min at 0.5kW and 30min at 2kW, no preemption allowed
    """
    
    def add_task(self, str_def):
        """
        Adds a task to the current appliance, using the definition given as argument
        Returns the current object
        """
        
        r1 = re.compile("^\[([\d\-:/T]{33})\](.+)$")
        res1 = r1.findall(str_def)
        
        # Checks that the full formula with date has been understood
        if len(res1) != 1 and len(res1[0]) != 2:
            raise Exception("Did not understand formula " + str_def)
        
        self.last_task = ActivityBasedApplianceTask(self, *pleiad.structure.ConnectableTask.parse_start_end(res1[0][0]), res1[0][1])
        self.tasks.append(self.last_task)
        return self
    
    def add_similar_task(self, when, which=-1):
        if len(self.tasks) == 0:
            raise Exception("No task to copy, please first add a task to the connectable")
        
        self.last_task = ActivityBasedApplianceTask(self, *pleiad.physicalobject.ConnectableTask.parse_start_end(when), self.tasks[which].__str_definition__())
        self.tasks.append(self.last_task)
        return self
    
    def set_preferred(self, defin):
        if self.last_task is None:
            raise Exception("No task to set")
        self.last_task.set_preferred(defin)
        return self
    
    def __str__(self, t=0):
        return "\t" * t + "| (Activity-based) " + self.name + ": " + super().__str_tasks__(t)