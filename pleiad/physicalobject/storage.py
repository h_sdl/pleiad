#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 16:34:59 2019

@author: Hugues Souchard de Lavoreille
"""

import re
import pleiad.structure
import pleiad.quantity
import datetime

class StorageTask(pleiad.structure.ConnectableTask):
    
    def __init__(self, connectable, time_range, cycle_limit, duration_for_cycle_limit, soc_constraints, same_soc_start_end):
        # Extracting start and end
        start, end = super().parse_start_end(time_range)
        
        clevel = []
        for c in soc_constraints:
            clevel.append(self.parse_constraint(c, connectable.e_max))
        
        self.cycle_limit = cycle_limit
        self.duration_for_cycle_limit = duration_for_cycle_limit
        self.soc_constraints = clevel
        self.same_soc_start_end = same_soc_start_end
        
        super().__init__(connectable, start, end)
    
    @staticmethod
    def parse_constraint(c, e_max):
        r1 = re.compile("^([\d\-:T]{16})(?:/([\d\-:T]{16}))?(=|<|>)([\d.]{1,}(?:J|Wh|kWh|%))$")
        res1 = r1.findall(c)
        
        if len(res1) != 1 or len(res1[0]) != 4: raise Exception("Impossible to parse battery level constraint '" + c + "'")
        
        dt1 = datetime.datetime.strptime(res1[0][0], "%Y-%m-%dT%H:%M")
        dt2 = dt1 if res1[0][1] == "" else datetime.datetime.strptime(res1[0][1], "%Y-%m-%dT%H:%M")
        
        if dt2 < dt1:
            raise Exception("Impossible definition for level constraint")
        
        dict_constraint_type = {"=":"E", "<":"L", ">":"G"}
        lvl = (float(res1[0][3][:-1]) / 100) * e_max if res1[0][3][-1] == "%" else pleiad.quantity.Energy.from_string(res1[0][3])
        
        return {"start":dt1, "end": dt2, "energy":lvl, "type": dict_constraint_type[res1[0][2]]}
        
    def check_compatibility_with_ts(self, ts):
        if abs((self.start - ts.start) % ts.delta).total_seconds() != 0:
            raise Exception() # Début tâche mal calé
        if abs((self.end - self.start) % ts.delta).total_seconds() != 0:
            raise Exception() # Fin tâche mal calée
        for el in self.soc_constraints:
            if abs((el["start"] - self.start) % ts.delta).total_seconds() != 0:
                raise Exception()
            if abs((el["end"] - self.start) % ts.delta).total_seconds() != 0:
                raise Exception()
            if el["start"] > self.end or el["start"] < self.start or el["end"] > self.end or el["end"] < self.start:
                raise Exception("Impossible to define a level constraint outside of battery temporal operation bounds")
        if (self.duration_for_cycle_limit % ts.delta).total_seconds() != 0:
            raise Exception()

    def load_snapshot(self, snapshot, t):
        """
        Input: snapshot containing an energy level, and a datetime
        Modifies the level constraints of the battery, so that they start after the beginning of the snapshot
        Creates another level constraint for the energy level at time t
        Ensures that the constraint are compatible and removes the redundant ones
        """
        stored = pleiad.quantity.Energy.from_string(snapshot["stored"])
        
        new_soc_constraints = []
        old_soc_constraints = self.soc_constraints
        for c in old_soc_constraints:
            if c["start"] > t:
                new_soc_constraints.append(c)
            elif c["start"] <= t and t <= c["end"]:
                if (c["type"] == "E" and c["energy"] != stored) or (c["type"] == "G" and stored < c["energy"]) or (c["type"] == "L" and stored > c["energy"]):
                    raise Exception("Snapshot incompatible with the soc constraints of the battery")
                else:
                    new_soc_constraints.append(c)
        
        new_soc_constraints.append({"start":t, "end":t, "energy": stored, "type":"E"})
        self.soc_constraints = new_soc_constraints
        
        self.set_start_end(start=t)
        
    def take_snapshot(self, model, t):
        """
        Returns a snapshot for an activity-based task
        Specification:
            {
                    "name": "appliance name" : {
                        "stored": "1kWh"
                    },
                    "datetime": "2017-01-01T10:00"
            }
        """
        stored = model.mt(self).stored_energy_at(t)
        return {self.connectable.name: {"stored": str(stored)}, "datetime": t.strftime("%Y-%m-%dT%H:%M")}
    
    def __str__(self, t=0):
        return "\t" * t + "[" + self.__str_time_range__() + "] (max. " + str(self.cycle_limit) + " cycles in " + str(self.duration_for_cycle_limit) + (", same SOC start/end" if self.same_soc_start_end else "") + ", " + str(len(self.soc_constraints)) + " SOC constraint(s) [" + "][".join([self.__str_soc_constraint__(c) for c in self.soc_constraints]) + "])"
    
    def __str_soc_constraint__(self, c):
        return c["start"].strftime("%Y-%m-%dT%H:%M") + ("/" + c["end"].strftime("%Y-%m-%dT%H:%M") if c["start"] != c["end"] else "") + ("=" if c["type"] == "E" else ("<" if c["type"] == "L" else ">")) + str(c["energy"])
    
    def build_soc_constraints(self, ts):
        """
        This function transforms the set of soc constraints present in the battery's definition into a set of constraints at each time step
        eg. a 10am-11am > 1kWh with 30min timestep is transformed into {10am>1kWh, 10.30am>1kWh, 11am>1kWh}
        The soc constraints are checked, so that they are not redundant and compatible one with another
        """
        
        # The following helper functions tell how to combine two or three constraints at the same time into one or two constraints
        # Example:
        # If 11am > 1kWh and 11am > 2kWh, the first constraint is useless and can be removed
        def combineEE(e1, e2):
            if e1 == e2:
                return [None, e1, None]
            else:
                raise Exception("Incompatible battery constraints")
        def combineEL(e, l):
            if e <= l:
                return [None, e, None]
            else:
                raise Exception("Incompatible battery constraints")
        def combineEG(e, g):
            if e >= g:
                return [None, e, None]
            else:
                raise Exception("Incompatible battery constraints")
        def combineLL(l1, l2):
            return [min(l1, l2), None, None]
        def combineGL(g, l):
            if l == g:
                return [None, l, None]
            elif l > g:
                raise Exception("Incompatible battery constraints")
            else:
                return [l, None, g]
        def combineGG(g1, g2):
            return [None, None, max(g1, g2)]
        def combineEGL(e, g, l):
            if g <= e and e <= l:
                return [None, e, None]
            else:
                raise Exception("Incompatible battery constraints")
        def combineGLL(g, l1, l2):
            return combineGL(combineLL(l1, l2)[0], g)
        def combineGGL(g1, g2, l):
            return combineGL(l, combineGG(g1, g2)[2])
        def combineE(e):
            return [None, e, None]
        def combineL(l):
            return [l, None, None]
        def combineG(g):
            return [None, None, g]
        helpers = {"E": combineE, "EE":combineEE, "EL": combineEL, "EG": combineEG, "EGL": combineEGL, "G": combineG, "GG": combineGG, "GL": combineGL, "GGL": combineGGL, "GLL": combineGLL, "combineL": combineL, "combineLL": combineLL}
        
        # List storing, for each t in [start, end], whether there is already a level constraint (+1 because state and not flux)
        # For each time step, there can be at most one constraint of each type and some combinations are not allowed (eg. >1 and < 5 is possible, whereas >1 and =0 is not)
        # The list is initialized with no constraints, and each constraint is added one at a time
        soc_constraints_time = [[None, None, None] for _ in range(ts.n + 2)]
        idx_start = ts.time_to_idx(self.start)
        
        # Now, we are reviewing each constraint in order to apply the abovedefined transformations
        for c in self.soc_constraints:
            dt1 = c["start"]
            dt2 = c["end"]
            lvl = c["energy"]
            typ = c["type"]
            dt_idx_start = ts.time_to_idx(dt1)
            dt_idx_end = ts.time_to_idx(dt2)
            
            # From the start to the end of the constraint time (both bounds included)
            for dt_idx in range(dt_idx_start, dt_idx_end + 1):
                c0 = soc_constraints_time[dt_idx - idx_start]
                c0L = c0[0]
                c0E = c0[1]
                c0G = c0[2]
                
                nbE, listE = (0, []) if c0E is None else (1, [c0E])
                nbL, listL = (0, []) if c0L is None else (1, [c0L])
                nbG, listG = (0, []) if c0G is None else (1, [c0G])
                
                if typ == "E":
                    nbE += 1
                    listE.append(lvl)
                elif typ == "L":
                    nbL += 1
                    listL.append(lvl)
                else:
                    nbG += 1
                    listG.append(lvl)
                
                str_func = nbE * "E" + nbG * "G" + nbL * "L"
                args = tuple(listE + listG + listL)
                
                c0[0], c0[1], c0[2] = helpers[str_func](*args)
        
        # Generating the final list of constraints from the "soc_constraints_time" array
        constraints_list = []
        for t in range(ts.n + 2):
            c = soc_constraints_time[t]
            if c[0] is not None:
                constraints_list.append({"datetime": ts.idx_to_time(idx_start + t), "energy": c[0], "type": "L"})
            if c[1] is not None:
                constraints_list.append({"datetime": ts.idx_to_time(idx_start + t), "energy": c[1], "type": "E"})
            if c[2] is not None:
                constraints_list.append({"datetime": ts.idx_to_time(idx_start + t), "energy": c[2], "type": "G"})
        
        return constraints_list
  
class Storage(pleiad.structure.Connectable):
    
    def __init__(self, name, e_max, p_max, self_discharge_rate_per_day = 0, charging_efficiency = 1, discharging_efficiency = 1):
        super().__init__(name)
        self.e_max = e_max
        self.p_max = p_max
        self.self_discharge_rate_per_day = self_discharge_rate_per_day
        self.charging_efficiency = charging_efficiency
        self.discharging_efficiency = discharging_efficiency
        
    def add_task(self, time_range="", cycle_limit=2, duration_for_cycle_limit=datetime.timedelta(days=1), soc_constraints=[], same_soc_start_end=True):
        self.tasks.append(StorageTask(self, time_range, cycle_limit, duration_for_cycle_limit, soc_constraints, same_soc_start_end))
        
        return self
        
    def __str__(self, t=0):
        """
        Representation of a storage
        """
        return "\t" * t + "| (Storage) " + self.name + " [e_max=" + str(self.e_max) + \
               ", p_max=" + str(self.p_max) + ", self_discharge_rate_per_day=" + str(self.self_discharge_rate_per_day) + \
               ", charging_efficiency=" + str(self.charging_efficiency) + ", discharging_efficiency=" + str(self.discharging_efficiency) + \
               "]: " + super().__str_tasks__(t)