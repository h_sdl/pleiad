#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 16:34:59 2019

@author: Hugues Souchard de Lavoreille
"""

import pleiad.quantity
import pleiad.structure
import pleiad.utils

class PowerSourceTask(pleiad.structure.ConnectableTask):
    
    def __init__(self, connectable, time_range=""):
        
        # Extracting start and end dates
        start, end = super().parse_start_end(time_range)
        
        super().__init__(connectable, start, end)
        
    def check_compatibility_with_ts(self, ts):
        if abs((self.start - ts.start) % ts.delta).total_seconds() != 0:
            raise Exception() # Début tâche mal calé
        if abs((self.end - self.start) % ts.delta).total_seconds() != 0:
            raise Exception() # Fin tâche mal calée
    
    def load_snapshot(self, snapshot, t):
        self.set_start_end(start=t)
    
    def take_snapshot(self, model, t):
        """
        No snapshot to be returned for a power source
        """
        return {self.connectable.name: {}, "datetime": t.strftime("%Y-%m-%dT%H:%M")}
    
    def __str__(self, t=0):
        return "\t" * t + "[" + self.__str_time_range__() + "]"

class PowerSource(pleiad.structure.Connectable):
    
    def __init__(self, name, time_range="", price_structure_purchases=pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(0)), price_structure_sales=pleiad.utils.EmptyPriceStructure(), amount=None):
        super().__init__(name)   
        
        self.price_structure_purchases = price_structure_purchases
        self.price_structure_sales = price_structure_sales
        self.amount = amount
        
        # Creating one task by default
        self.tasks.append(PowerSourceTask(self, time_range))
        
        
    def __str__(self, t=0):
        """
        Representation of a power source
        """
        return "\t" * t + "| (Power source) " + self.name + " [" + ("Purchase:" + str(self.price_structure_purchases) if self.price_structure_purchases is not None else "") + "," + ("Sales:" + str(self.price_structure_sales) if self.price_structure_sales is not None else "") + "," + ("With set amount" if self.amount is not None else "") + "]: " + super().__str_tasks__(t)