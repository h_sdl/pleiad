#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 14:30:50 2019

@author: Hugues Souchard de Lavoreille
"""

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tkinter as tk
import pleiad.physicalobject
from . import plotting
import datetime
import numpy as np


class PlotConfigurator:
    
    def __init__(self):
        self.all_connectables = plotting.PlotParameters.appliances
        self.all_connectables_names = [i.name for i in self.all_connectables]
        self.all_nodes = plotting.PlotParameters.nodes
        self.all_nodes_names = [i.name for i in self.all_nodes]
        self.batteries = []
        self.powersources = []
        for s in self.all_connectables:
            if isinstance(s, pleiad.physicalobject.Storage):
                self.batteries.append(s)
            if isinstance(s, pleiad.physicalobject.PowerSource):
                self.powersources.append(s)
                
        # Time horizon indexes
        self.all_indexes = []
        for t in plotting.PlotParameters.indexes:
            self.all_indexes.append(t.strftime("%Y-%m-%d %H:%M"))
        
        self.indexes_start = self.all_indexes[:-1]
        self.indexes_end = self.all_indexes[1:]
        
        # Main window
        self.w = tk.Tk()
        self.w.title("Pleiad - " + plotting.PlotParameters.ts.__str_time_range__())
        
        self.w1 = None
        self.w2 = None
        self.w3 = None
        
        # Menu
#        menubar = tk.Menu(self.w)
#        menu1 = tk.Menu(menubar, tearoff=0)
#        menu1.add_command(label="Save chart", command=self.savechart)
#        menubar.add_cascade(label="File", menu=menu1)
#        self.w.config(menu=menubar)
        
        # Time horizon selection
        timeselector = tk.LabelFrame(self.w, text="Visualization horizon selection")
        timeselector.grid(row=0, column=0, columnspan=2)
        
        lbl_idxstart = tk.Label(timeselector, text="Start")
        lbl_idxstart.grid(row=0, column=0, sticky="e")
        
        self.vstart = tk.StringVar(timeselector)
        self.vstart.set(self.indexes_start[0])
        self.menustart = tk.OptionMenu(timeselector, self.vstart, *self.indexes_start, command=lambda _:self.startindexmenuchange())
        self.menustart.grid(row=0, column=1, sticky="w")
        
        lbl_idxend = tk.Label(timeselector, text="End")
        lbl_idxend.grid(row=1, column=0, sticky="e")
        
        self.vend = tk.StringVar(timeselector)
        self.vend.set(self.indexes_end[-1])
        self.menuend = tk.OptionMenu(timeselector, self.vend, *self.indexes_end, command=lambda _:self.endindexmenuchange())
        self.menuend.grid(row=1, column=1, sticky="w")
        
        lbl_period = tk.Label(timeselector, text="x-axis period")
        lbl_period.grid(row=2, column=0, sticky="e")
        
        self.vperiod = tk.StringVar(timeselector)
        self.menuperiod = tk.OptionMenu(timeselector, self.vperiod, 1, command=lambda _:self.periodchange())
        self.menuperiod.grid(row=2, column=1, sticky="w")
        self.update_possible_periods()
        
        self.gridvar = tk.IntVar()
        gridbutton = tk.Checkbutton(timeselector, text="Show grid", variable=self.gridvar)
        gridbutton.grid(row=3, column=0, columnspan=2)
        
        # Appliance selector
        appselector = tk.LabelFrame(self.w, text="Connectable selection")
        appselector.grid(row=1, column=0, columnspan=2)
        
        lblnode = tk.Label(appselector, text="Node")
        lblnode.grid(row=0, column=0, sticky="e")
        
        self.vnode = tk.StringVar(appselector)
        self.vnode.set(self.all_nodes_names[0])
        menunode = tk.OptionMenu(appselector, self.vnode, "All", *self.all_nodes_names, command=lambda _:self.populate_appliance_lists())
        menunode.grid(row=0, column=1, columnspan=2, sticky="w")
        
        lbl1 = tk.Label(appselector, text="Consumption graph")
        lbl1.grid(row=1, column=0, sticky="s")
        lbl2 = tk.Label(appselector, text="Schedule graph")
        lbl2.grid(row=3, column=0, sticky="s")
        lbl3 = tk.Label(appselector, text="Battery graph")
        lbl3.grid(row=1, column=1, sticky="s")
        lbl4 = tk.Label(appselector, text="Price graph")
        lbl4.grid(row=3, column=1, sticky="s")
        lbl5 = tk.Label(appselector, text="Costs graph")
        lbl5.grid(row=1, column=2, sticky="s")
        lbl5 = tk.Label(appselector, text="Virtual costs graph")
        lbl5.grid(row=3, column=2, sticky="s")
        self.l = tk.Listbox(appselector, selectmode="multiple", exportselection=0)
        self.l2 = tk.Listbox(appselector, selectmode="multiple", exportselection=0)
        self.l3 = tk.Listbox(appselector, selectmode="multiple", exportselection=0)
        self.l4 = tk.Listbox(appselector, selectmode="multiple", exportselection=0)
        self.l5 = tk.Listbox(appselector, selectmode="multiple", exportselection=0)
        self.l6 = tk.Listbox(appselector, selectmode="multiple", exportselection=0)
        self.l.grid(row=2, column=0, sticky="n")
        self.l2.grid(row=4, column=0, sticky="n")
        self.l3.grid(row=2, column=1, sticky="n")
        self.l4.grid(row=4, column=1, sticky="n")
        self.l5.grid(row=2, column=2, sticky="n")
        self.l6.grid(row=4, column=2, sticky="n")
        
        # Color picker
        colorpicker = tk.LabelFrame(self.w, text="Color picker")
        colorpicker.grid(row=3, column=0, columnspan=2)
        
        self.colorcanvas = tk.Canvas(colorpicker, width=100, height=50)
        self.colorcanvas.grid(row=0, column=3, columnspan=3)
        
        self.pickerlist = tk.Listbox(colorpicker, exportselection=0)
        self.pickerlist.grid(row=0, column=0, rowspan=6, columnspan=2)
        self.pickerlist.bind("<<ListboxSelect>>", lambda _:self.update_color_selected())
        
        self.populate_appliance_lists()
        
        labelred = tk.Label(colorpicker, text="Red")
        labelred.grid(row=1, column=3, sticky="e")
        bplusred = tk.Button(colorpicker, text="+", command=self.redplus)
        bplusred.grid(row=1, column=4)
        bminusred = tk.Button(colorpicker, text="-", command=self.redminus)
        bminusred.grid(row=1, column=5)
        
        labelgreen = tk.Label(colorpicker, text="Green")
        labelgreen.grid(row=2, column=3, sticky="e")
        bplusgreen = tk.Button(colorpicker, text="+", command=self.greenplus)
        bplusgreen.grid(row=2, column=4)
        bminusgreen = tk.Button(colorpicker, text="-", command=self.greenminus)
        bminusgreen.grid(row=2, column=5)
        
        labelblue = tk.Label(colorpicker, text="Blue")
        labelblue.grid(row=3, column=3, sticky="e")
        bplusblue = tk.Button(colorpicker, text="+", command=self.blueplus)
        bplusblue.grid(row=3, column=4)
        bminusblue = tk.Button(colorpicker, text="-", command=self.blueminus)
        bminusblue.grid(row=3, column=5)

        
        
        # Simulation button
        self.b = tk.Button(self.w, text="Show", command=self.goplot)
        self.b.grid(row=4, column=0, columnspan=2)
        
        
        # Snapshot button
#        snapselector = tk.LabelFrame(self.w, text="Snapshot taker")
#        snapselector.grid(row=3, column=0, columnspan=2)
#        
#        sb = tk.Button(snapselector, text="Take")
#        sb.grid(row=0, column=0)
    
    def startindexmenuchange(self):
        start = self.vstart.get()
        startplustsdelta = (datetime.datetime.strptime(start, "%Y-%m-%d %H:%M") + plotting.PlotParameters.ts.delta).strftime("%Y-%m-%d %H:%M")
        end = self.vend.get()
        
        if start >= end:
            self.vend.set(startplustsdelta)
            
        menu = self.menuend["menu"]
        menu.delete(0, "end")
        
        def f(value):
            self.vend.set(value);
            self.endindexmenuchange()
        
        for string in self.indexes_end[self.indexes_end.index(startplustsdelta):]:
            menu.add_command(label=string, command=lambda value=string:f(value))
            
        self.update_possible_periods()
    
    def endindexmenuchange(self):
        start = self.vstart.get()
        end = self.vend.get()
        endminustsdelta = (datetime.datetime.strptime(end, "%Y-%m-%d %H:%M") - plotting.PlotParameters.ts.delta).strftime("%Y-%m-%d %H:%M")
        
        if start >= end:
            self.vstart.set(endminustsdelta)
            
        menu = self.menustart["menu"]
        menu.delete(0, "end")
        
        def f(value):
            self.vstart.set(value)
            self.startindexmenuchange()
            
        for string in self.indexes_start[:self.indexes_start.index(endminustsdelta)+1]:
            menu.add_command(label=string, command=lambda value=string:f(value))
        
        self.update_possible_periods()
    
    def update_possible_periods(self):
        plotstart = plotting.PlotParameters.indexes[self.all_indexes.index(self.vstart.get())]
        plotend = plotting.PlotParameters.indexes[self.all_indexes.index(self.vend.get())]
        nb_intervals = int((plotend - plotstart) / plotting.PlotParameters.r_period)
        
        
        def str_minutes(t):
            reste_min = t % 60
            h = t // 60
            reste_h = h % 24
            d = h // 24
            
            s = ""
            if d != 0:
                s += str(int(d)) + "d"
            if reste_h != 0:
                s += str(int(reste_h)) + "h"
            if reste_min != 0:
                s += str(int(reste_min)) + "m"
            return s
            
        mini_nb_ticks = 5
        maxi_nb_ticks = 50
        desired_nb_ticks = 10
        mini_period = int(max(1, (nb_intervals) // maxi_nb_ticks))
        maxi_period = int(max(1, (nb_intervals) // mini_nb_ticks))
        desired_period = int(max(1, (nb_intervals) // desired_nb_ticks))
        self.possible_periods = [i for i in range(mini_period, maxi_period + 1)]
        self.possible_time_periods = [str_minutes((i * plotting.PlotParameters.r_period)/np.timedelta64(1, "m")) for i in range(mini_period, maxi_period + 1)]
        
        self.vperiod.set(self.possible_time_periods[self.possible_periods.index(desired_period)])
        menu = self.menuperiod["menu"]
        menu.delete(0, "end")
        
        def f(value):
            self.vperiod.set(value);
        
        for string in self.possible_time_periods:
            menu.add_command(label=string, command=lambda value=string:f(value))
    
    def populate_appliance_lists(self):
        if self.vnode.get() == "All":
            current_connectables = self.all_connectables
        else:
            current_node = self.all_nodes[self.all_nodes_names.index(self.vnode.get())]
            current_connectables = [i for i in current_node.get_connectables()]
        
        self.l.delete(0, 'end')
        self.l2.delete(0, 'end')
        self.l3.delete(0, 'end')
        self.l4.delete(0, 'end')
        self.l5.delete(0, 'end')
        self.l6.delete(0, 'end')
        
        for i in range(len(current_connectables)):
            obj_i = current_connectables[i]
            n_i = obj_i.name
            
            self.l.insert(i, n_i)
            self.l2.insert(i, n_i)
            if isinstance(obj_i, pleiad.physicalobject.Storage):
                self.l3.insert(i, n_i)
            if isinstance(obj_i, (pleiad.physicalobject.PowerSource)):
                self.l4.insert(i, n_i)
            self.l5.insert(i, n_i)
            self.l6.insert(i, n_i)
        
        self.l.select_set(0, 'end')
        self.l2.select_set(0, 'end')
        self.l3.select_set(0, 'end')
        self.l4.select_set(0, 'end')
        self.l5.select_set(0, 'end')
        self.l6.select_set(0, 'end')
        
        # Now the color picker
        self.pickerlist.delete(0, 'end')
        for i in range(len(current_connectables)):
            self.pickerlist.insert(i, current_connectables[i].name)
        self.pickerlist.select_set(0)
        self.update_color_selected()
    
    def update_color_selected(self):
        if len(self.pickerlist.curselection()) == 0:
            return
        #print(self.pickerlist.get(self.pickerlist.curselection()[0])
        self.colorconnectable = self.all_connectables[self.all_connectables_names.index(self.pickerlist.get(self.pickerlist.curselection()[0]))]
        self.colorconnectable_color = plotting.PlotParameters.colors[self.colorconnectable.id]
        def clamp(x): 
            return max(0, min(x, 255))  
        self.colorcanvas.create_rectangle(0, 0, 100, 50, fill="#{0:02x}{1:02x}{2:02x}".format(clamp(int(self.colorconnectable_color[0]*255)), clamp(int(self.colorconnectable_color[1]*255)), clamp(int(self.colorconnectable_color[2]*255))))
    
    def blueplus(self):
        self.colorconnectable_color[2] = min(1, self.colorconnectable_color[2] + 25/255)
        self.update_color_selected()
        
    def blueminus(self):
        self.colorconnectable_color[2] = max(0, self.colorconnectable_color[2] - 25/255)
        self.update_color_selected()
        
    def redplus(self):
        self.colorconnectable_color[0] = min(1, self.colorconnectable_color[0] + 25/255)
        self.update_color_selected()
        
    def redminus(self):
        self.colorconnectable_color[0] = max(0, self.colorconnectable_color[0] - 0.1)
        self.update_color_selected()
        
    def greenplus(self):
        self.colorconnectable_color[1] = min(1, self.colorconnectable_color[1] + 0.1)
        self.update_color_selected()
        
    def greenminus(self):
        self.colorconnectable_color[1] = max(0, self.colorconnectable_color[1] - 0.1)
        self.update_color_selected()
    
    def goplot(self):
        # Retrieving start and end of the plot
        plotting.PlotParameters.plotstart = plotting.PlotParameters.indexes[self.all_indexes.index(self.vstart.get())]
        plotting.PlotParameters.plotend = plotting.PlotParameters.indexes[self.all_indexes.index(self.vend.get())]
        plotting.PlotParameters.period = self.possible_periods[self.possible_time_periods.index(self.vperiod.get())]
        plotting.PlotParameters.time_period = plotting.PlotParameters.r_period * self.possible_periods[self.possible_time_periods.index(self.vperiod.get())]
        plotting.PlotParameters.gridactivated = bool(self.gridvar.get())
        
        tup1 = ()
        for s in self.l.curselection():
            tup1 = tup1 + (self.all_connectables[self.all_connectables_names.index(self.l.get(s))],)
        tup2 = ()
        for s in self.l2.curselection():
            tup2 = tup2 + (self.all_connectables[self.all_connectables_names.index(self.l2.get(s))],)
        tup3 = ()
        for s in self.l3.curselection():
            tup3 = tup3 + (self.all_connectables[self.all_connectables_names.index(self.l3.get(s))],)
        tup4 = ()
        for s in self.l4.curselection():
            tup4 = tup4 + (self.all_connectables[self.all_connectables_names.index(self.l4.get(s))],)
        tup5 = ()
        for s in self.l5.curselection():
            tup5 = tup5 + (self.all_connectables[self.all_connectables_names.index(self.l5.get(s))],)
        tup6= ()
        for s in self.l6.curselection():
            tup6 = tup6 + (self.all_connectables[self.all_connectables_names.index(self.l6.get(s))],)
        
        if self.w1 is not None:
            self.plot_widget1.destroy()
        if self.w1 is None or not self.w1.winfo_exists(): 
            self.w1=tk.Toplevel()
            self.w1.title("Energy / Schedule")
            
        if self.w2 is not None:
            self.plot_widget2.destroy()
        if self.w2 is None or not self.w2.winfo_exists(): 
            self.w2=tk.Toplevel()
            self.w2.title("Battery / Electricity price")
            
        if self.w3 is not None:
            self.plot_widget3.destroy()
        if self.w3 is None or not self.w3.winfo_exists(): 
            self.w3=tk.Toplevel()
            self.w3.title("Costs / Utility costs")
        
        fig1 = plotting.power_schedule_chart(tup1, tup2)
        self.canvas1 = FigureCanvasTkAgg(fig1, master=self.w1)
        self.plot_widget1 = self.canvas1.get_tk_widget()
        self.plot_widget1.pack(side="top",fill='both',expand=True)
        
        fig2 = plotting.battery_price_chart(tup3, tup4)
        self.canvas2 = FigureCanvasTkAgg(fig2, master=self.w2)
        self.plot_widget2 = self.canvas2.get_tk_widget()
        self.plot_widget2.pack(side="top",fill='both',expand=True)
        
        fig3 = plotting.costs_virtualcosts_chart(tup5, tup6)
        self.canvas3 = FigureCanvasTkAgg(fig3, master=self.w3)
        self.plot_widget3 = self.canvas3.get_tk_widget()
        self.plot_widget3.pack(side="top",fill='both',expand=True)
        
    def render(self):
        self.w.mainloop()