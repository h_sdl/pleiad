#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 27 11:42:56 2019

@author: Hugues Souchard de Lavoreille
"""

import numpy as np
import pandas as pd
import pleiad.physicalobject
#import matplotlib as mpl
#import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
import matplotlib.figure as mfig
import matplotlib.gridspec as mgridspec
import matplotlib.cm as mcm
#import datetime

class PlotParameters:
    # Dict : [connectable.id -> color]
    colors = []
    
    # Period for x-axis
    period = None
    time_period = None
    
    r = None
    r0 = None
    ts = None
    
    plotstart = None
    plotend = None
    indexes = None
    
    gridactivated = False

def initialize(_model):
    PlotParameters.r0 = _model.results
    PlotParameters.r = regularize_graph(PlotParameters.r0)
    PlotParameters.ts = _model.ts
    PlotParameters.appliances = [i for i in _model.localnetwork.get_all_nodes_and_connectables()]
    PlotParameters.nodes = [i for i in _model.localnetwork.get_nodes()]
    
    # Detecting frequency to have it graduated in hours (or more if not possible)
    r_period = np.mean(pd.Series(PlotParameters.r0.index[1:] - PlotParameters.r0.index[:-1]))
    nb_entries = len(PlotParameters.r0.index)
    desired_nb_ticks = 10
    desired_period = int(max(1, (nb_entries - 3) // desired_nb_ticks)) # -3 : removing the two extreme values and the last one (hills and holes)
    desired_time_period = desired_period * r_period
    PlotParameters.period = desired_period
    PlotParameters.time_period = desired_time_period
    PlotParameters.r_period = r_period
    
    PlotParameters.plotstart = PlotParameters.ts.start
    PlotParameters.plotend = PlotParameters.ts.end
    PlotParameters.indexes = PlotParameters.r0[PlotParameters.plotstart:PlotParameters.plotend].index
    
    n = pleiad.structure.Connectable.counter + pleiad.structure.ConnectableNode.counter
    np.random.seed(1)
    permutation = np.random.permutation(n)
    rainbow = mcm.rainbow(np.linspace(0, 1, n))
    PlotParameters.colors = [rainbow[permutation[i]] for i in range(n)]
    

def regularize_graph(r):
    ridx_shifted = r.index.shift(1) - pd.Timedelta(1, unit='s')
    r_shifted = pd.DataFrame(index=ridx_shifted, columns=r.columns)
    new_r = r.append(r_shifted).sort_index()
    for (sid, cname) in r.columns:
        column = (sid, cname)
        if cname == "Energy_stored_beginning":
            # This is a state
            new_r.loc[new_r.index[1:-1:2],column] = new_r.loc[new_r.index[2::2],column].values
            new_r.loc[new_r.index[-1],column] = r.loc[r.index[-1],column] + r.loc[r.index[-1], (sid, "energy_drawn")]
        else:
            # This is a flow
            new_r.loc[new_r.index[1::2],column] = new_r.loc[new_r.index[0::2],column].values
            
    return new_r
    


def bigplot(args_all, args_sched, args_batt, args_price):
    fig = mfig.Figure(figsize=(16, 8))
    fig.subplots_adjust(right=0.667)
    grid = mgridspec.GridSpec(6, 2, wspace=0.15, hspace=0.9)
    ax1 = fig.add_subplot(grid[:4,0])
    ax2 = fig.add_subplot(grid[4:,0])
    ax3 = fig.add_subplot(grid[:3,1])
    ax4 = fig.add_subplot(grid[3:,1])
    
    power_chart(args_all, ax1)
    schedule_chart(args_sched, ax2)
    battery_chart(args_batt, ax3)
    price_chart(args_price, ax4)
    
    return fig

def power_schedule_chart(args_energy, args_sched):
    fig = mfig.Figure(figsize=(6, 6))
    fig.subplots_adjust(right=0.667)
    grid = mgridspec.GridSpec(6, 2, wspace=0.15, hspace=1.4)
    ax1 = fig.add_subplot(grid[:4,:])
    ax2 = fig.add_subplot(grid[4:,:])
    
    power_chart(args_energy, ax1)
    schedule_chart(args_sched, ax2)
    
    return fig
    
def battery_price_chart(args_batt, args_price):
    fig = mfig.Figure(figsize=(6, 6))
    fig.subplots_adjust(right=0.667)
    grid = mgridspec.GridSpec(6, 2, wspace=0.15, hspace=1.4)
    ax1 = fig.add_subplot(grid[:3,:])
    ax2 = fig.add_subplot(grid[3:,:])
    
    battery_chart(args_batt, ax1)
    price_chart(args_price, ax2)
    
    return fig

def costs_virtualcosts_chart(args_batt, args_price):
    fig = mfig.Figure(figsize=(6, 6))
    fig.subplots_adjust(right=0.667)
    grid = mgridspec.GridSpec(6, 2, wspace=0.15, hspace=1.4)
    ax1 = fig.add_subplot(grid[:3,:])
    ax2 = fig.add_subplot(grid[3:,:])
    
    costs_chart(args_batt, ax1)
    vcosts_chart(args_price, ax2)
    
    return fig
    
    
def schedule_chart(args, ax0=None):    
    if ax0 is None:
        fig = mfig.Figure(figsize=(8, 4))
        fig.subplots_adjust(right=0.667)
        ax = fig.gca()
    else:
        ax = ax0
    
    count_app = 0
    conv_dev = {}
    
    ylab = []
    
    for app in args:
        if app.id not in conv_dev.keys():
            conv_dev[app.id] = count_app
            count_app += 1
            ylab.append(app.name)
            
        for t in PlotParameters.r0[PlotParameters.plotstart:PlotParameters.plotend-PlotParameters.ts.delta].index:
            nrj = PlotParameters.r0[app.name]['energy_drawn'].loc[t]
            if not(np.isnan(nrj.val)) and abs(nrj).val > 1e-6:
                if nrj.val > 0:                
                    hatch = "////"
                else:
                    hatch = ""
                ax.barh(y=conv_dev[app.id], width=PlotParameters.ts.delta.total_seconds(), left=t.timestamp(), color=PlotParameters.colors[app.id], hatch=hatch)   
            
    ax.set_yticks([i for i in range(len(ylab))])
    ax.set_yticklabels(ylab)
    
    xticks = [i for i in range(int(PlotParameters.plotstart.timestamp()), int(PlotParameters.plotend.timestamp())+1, int(PlotParameters.time_period/np.timedelta64(1, "s")))]
    if np.timedelta64(PlotParameters.plotend - PlotParameters.plotstart) % PlotParameters.time_period > np.timedelta64(1, "s"): xticks.append(int(PlotParameters.plotend.timestamp()))
    shift_bug_pandas = pd.Timestamp.fromtimestamp(PlotParameters.r0.index[1].timestamp())-PlotParameters.r0.index[1]
    xlabels = [(pd.Timestamp.fromtimestamp(i) - shift_bug_pandas).strftime("%H:%M") for i in xticks]
    ax.set_xticks(xticks)
    ax.set_xticklabels(xlabels, rotation=45)
    if PlotParameters.gridactivated: ax.xaxis.grid(which="major", linestyle="--", alpha=0.5)
    
    hatch_patch = mpatches.Patch(edgecolor="black", facecolor='white', hatch="////", label='Consuming')
    nohatch_patch = mpatches.Patch(edgecolor="black", facecolor='white', label='Providing')
    ax.legend(handles=[hatch_patch, nohatch_patch], loc="upper left", bbox_to_anchor=(1.05, 1))
    
    if ax0 is None:
        return fig
                

def battery_chart(args, ax0=None):
    if ax0 is None:
        fig = mfig.Figure(figsize=(8, 4))
        fig.subplots_adjust(right=0.667)
        ax = fig.gca()
    else:
        ax = ax0
    
    for i in args:
        ax.plot(PlotParameters.r[i.name]['Energy_stored_beginning'][PlotParameters.plotstart:PlotParameters.plotend]/pleiad.quantity.Energy(kWh=1), label=i.name + ' (stored)', c=PlotParameters.colors[i.id])
    
    ax.set_ylabel("Energy stored [kWh]")
    if len(args) != 0: ax.legend(loc="upper left", bbox_to_anchor=(1.05, 1))
    ax.set_xlabel("Time")
    xticks = PlotParameters.r[PlotParameters.plotstart:PlotParameters.plotend:2*PlotParameters.period].index
    if xticks[-1] != PlotParameters.plotend: xticks = list(xticks) + [PlotParameters.plotend]
    ax.set_xticks(xticks)
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax.xaxis.set_minor_formatter(mdates.DateFormatter("%H:%M"))
    ax.set_xlim(PlotParameters.plotstart, PlotParameters.plotend)
    if PlotParameters.gridactivated: ax.grid(which="major", linestyle="--", alpha=0.5)
    ax.tick_params(rotation=45)
    
    if ax0 is None:
        return fig
    
def price_chart(args, ax0=None):
    if ax0 is None:
        fig = mfig.Figure(figsize=(8, 4))
        fig.subplots_adjust(right=0.667)
        ax = fig.gca()
    else:
        ax = ax0
    
    linestyles = ["-", "--", ":", ".", "-."]
    
    for ps in args:
        pricedfP = ps.price_structure_purchases.get_prices_vect(PlotParameters.r[PlotParameters.plotstart:PlotParameters.plotend].index)
        pricedfS = ps.price_structure_sales.get_prices_vect(PlotParameters.r[PlotParameters.plotstart:PlotParameters.plotend].index)
        i_f = 0
        for i in range(len(pricedfP.columns)):
            ax.plot(pricedfP.iloc[:,i]/pleiad.quantity.EnergyPrice(cUSDpkWh=1), label=ps.name + ' (' + pricedfP.columns[i] + ' purchases price)', linestyle=linestyles[i], c=PlotParameters.colors[ps.id])
            i_f = i
        for i in range(len(pricedfS.columns)):
            ax.plot(pricedfS.iloc[:,i]/pleiad.quantity.EnergyPrice(cUSDpkWh=1), label=ps.name + ' (' + pricedfS.columns[i] + ' sales price)', linestyle=linestyles[i_f + i + 1], c=PlotParameters.colors[ps.id])
    
    
    ax.set_ylabel("Electricity price [¢/kWh]")
    if len(args) != 0: ax.legend(loc="upper left", bbox_to_anchor=(1.05, 1))
    ax.set_xlabel("Time")
    xticks = PlotParameters.r[PlotParameters.plotstart:PlotParameters.plotend:2*PlotParameters.period].index
    if xticks[-1] != PlotParameters.plotend: xticks = list(xticks) + [PlotParameters.plotend]
    ax.set_xticks(xticks)
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax.xaxis.set_minor_formatter(mdates.DateFormatter("%H:%M"))
    ax.set_xlim(PlotParameters.plotstart, PlotParameters.plotend)
    if PlotParameters.gridactivated: ax.grid(which="major", linestyle="--", alpha=0.5)
    ax.tick_params(rotation=45)
    
    if ax0 is None:
        return fig

def power_chart(args, ax0=None):
    if ax0 is None:
        fig = mfig.Figure(figsize=(8, 4))
        fig.subplots_adjust(right=0.667)
        ax = fig.gca()
    else:
        ax = ax0
    
    for i in args:
        ax.plot(PlotParameters.r[i.name]['energy_drawn'][PlotParameters.plotstart:PlotParameters.plotend]/PlotParameters.ts.delta/pleiad.quantity.Power(kW=1), label=i.name + ' (consumed)', c=PlotParameters.colors[i.id])
    
    ax.set_ylabel("Power [kW]")
    if len(args) != 0: ax.legend(loc="upper left", bbox_to_anchor=(1.05, 1))
    ax.set_xlabel("Time")
    xticks = PlotParameters.r[PlotParameters.plotstart:PlotParameters.plotend:2*PlotParameters.period].index
    if xticks[-1] != PlotParameters.plotend: xticks = list(xticks) + [PlotParameters.plotend]
    ax.set_xticks(xticks)
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax.xaxis.set_minor_formatter(mdates.DateFormatter("%H:%M"))
    ax.set_xlim(PlotParameters.plotstart, PlotParameters.plotend)
    if PlotParameters.gridactivated: ax.grid(which="major", linestyle="--", alpha=0.5)
    ax.tick_params(rotation=45)
    
    if ax0 is None:
        return fig
    
def costs_chart(args, ax0=None):
    if ax0 is None:
        fig = mfig.Figure(figsize=(8, 4))
        fig.subplots_adjust(right=0.667)
        ax = fig.gca()
    else:
        ax = ax0
    
    for i in args:
        ax.plot(PlotParameters.r[i.name]['cost'][PlotParameters.plotstart:PlotParameters.plotend]/pleiad.quantity.Price(cUSD=1), label=i.name, c=PlotParameters.colors[i.id])
    
    ax.set_ylabel("Instant cost [c$/" + str(int(PlotParameters.ts.delta.total_seconds()/60)) + "min]")
    if len(args) != 0: ax.legend(loc="upper left", bbox_to_anchor=(1.05, 1))
    ax.set_xlabel("Time")
    xticks = PlotParameters.r[PlotParameters.plotstart:PlotParameters.plotend:2*PlotParameters.period].index
    if xticks[-1] != PlotParameters.plotend: xticks = list(xticks) + [PlotParameters.plotend]
    ax.set_xticks(xticks)
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax.xaxis.set_minor_formatter(mdates.DateFormatter("%H:%M"))
    ax.set_xlim(PlotParameters.plotstart, PlotParameters.plotend)
    if PlotParameters.gridactivated: ax.grid(which="major", linestyle="--", alpha=0.5)
    ax.tick_params(rotation=45)
    
    if ax0 is None:
        return fig
    
def vcosts_chart(args, ax0=None):
    if ax0 is None:
        fig = mfig.Figure(figsize=(8, 4))
        fig.subplots_adjust(right=0.667)
        ax = fig.gca()
    else:
        ax = ax0
    
    for i in args:
        ax.plot(PlotParameters.r[i.name]['virtual_cost'][PlotParameters.plotstart:PlotParameters.plotend]/pleiad.quantity.Price(cUSD=1), label=i.name, c=PlotParameters.colors[i.id])
    
    ax.set_ylabel("Instant virtual cost [c$/" + str(int(PlotParameters.ts.delta.total_seconds()/60)) + "min]")
    if len(args) != 0: ax.legend(loc="upper left", bbox_to_anchor=(1.05, 1))
    ax.set_xlabel("Time")
    xticks = PlotParameters.r[PlotParameters.plotstart:PlotParameters.plotend:2*PlotParameters.period].index
    if xticks[-1] != PlotParameters.plotend: xticks = list(xticks) + [PlotParameters.plotend]
    ax.set_xticks(xticks)
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax.xaxis.set_minor_formatter(mdates.DateFormatter("%H:%M"))
    ax.set_xlim(PlotParameters.plotstart, PlotParameters.plotend)
    if PlotParameters.gridactivated: ax.grid(which="major", linestyle="--", alpha=0.5)
    ax.tick_params(rotation=45)
    
    if ax0 is None:
        return fig