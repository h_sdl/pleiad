#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  7 14:35:10 2019

@author: Hugues Souchard de Lavoreille
"""

import pleiad.utils
import pleiad.quantity
import datetime

EDFFeedInSmallISB2019 = pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(cEURpkWh=18.66))
EDFFeedInMidISB2019 = pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(cEURpkWh=15.86))
EDFPurchaseConstant2019 = pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(cEURpkWh=15.55))

def f1(time):
    if not isinstance(time, datetime.datetime):
        raise Exception()
    if time.hour < 7:
        return pleiad.quantity.EnergyPrice(cEURpkWh=13.20)
    if time.hour < 23:
        return pleiad.quantity.EnergyPrice(cEURpkWh=17.10)
    return pleiad.quantity.EnergyPrice(cEURpkWh=13.20)

EDFPurchaseTOU2019 = pleiad.utils.TOUPriceStructure(f1)

GermanyFeedInMidSept2019 = pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(cEURpkWh=10.33))
BerlinEONPurchaseConstantSept2019 = pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(cEURpkWh=25.99))


def tou_ontario_s2019(time):
    if not isinstance(time, datetime.datetime):
        raise Exception()
    
    p0 = pleiad.quantity.EnergyPrice(cCADpkWh=6.5)
    p1 = pleiad.quantity.EnergyPrice(cCADpkWh=9.4)
    p2 = pleiad.quantity.EnergyPrice(cCADpkWh=13.4)    
    
    if time.weekday() == 5 or time.weekday() == 6 or time.hour < 7 or time.hour > 19:
        return p0
    if time.hour < 11 or time.hour > 17:
        return p1
    return p2
OntarioTOUSummer2019 = pleiad.utils.TOUPriceStructure(tou_ontario_s2019)

def tou_ontario_w2020(time):
    if not isinstance(time, datetime.datetime):
        raise Exception()
    
    p0 = pleiad.quantity.EnergyPrice(cCADpkWh=10.1)
    p1 = pleiad.quantity.EnergyPrice(cCADpkWh=14.4)
    p2 = pleiad.quantity.EnergyPrice(cCADpkWh=20.8)    
    
    if time.weekday() == 5 or time.weekday() == 6 or time.hour < 7 or time.hour > 19:
        return p0
    if time.hour < 11 or time.hour > 17:
        return p1
    return p2

OntarioTOUWinter2020 = pleiad.utils.TOUPriceStructure(tou_ontario_w2020)


def tou_ontario_w2020_modified(time):
    if not isinstance(time, datetime.datetime):
        raise Exception()
    
    p0 = pleiad.quantity.EnergyPrice(cCADpkWh=14.1)
    p1 = pleiad.quantity.EnergyPrice(cCADpkWh=18.4)
    p2 = pleiad.quantity.EnergyPrice(cCADpkWh=24.8)   
    
    if time.weekday() == 5 or time.weekday() == 6 or time.hour < 7 or time.hour > 19:
        return p0
    if time.hour < 11 or time.hour > 17:
        return p1
    return p2

OntarioTLOUWinter2020 = pleiad.utils.TLOUPriceStructure(tou_ontario_w2020, tou_ontario_w2020_modified, pleiad.quantity.Power(kW=9))


OntarioConstantWinter2020 = pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(cCADpkWh=10.1))


Quebec = pleiad.utils.BlockPriceStructure(
    pleiad.quantity.Energy(kWh=40), 
    pleiad.quantity.EnergyPrice(cCADpkWh=6.08), 
    pleiad.quantity.EnergyPrice(cCADpkWh=9.38), 
    None # Hardcoded : 1 day
)