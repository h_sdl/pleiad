#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 15:44:29 2019

@author: Hugues Souchard de Lavoreille
"""

import datetime

import pandas as pd

from pvlib.forecast import GFS
from pvlib.pvsystem import PVSystem, retrieve_sam
from pvlib.modelchain import ModelChain
from pvlib.location import Location

import pleiad.quantity

#############
# PV system #
#############
n_pv = 0

# TODO License copernicus
# TODO License pvlib + siphon? + paper Kc


def gen_pv_pvlib(ts, name=None, lat=45.5, lon=-73.62, tz='America/Toronto', modules_per_string=8, strings_per_inverter=2):
    """
    Function used to generate a PV system operating during the time horizon defined by ts
    
    Parameters
    ----------
        ts : TimeDivision object
        name (optional) : name for the Connectable
        lat (optional) : latitude of the PV system
        lon (optional) : longitude of the PV system
        tz (optional) : time zone of the PV system
        modules_per_string (optional) : number of PV modules per PV string
        strings_per_inverter (optional) : number of PV strings for the inverter
    
    Returns
    -------
        PV : pleiad.physicalobject.PowerSource object
    """
    
    global n_pv
    n_pv = n_pv + 1
    
    if name is None:
        name = "Photovoltaic#" + str(n_pv)
        
    d1 = pd.Timestamp(ts.start, tz=tz).replace(second=0, microsecond=0)
    d2 = pd.Timestamp(ts.end, tz=tz).replace(second=0, microsecond=0)
    
    # Fetching CEC databases
    cec_modules = retrieve_sam('cecmod')
    cec_inverters = retrieve_sam('cecinverter')
    
    # Taking a Jinko solar panel of 270Wp
    module = cec_modules['Jinko_Solar_Co___Ltd_JKM270PP_60']
    
    # Taking a SMA Sunny Boy inverter of 5 kW
    inverter = cec_inverters['SMA_America__SB5_0_1SP_US_40__240V_']
    
    # Building the PV system
    system = PVSystem(module_parameters=module, inverter_parameters=inverter, modules_per_string=modules_per_string, strings_per_inverter=strings_per_inverter)
    
    # Getting clear sky data
    # TODO We should take altitude into account - SRTM
    loc = Location(latitude=lat, longitude=lon, tz=tz)
    clearsky = loc.get_clearsky(pd.date_range(d1, d2, freq="T"))
    
    # Getting forecast data
    day = datetime.timedelta(days=1)
    now = pd.Timestamp.now(tz=tz)
    
    # In the following, we take the forecast data in a window [now-14days,now+10days]
    # Out of this window, clear sky data is taken
    if d2 < now - 14 * day or d1 > now + 10 * day:
        weather = clearsky
    else:
        if d1 > now - 14 * day and d2 < now + 10 * day:
            fcd1 = d1
            fcd2 = d2
        elif d2 < now + 10 * day:
            fcd1 = now - 14 * day
            fcd2 = d2
        elif d1 > now - 14 * day:
            fcd1 = d1
            fcd2 = now + 10 * day
        else:
            fcd1 = now - 14 * day
            fcd2 = now + 10 * day
        
        # Fetching forecast data from the GFS model, on the web (margin of 6 hours just in case)
        fx_model = GFS()
        fx_data = fx_model.get_processed_data(lat, lon, fcd1 - datetime.timedelta(hours=6), fcd2 + datetime.timedelta(hours=6))
        
        # Forecast data is given every 3 hours
        # We divide them by th eclear sky data, interpolate and eventually multiply by the clear sky data
        clearsky.columns = ["ghicc", "dnicc", "dhicc"]
        clearsky["ghikc"] = fx_data["ghi"] / clearsky["ghicc"]
        clearsky["dhikc"] = fx_data["dhi"] / clearsky["dhicc"]
        clearsky["dnikc"] = fx_data["dni"] / clearsky["dnicc"]
        clearsky.interpolate(limit_direction="both", inplace=True)
        clearsky.loc[(clearsky.index < fcd1) | (clearsky.index > fcd2), ["ghikc", "dhikc", "dnikc"]] = 1
        clearsky["cdni"] = clearsky["dnikc"] * clearsky["dnicc"]
        clearsky["cdhi"] = clearsky["dhikc"] * clearsky["dhicc"]
        clearsky["cghi"] = clearsky["ghikc"] * clearsky["ghicc"]
        
        # Finally extracting the weather data minute-wise, interpolated
        weather = clearsky.loc[:,["cdni", "cdhi", "cghi"]]
        weather.columns = ["dni", "dhi", "ghi"]
    
    # Computing PV AC output
    mc = ModelChain(system, loc, aoi_model="no_loss", spectral_model="no_loss")
    mc.run_model(weather);
    ac = mc.ac.fillna(0)
    ac[ac<0] = 0 # Negative data (inverter + module consumption) is neglected here in our model
    
    def prod(t):
        t=pd.Timestamp(t.strftime("%Y-%m-%dT%H:%M"), tz=tz)
        
        if t < d1 or t > d2:
            raise Exception("Datetime out of bounds")
        
        return pleiad.quantity.Power(W=ac[t])
    
    return pleiad.physicalobject.PowerSource(name, amount=prod, price_structure_sales=pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(0)))
    