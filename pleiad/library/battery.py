#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 15:46:17 2019

@author: Hugues Souchard de Lavoreille
"""

import datetime

import pleiad.quantity


###########
# Battery #
###########
# https://www.tesla.com/powerwall
# We take continuous power
# Round-trip efficiency = charging efficienty * discharging efficiency. We suppose the losses are equally balanced between charge and discharge
# Self discharge rate from wikipedia for lithium-ion batteries : https://en.wikipedia.org/wiki/Self-discharge
#     (supposing exponential self-discharge for the day value)
n_batteries = 0

def PowerWall(name=None):
    global n_batteries
    n_batteries = n_batteries + 1
    
    if name is None:
        name = "Battery#" + str(n_batteries)
    return pleiad.physicalobject.Storage(name, \
    e_max=pleiad.quantity.Energy(kWh=13.5), \
    p_max=pleiad.quantity.Power(kW=5), \
    self_discharge_rate_per_day=0.0008, \
    charging_efficiency=0.95, \
    discharging_efficiency=0.95).add_task(cycle_limit=2, \
        duration_for_cycle_limit=datetime.timedelta(days=1), \
        soc_constraints=[], \
        same_soc_start_end=True)
    



##############
# EV Battery #
##############
# We estimate that 130km in average are needed per day
# If 20kWh are consumed for 100km, the car needs to have at least 70% of its capacity for a daily trip.
# We charge it from 30% to 100% overnight
# https://branchezvous.org/wp-content/uploads/2019/01/RoulonsElectrique_brochure2019v2.pdf
    
def gen_ev_battery(ts, name=None):
    global n_batteries
    n_batteries = n_batteries + 1
    
    if name is None:
        name = "EV_battery#" + str(n_batteries)
    
    d1 = ts.start
    d2 = ts.end
    
    o = pleiad.physicalobject.Storage(name, e_max=pleiad.quantity.Energy(kWh=40), p_max=pleiad.quantity.Power(kW=8), charging_efficiency=0.95, discharging_efficiency=0.95)
    
    start = d1
    while start < d2:
        if start.hour > 18 or start.hour < 8:
            begin = start
            stop = min((begin + datetime.timedelta(hours=10)).replace(hour=8, minute=0, second=0, microsecond=0), d2)
            normalstop = (begin + datetime.timedelta(hours=10)).replace(hour=8, minute=0, second=0, microsecond=0)
            normalbegin = normalstop - datetime.timedelta(hours=14)
        else:
            begin = start.replace(hour=18, minute=0, second=0, microsecond=0)
            stop = min(begin + datetime.timedelta(hours=14), d2)
            normalbegin = begin
            normalstop = begin + datetime.timedelta(hours=14)
        
        lvlstop = round(100 - abs((stop - normalstop) / datetime.timedelta(hours=14)) * 70)
        lvlbegin = round(30 + abs((begin - normalbegin) / datetime.timedelta(hours=14)) * 70)
            
        
        o.add_task(time_range=begin.strftime("%Y-%m-%dT%H:%M") + "/" + stop.strftime("%Y-%m-%dT%H:%M"),
            cycle_limit=2,
            duration_for_cycle_limit=datetime.timedelta(hours=14),
            same_soc_start_end=False,
            soc_constraints=[begin.strftime("%Y-%m-%dT%H:%M") + "=" + str(lvlbegin) + "%", stop.strftime("%Y-%m-%dT%H:%M") + "=" + str(lvlstop) + "%"]
        )
        
        start = stop.replace(hour=12)
        
    return o
#        add_task(time_range="2017-01-01T17:00/2017-01-02T08:00", \
#            cycle_limit=2, \
#            soc_constraints=["2017-01-01T18:00=15%", "2017-01-01T18:00/2017-01-01T20:00>10%", "2017-01-02T08:00>70%", "2017-01-01T20:00/2017-01-01T21:00>30%"], \
#            same_soc_start_end=False)