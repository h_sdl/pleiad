#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 14:00:32 2019

@author: Hugues Souchard de Lavoreille

This file gathers several examples of appliances with real-world characteristics, to be used by pleiad in test systems
Data is partly sourced, partly invented with "reasonable" characteristics

"""

import datetime
import random as rd

import pleiad.quantity
import pleiad.physicalobject

##########
# Fridge #
##########

n_fridge = 0
p_max_avg_fridge = pleiad.quantity.Power(W=120)
p_max_std_fridge = pleiad.quantity.Power(W=20)
duty_cycle_avg_fridge = 0.5
duty_cycle_std_fridge = 0.3
period_avg_fridge = datetime.timedelta(hours=1)
period_std_fridge = datetime.timedelta(minutes=30)

def gen_random_fridge(ts, name=None):
    p_max = pleiad.quantity.Power(rd.gauss(p_max_avg_fridge.val, p_max_std_fridge.val)).__round__(0, "W")
    duty_cycle = min(max(rd.gauss(duty_cycle_avg_fridge, duty_cycle_std_fridge), 0), 1)
    period = datetime.timedelta(seconds=int(rd.gauss(period_avg_fridge.total_seconds(), period_std_fridge.total_seconds())))
    
    return gen_fridge(ts, p_max, duty_cycle, period, name)

def gen_fridge(ts, p_max=None, duty_cycle=None, period=None, name=None):
    global n_fridge
    n_fridge = n_fridge + 1
    
    if name is None:
        name = "Fridge#" + str(n_fridge)
        
    d1 = ts.start
    d2 = ts.end
    
    defin0 = {}
    definition = {"patterns": {"regular" : defin0}}
    
    if p_max is None: p_max = p_max_avg_fridge
    if duty_cycle is None: duty_cycle = duty_cycle_avg_fridge
    if period is None: period = period_avg_fridge
    
    duration_compressor_on = datetime.timedelta(seconds=int((duty_cycle * period).total_seconds()))
    duration_compressor_off =  period - duration_compressor_on
    
    on = True
    now = d1
    while now < d2:
        if on:
            newnow = min(now + duration_compressor_on, d2)
            defin0[now.strftime("%Y-%m-%dT%H:%M") + "/" + newnow.strftime("%Y-%m-%dT%H:%M")] = str(p_max)
            on = False
            now = newnow
        else:
            now = min(now + duration_compressor_off, d2)
            on = True
            
    
    return pleiad.physicalobject.ThermalAppliance(name).add_task(definition)


################
# Water heater #
################

n_waterheater = 0
n_ppl_avg_waterheater = 3
n_ppl_std_waterheater = 2
p_avg_waterheater = pleiad.quantity.Power(kW=2.5)
p_std_waterheater = pleiad.quantity.Power(kW=0.5)
consumption_per_ppl_and_day_avg_waterheater = pleiad.quantity.Energy(kWh=2.5)
consumption_per_ppl_and_day_std_waterheater = pleiad.quantity.Energy(kWh=1)

def gen_random_waterheater(ts, name=None, ts_compatible=True):
    p = rd.gauss(p_avg_waterheater, p_std_waterheater).__round__(0, "W")
    n_ppl = max(0, int(rd.gauss(n_ppl_avg_waterheater, n_ppl_std_waterheater)))
    cons = pleiad.quantity.Energy(rd.gauss(consumption_per_ppl_and_day_avg_waterheater.val, consumption_per_ppl_and_day_std_waterheater.val))
    
    if ts_compatible:
        quantum_consumption = p * ts.delta
        print(quantum_consumption)
        k = (n_ppl * cons) / quantum_consumption
        print(round(k))
        e_max = round(k) * quantum_consumption
    else:
        e_max = None
    
    print(p, n_ppl, cons, e_max)
    return gen_waterheater(ts, p, n_ppl, cons, e_max, name, ts_compatible)

def gen_waterheater(ts, p=None, n_ppl=None, consumption_per_ppl_and_day=None, e_max=None, name=None, ts_compatible=True):
    global n_waterheater
    n_waterheater = n_waterheater + 1
    
    if name is None:
        name = "WaterHeater#" + str(n_waterheater)
    
    d1 = ts.start
    d2 = ts.end
    
    
    if n_ppl is None: n_ppl = n_ppl_avg_waterheater
    if p is None: p = p_avg_waterheater
    if consumption_per_ppl_and_day is None: consumption_per_ppl_and_day = consumption_per_ppl_and_day_avg_waterheater
    if e_max is None: e_max = n_ppl * consumption_per_ppl_and_day
    
    if ts_compatible:
        quantum_consumption = p * ts.delta
    
    o = pleiad.physicalobject.FlexibleAppliance(name)
    
    start = d1
    end = min(d2, (start + datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0))
    while start < d2:
        if ts_compatible:
            conso_e_max = round((e_max * ((end - start) / datetime.timedelta(days=1))) / quantum_consumption) * quantum_consumption
        else:
            conso_e_max = (e_max * ((end - start) / datetime.timedelta(days=1))).__round__(0, "Wh")
        o.add_task("[" + start.strftime("%Y-%m-%dT%H:%M") + "/" + end.strftime("%Y-%m-%dT%H:%M") + "]0W|" + str(p) + "=" + str(conso_e_max))
        start = end
        end = min(d2, end + datetime.timedelta(days=1))

    return o
    

########
# Oven #
########
n_oven = 0
p_avg_oven = pleiad.quantity.Power(kW=3)
p_std_oven = pleiad.quantity.Power(kW=1)
dur_avg_oven = datetime.timedelta(minutes=30)
dur_std_oven = datetime.timedelta(minutes=10)
nb_use_per_weekend_avg_oven = 1.2
nb_use_per_weekend_std_oven = 1
nb_use_per_week_avg_oven = 2.3
nb_use_per_week_std_oven = 1
hour_launch_weekday_avg_oven = datetime.timedelta(hours=18, minutes=0)
hour_launch_weekday_std_oven = datetime.timedelta(minutes=30)
hour_launch_weekend_avg_oven = datetime.timedelta(hours=11, minutes=30)
hour_launch_weekend_std_oven = datetime.timedelta(hours=3)


def gen_oven(ts, p=None, duration=None, hour_launch_weekday=None, hour_launch_weekend=None, name=None):
    global n_oven
    n_oven = n_oven + 1
    
    if name is None:
        name = "Oven#" + str(n_oven)
    
    d1 = ts.start
    d2 = ts.end
    
    if p is None: p = p_avg_oven
    if duration is None: duration = dur_avg_oven
    if hour_launch_weekday is None: hour_launch_weekday = hour_launch_weekday_avg_oven
    if hour_launch_weekend is None: hour_launch_weekend = hour_launch_weekend_avg_oven
        
    o = pleiad.physicalobject.ActivityBasedAppliance(name)
    
    start = d1
    while start < d2:
        run_today = False
        
        # TODO Still temporarily setting 2 days in week. Change for random
        if start.weekday() == 1:
            oven_start = start.replace(hour=0, minute=0, second=0, microsecond=0) + hour_launch_weekday
            run_today = True
        elif start.weekday() == 6:
            oven_start = start.replace(hour=0, minute=0, second=0, microsecond=0) + hour_launch_weekend
            run_today = True
            
        if run_today:
            oven_end = oven_start + duration
            
            acceptable_oven_start = oven_start - datetime.timedelta(hours=2)
            acceptable_oven_end = oven_end + datetime.timedelta(hours=2)
            
            if acceptable_oven_end > start and acceptable_oven_start < d2:
                o.add_task("[" + acceptable_oven_start.strftime("%Y-%m-%dT%H:%M") + "/" + acceptable_oven_end.strftime("%Y-%m-%dT%H:%M") + "]" + str(int(duration.total_seconds() / 60)) + "m@" + str(p))
                o.set_preferred(oven_start.strftime("%Y-%m-%dT%H:%M") + "/" + oven_end.strftime("%Y-%m-%dT%H:%M") + ",1cUSDph")
            
        start = (start + datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
        
    return o

#################
# Cooking stove #
#################
n_stove = 0
p_avg_stove = pleiad.quantity.Power(W=1500)
duration_avg_stove = datetime.timedelta(minutes=15)
launch_time_weekday_noon_avg = datetime.timedelta(hours=12, minutes=0)
launch_time_weekday_evening_avg = datetime.timedelta(hours=18, minutes=30)
launch_time_weekend_noon_avg = datetime.timedelta(hours=12, minutes=30)
launch_time_weekend_evening_avg = datetime.timedelta(hours=19)

def gen_stove(ts, p=None, duration=None, ltwn=None, ltwe=None, lten=None, ltee=None, name=None):
    global n_stove
    n_stove = n_stove + 1
    
    if name is None:
        name = "Stove#" + str(n_stove)
        
    d1 = ts.start
    d2 = ts.end
    
    if p is None: p = p_avg_stove
    if duration is None: duration = duration_avg_stove
    if ltwn is None: ltwn = launch_time_weekday_noon_avg
    if ltwe is None: ltwe = launch_time_weekday_evening_avg
    if lten is None: lten = launch_time_weekend_noon_avg
    if ltee is None: ltee = launch_time_weekend_evening_avg
        
    o = pleiad.physicalobject.ActivityBasedAppliance(name)
    
    start = d1
    while start < d2:
        
        if start.weekday() < 5:
            stove_start_1 = start.replace(hour=0, minute=0, second=0, microsecond=0) + ltwn
            stove_start_2 = start.replace(hour=0, minute=0, second=0, microsecond=0) + ltwe
        else:
            stove_start_1 = start.replace(hour=0, minute=0, second=0, microsecond=0) + lten
            stove_start_2 = start.replace(hour=0, minute=0, second=0, microsecond=0) + ltee
            
        stove_end_1 = stove_start_1 + duration
        stove_end_2 = stove_start_2 + duration
        
        acceptable_stove_start_1 = stove_start_1 - datetime.timedelta(hours=0.5)
        acceptable_stove_end_1 = stove_end_1 + datetime.timedelta(hours=0.5)
        acceptable_stove_start_2 = stove_start_2 - datetime.timedelta(hours=0.5)
        acceptable_stove_end_2 = stove_end_2 + datetime.timedelta(hours=0.5)
        
        if acceptable_stove_end_1 > start and acceptable_stove_start_1 < d2:
            o.add_task("[" + acceptable_stove_start_1.strftime("%Y-%m-%dT%H:%M") + "/" + acceptable_stove_end_1.strftime("%Y-%m-%dT%H:%M") + "]" + str(int(duration.total_seconds() / 60)) + "m@" + str(p))
            o.set_preferred(stove_start_1.strftime("%Y-%m-%dT%H:%M") + "/" + stove_end_1.strftime("%Y-%m-%dT%H:%M") + ",1cUSDph")
        
        if acceptable_stove_end_2 > start and acceptable_stove_start_2 < d2:
            o.add_task("[" + acceptable_stove_start_2.strftime("%Y-%m-%dT%H:%M") + "/" + acceptable_stove_end_2.strftime("%Y-%m-%dT%H:%M") + "]" + str(int(duration.total_seconds() / 60)) + "m@" + str(p))
            o.set_preferred(stove_start_2.strftime("%Y-%m-%dT%H:%M") + "/" + stove_end_2.strftime("%Y-%m-%dT%H:%M") + ",1cUSDph")
            
        start = (start + datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
        
    return o

##############
# Dishwasher #
##############

n_dishwasher = 0
p_op_avg_dishwasher = pleiad.quantity.Power(W=300)
p_heat_avg_dishwasher = pleiad.quantity.Power(kW=1.2)
p_dry_avg_dishwasher = pleiad.quantity.Power(W=600)

# Fig 10 Pipattanasomporn et al.
# 15/300 10/1200 25/300 wash
# 15/1200 15/300 rinse
# 20/600  dry

def gen_dishwasher(ts, p_op=None, p_heat=None, p_dry=None, name=None):
    global n_dishwasher
    n_dishwasher = n_dishwasher + 1
    
    if name is None:
        name = "Dishwasher#" + str(n_dishwasher)
    
    d1 = ts.start
    d2 = ts.end
    
    if p_op is None: p_op = p_op_avg_dishwasher
    if p_heat is None: p_heat = p_heat_avg_dishwasher
    if p_dry is None: p_dry = p_dry_avg_dishwasher
    
    o = pleiad.physicalobject.ActivityBasedAppliance(name)
    
    start = d1 - datetime.timedelta(days=1)
    
    while start < d2:
        dw_acceptable_start = start.replace(hour=19, minute=30, second=0, microsecond=0) # 7:30pm
        dw_acceptable_end = dw_acceptable_start + datetime.timedelta(hours=11) # 6:30am the next day
        
        if dw_acceptable_end > d1 and dw_acceptable_start < d2:
            o.add_task("[" + dw_acceptable_start.strftime("%Y-%m-%dT%H:%M") + "/" + dw_acceptable_end.strftime("%Y-%m-%dT%H:%M") + "]15m@" + str(p_op) + ">15m@" + str(p_heat) + ">30m@" + str(p_op) + "(-)15m@" + str(p_heat) + ">15m@" + str(p_op) + "(-)30m@" + str(p_dry) + ">")
        
        start = (start + datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
        
    return o


###########################
# Washing machine + Dryer #
###########################
# Modelled together, so that the drying phase happens after the washing phase

n_washingmachine = 0
p_wash_rinse_avg_washingmachine = pleiad.quantity.Power(W=300)
p_spin_avg_washingmachine = pleiad.quantity.Power(W=400)
p_avg_dryer = pleiad.quantity.Power(W=4000)
# Pipattanasomporn et al
# 30m wash, 15m rinse, 15m spin
# Dryer: 4000W, 30m (can be cut into 2 parts)

# TODO Too heavy calculations with this modelling

def gen_washingmachine_dryer(ts, p_wash_rinse=None, p_spin=None, p_dry=None, name=None):
    global n_washingmachine
    n_washingmachine = n_washingmachine + 1
    
    if name is None:
        name = "WashingMachine#" + str(n_washingmachine)
    
    d1 = ts.start
    d2 = ts.end
    
    if p_wash_rinse is None: p_wash_rinse = p_wash_rinse_avg_washingmachine
    if p_spin is None: p_spin = p_spin_avg_washingmachine
    if p_dry is None: p_dry = p_avg_dryer
    
    o = pleiad.physicalobject.ActivityBasedAppliance(name)
    
    start = d1
    
    while start < d2:
        
        if start.weekday() == 5 or start.weekday() == 6:
            wmd_acceptable_start = start.replace(hour=10, minute=00, second=0, microsecond=0) # 10am
            wmd_acceptable_end = wmd_acceptable_start + datetime.timedelta(hours=11) # 9pm the same day
            
            if wmd_acceptable_end > d1 and wmd_acceptable_start < d2:
                o.add_task("[" + wmd_acceptable_start.strftime("%Y-%m-%dT%H:%M") + "/" + wmd_acceptable_end.strftime("%Y-%m-%dT%H:%M") + "]30m@" + str(p_wash_rinse) + "(-2h)15m@" + str(p_wash_rinse) + "(-1h)15m@" + str(p_spin) + "(-3h)15m@" + str(p_dry) + "(-1h)15m@" + str(p_dry))
        
        start = (start + datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
        
    return o

#################
# Garage heater #
#################
# Powers: https://www.sylvane.com/dimplex-dch4831l-electric-heater.html
#         https://www.newair.com/products/5000-watt-g73
#         https://www.helpadvisors.org/best-garage-heater-review/
# 2500W or 5000W = power of space heaters.
# Estimated losses in the walls: 300W for 20°C difference between outside temperature and garage temp (less if difference is smaller)
# For a 45m3 garage, 1°C <=> 12.5Wh
# Characteristic time of energy loss (exponential) of the room: 1,25h
# Total consumed energy per day:  let's say we can make everything flexible 7500Wh and that it won't be too cold if it waits too long
# This assumption is quite wrong bug it's the best we can make...
# TODO Add the calorific capacity of objects inside the garage

n_garageheater = 0
p0_avg_garageheater = pleiad.quantity.Power(kW=2.5)
p1_avg_garageheater = pleiad.quantity.Power(kW=5)
e_sum_avg_garageheater = pleiad.quantity.Energy(kWh=7.5)

def gen_garageheater(ts, p0=None, p1=None, e_sum=None, name=None, ts_compatible=True):
    global n_garageheater
    n_garageheater = n_garageheater + 1
    
    if name is None:
        name = "GarageHeater#" + str(n_garageheater)
    
    
    d1 = ts.start
    d2 = ts.end
    
    
    if p0 is None: p0 = p0_avg_garageheater
    if p1 is None: p1 = p1_avg_garageheater
    if e_sum is None: e_sum = e_sum_avg_garageheater
    
    if ts_compatible:
        quantum_consumption = p0 * ts.delta
    
    o = pleiad.physicalobject.FlexibleAppliance(name)
    
    start = d1
    end = min(d2, (start + datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0))
    while start < d2:
        if ts_compatible:
            conso_e_sum = round((e_sum * ((end - start) / datetime.timedelta(days=1))) / quantum_consumption) * quantum_consumption
        else:
            conso_e_sum = (e_sum * ((end - start) / datetime.timedelta(days=1))).__round__(0, "Wh")
        o.add_task("[" + start.strftime("%Y-%m-%dT%H:%M") + "/" + end.strftime("%Y-%m-%dT%H:%M") + "]0W|" + str(p0) + "|" + str(p1) + "=" + str(conso_e_sum))
        start = end
        end = min(d2, end + datetime.timedelta(days=1))

    return o




################
# Space heater #
################
# 1250W = very frequent space heater power
# Load profile: not sourced
n_spaceheater = 0

def gen_space_heater(ts, name=None):
    global n_spaceheater
    n_spaceheater = n_spaceheater + 1
    
    if name is None:
        name = "SpaceHeater#" + str(n_spaceheater)
    
    d1 = ts.start
    d2 = ts.end
    
    o = pleiad.physicalobject.ThermalAppliance(name)
    
    power_comfort = "1500W"
    power_high = "3000W"
    
    start = d1
    end = min(d2, (start + datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0))
    while start < d2:
        patterns = {}
        defin = {"patterns": patterns}
        
        patterns["comfort"] = {start.strftime("%Y-%m-%dT%H:%M") + "/" + end.strftime("%Y-%m-%dT%H:%M"): power_comfort}
        patterns["nightandday"] = {}
        
        if start.hour < 6:
            patterns["nightandday"][start.strftime("%Y-%m-%dT%H:%M") + "/" + start.replace(hour=6, minute=0, second=0, microsecond=0).strftime("%Y-%m-%dT%H:%M")] = power_high
            
        if start.hour < 12:
            patterns["nightandday"][start.replace(hour=12, minute=0, second=0, microsecond=0).strftime("%Y-%m-%dT%H:%M") + "/" + start.replace(hour=18, minute=0, second=0, microsecond=0).strftime("%Y-%m-%dT%H:%M")] = power_high
        elif start.hour < 18:
            patterns["nightandday"][start.strftime("%Y-%m-%dT%H:%M") + "/" + start.replace(hour=18, minute=0, second=0, microsecond=0).strftime("%Y-%m-%dT%H:%M")] = power_high
        
        o.add_task(defin)
        o.set_preferred("comfort,1USDph")
        
        start = end
        end = min(d2, end + datetime.timedelta(days=1))

    return o


n_flexiblespaceheater = 0

def gen_flexible_space_heater(ts, name=None):
    global n_flexiblespaceheater
    n_flexiblespaceheater = n_flexiblespaceheater + 1
    
    if name is None:
        name = "FlexibleSpaceHeater#" + str(n_flexiblespaceheater)
    
    d1 = ts.start
    d2 = ts.end
    
    p0 = pleiad.quantity.Power(W=1500)
    p1 = pleiad.quantity.Power(W=3000)
    e_sum = pleiad.quantity.Energy(Wh=4500)
    
    quantum_consumption = p0 * ts.delta
    
    o = pleiad.physicalobject.FlexibleAppliance(name)
    
    start = d1
    end = min(d2, (start + datetime.timedelta(hours=3)).replace(minute=0, second=0, microsecond=0))
    while start < d2:
        conso_e_sum = round((e_sum * ((end - start) / datetime.timedelta(hours=3))) / quantum_consumption) * quantum_consumption
        o.add_task("[" + start.strftime("%Y-%m-%dT%H:%M") + "/" + end.strftime("%Y-%m-%dT%H:%M") + "]0W|" + str(p0) + "|" + str(p1) + "=" + str(conso_e_sum))
        start = end
        end = min(d2, end + datetime.timedelta(hours=3))

    return o


################
#  Television  #
################
n_tv = 0
# Issi et Kaplan 2018
# 60W whenoperating when 20W after shutdown, for 10-15 min

def gen_tv(ts, name=None):
    global n_tv
    n_tv = n_tv + 1
    
    if name is None:
        name = "TV#" + str(n_tv)
    
    d1 = ts.start
    d2 = ts.end
    
    o = pleiad.physicalobject.ActivityBasedAppliance(name)
    
    start = d1
    
    while start < d2:
        
        if start.weekday() == 5 or start.weekday() == 6:
            tv_acceptable_start = start.replace(hour=16, minute=0, second=0, microsecond=0) # 4pm
            tv_acceptable_end = tv_acceptable_start + datetime.timedelta(hours=6, minutes=30) # 10:30pm the same day
            
            if tv_acceptable_end > d1 and tv_acceptable_start < d2:
                o.add_task("[" + tv_acceptable_start.strftime("%Y-%m-%dT%H:%M") + "/" + tv_acceptable_end.strftime("%Y-%m-%dT%H:%M") + "]120m@60W>15m@20W")
        
        # News bulletin
        tv_acceptable_start = start.replace(hour=19, minute=0, second=0, microsecond=0) # 7pm
        tv_acceptable_end = tv_acceptable_start + datetime.timedelta(hours=1, minutes=30) # 8:30pm the same day
        
        if tv_acceptable_end > d1 and tv_acceptable_start < d2:
            o.add_task("[" + tv_acceptable_start.strftime("%Y-%m-%dT%H:%M") + "/" + tv_acceptable_end.strftime("%Y-%m-%dT%H:%M") + "]75m@60W>15m@20W")
        
        start = (start + datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
        
    return o

# Still to be added
# --
# Kitchen hood
# Lightning
# Computer
# AC
# Bathroom air vent
# Other appliances : blender, microwave, video game console, ...