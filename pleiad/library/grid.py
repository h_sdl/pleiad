#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 15:48:00 2019

@author: Hugues Souchard de Lavoreille
"""

import pleiad.quantity

########
# Grid #
########
n_grid = 0

def gen_edf_grid(name=None):
    global n_grid
    n_grid = n_grid + 1
    
    if name is None:
        name = "Grid#" + str(n_grid)
        
    EDFGrid = pleiad.physicalobject.PowerSource(name, price_structure_purchases=pleiad.library.pricestructure.EDFConstant, price_structure_sales=pleiad.library.pricestructure.EDFAutoSales)
    return EDFGrid

def gen_quebec_grid(name=None):
    global n_grid
    n_grid = n_grid + 1
    
    if name is None:
        name = "Grid#" + str(n_grid)
        
    QCGrid = pleiad.physicalobject.PowerSource(name, price_structure_purchases=pleiad.library.pricestructure.Quebec, price_structure_sales=pleiad.utils.EmptyPriceStructure())
    return QCGrid

def gen_ontario_grid(name=None):
    global n_grid
    n_grid = n_grid + 1
    
    if name is None:
        name = "Grid#" + str(n_grid)
        
    OGrid = pleiad.physicalobject.PowerSource(name, price_structure_purchases=pleiad.library.pricestructure.OntarioTOUWinter2020, price_structure_sales=pleiad.utils.EmptyPriceStructure())
    return OGrid

def gen_ontario_grid_modified(name=None):
    global n_grid
    n_grid = n_grid + 1
    
    if name is None:
        name = "Grid#" + str(n_grid)
        
    OGrid = pleiad.physicalobject.PowerSource(name, price_structure_purchases=pleiad.library.pricestructure.OntarioTLOUWinter2020, price_structure_sales=pleiad.utils.EmptyPriceStructure())
    return OGrid

def gen_purchaseonly_grid(name=None):
    global n_grid
    n_grid = n_grid + 1
    
    if name is None:
        name = "Grid#" + str(n_grid)
        
    return pleiad.physicalobject.PowerSource(name, price_structure_purchases=pleiad.library.pricestructure.OntarioConstantWinter2020, price_structure_sales=pleiad.utils.EmptyPriceStructure())