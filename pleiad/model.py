#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  7 11:50:32 2019

@author: Hugues Souchard de Lavoreille
"""

import datetime

import pleiad.timedivision
import pleiad.physicalobject
import pleiad.structure

import pandas as pd
import numpy as np


class Model:
    def __init__(self, ts, localnetwork):   
        self.solved = False
        
        if not(isinstance(localnetwork, pleiad.structure.LocalNetwork)):
            raise Exception("localnetwork should be LocalNetwork")
        self.localnetwork = localnetwork
            
        if not(isinstance(ts, pleiad.timedivision.TimeDivision)):
            raise Exception("ts should be TimeDivision")
        self.ts = ts
        
        # Creating the list of tasks required for the model
        self.tasks = []
        for s in self.localnetwork.get_all_connectables():
            # This function checks whether everything is in the simulation horizon
            connectabletasks = s.get_tasks_and_bounds_inside_ts(self.ts)
            for connectabletask in connectabletasks:
                self.tasks.append(type(self).association_tasks[type(connectabletask)](connectabletask, self.ts))
        
        # Initializing variables and constraints of the model
        self.variables = []
        self.constraints = []
        
        # Initializing the dataframe containing the results : computing its columns and its lines, then putting the right data type inside        
        results_columns, results_columns_types = self.build_results_columns_list()        
        ridx = pd.date_range(self.ts.start - self.ts.delta, self.ts.end, freq=self.ts.delta) # One more row at the start and at the end for states (n intervals -> n+1 points)
        self.results = pd.DataFrame(0, index=ridx, columns=pd.MultiIndex.from_tuples(results_columns))
        for i in range(len(results_columns_types)):
            if self.results.columns[i][1][0] >= "A" and self.results.columns[i][1][0] <= "Z":
                if results_columns_types[i] is int:
                    tmp = np.nan
                else:
                    tmp = results_columns_types[i](np.nan)
                self.results.iloc[:,i] = tmp
            else:
                self.results.iloc[:,i] = results_columns_types[i](0)
                
    def build_results_columns_list(self):
        l=[]
        t=[]
        
        def f(x, l, t):
            if type(x) not in type(self).results_columns.keys():
                use = "default"
            else:
                use = type(x)
            for (li,ti) in type(self).results_columns[use]:
                l += [(x.name, li)]
                t += [ti]
        
        for n in self.localnetwork.get_nodes():
            f(n, l, t)
            for c in n.get_connectables():
                f(c, l, t)
                    
        return l,t
    
    def take_snapshot(self, t):
        """
        Fetches the snapshot from the right tasks
        Returns a fully formatted snapshot, which MAY be used to make a bigger snapshot
        """
        
        if not(isinstance(t, datetime.datetime)):
            raise pleiad.error.WrongArgumentError()
        if not(self.solved):
            raise pleiad.error.ModelNotSolvedError()
        if t > self.ts.end or t < self.ts.start:
            raise Exception("Impossible to take a snapshot out of the horizon (" + self.ts.__str_time_range__() + ")")
        
        for c in self.localnetwork.get_all_connectables():
            c.take_snapshot(t)