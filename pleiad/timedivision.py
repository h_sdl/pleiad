#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 15:57:00 2019

@author: Hugues Souchard de Lavoreille
"""

from datetime import datetime, timedelta
import re

class TimeDivision:
    def __init__(self, x):
        r1 = re.compile("^([\d\-T:]{16})/([\d\-T:]{16})/([\d]+)(m|h)$")
        res1 = r1.findall(x)
        
        conv = {'h':'hours', 'm':'minutes'}
        
        try:
            self.start = datetime.strptime(res1[0][0], "%Y-%m-%dT%H:%M")
            self.end = datetime.strptime(res1[0][1], "%Y-%m-%dT%H:%M")
            self.delta = timedelta(**{conv[res1[0][3]]:float(res1[0][2])})
            
        except:
            raise Exception("Did not understand TimeDivision " + x)
        
        if ((self.end - self.start) % self.delta).total_seconds() != 0: raise Exception("TimeDivision start and end times are not compatible with the timedelta")
        self.n = (self.end - self.start) // self.delta
    
    def time_to_idx(self, t):
        if ((t - self.start) % self.delta).total_seconds() != 0: raise Exception("This datetime is not compatible with the timedivision")
        return (t - self.start) // self.delta
    
    def idx_to_time(self, idx):
        return self.start + idx * self.delta
    
    def delta_to_idxr(self, td):
        return td // self.delta
    
    def idxr_to_delta(self, idxr):
        return idxr * self.delta
    
    def total_duration(self):
        return self.end - self.start
    
    def __str_time_range__(self):
        return self.start.strftime("%Y-%m-%dT%H:%M") + "/" + self.end.strftime("%Y-%m-%dT%H:%M")