#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 11:28:03 2019

@author: Hugues Souchard de Lavoreille
"""

import datetime
import operator

import pleiad.quantity
import pleiad.formal
import pleiad.utils

import math

class Container:
    """
    Container class enabling to store objects as attributes, using the dot "."
    The tolist method gathers all the stored objects in a list and returns them
    """
    
    def tolist(self):
        """
        Returns a list of all the stored elements
        """
        res = []
        for el in self.__dict__.values():
            res += [x for x in Container.recursiveflatten(el)]
        return res
    
    @staticmethod
    def recursiveflatten(x):
        """
        Yields each non-list/array element during a depth-first search in the lists/arrays
        """
        if isinstance(x, pleiad.formal.Array):
            for y in x.flatten():
                yield from Container.recursiveflatten(y)
        elif isinstance(x, list):
            for y in x:
                yield from Container.recursiveflatten(y)
        else:
            yield x

class SchedulerTask:
    """
    Base class used by the 'Scheduler' Model in order to translate ConnectableTasks into model objects
    """
    
    association_connectabletask = {}
    
    def __init__(self, connectabletask, ts):
        """
        Initializer for class objects common to every SchedulerTask object
        """
        self.variables = Container()
        self.variables.energies = pleiad.formal.Array()
        self.variables.costs = pleiad.formal.Array()
        self.variables.virtual_costs = pleiad.formal.Array()
        self.variables.operating = pleiad.formal.Array()
        self.constraints = Container()
        self.st = connectabletask
        self.ts = ts
        SchedulerTask.association_connectabletask[self.st.id] = self
    
    def get_idx_start(self):
        return self.ts.time_to_idx(self.st.start)
    
    def get_idx_end(self):
        return self.ts.time_to_idx(self.st.end)
    
    idx_start = property(get_idx_start)
    idx_end = property(get_idx_end)
    
class ThermalSchedulerTask(SchedulerTask):
    """
    Class used for Thermal Appliances (type A)
    """
    
    def generate_shifted_loads_wrt_preferred(self):                
        shifted_load = []
        if self.preferred is None:
            for pat in self.schemes:
                shifted_load.append([pleiad.quantity.Power(0)] * len(pat))
        else:
            pattern_preferred = self.schemes[self.schemes_names.index(self.preferred)]
            for pat in self.schemes:
                shifted = []
                for i in range(len(pat)):
                    shifted.append(max(pleiad.quantity.Power(0), pat[i] - pattern_preferred[i]))
                shifted_load.append(shifted)
        return shifted_load
                
    def generate_binaries_out_of_preferred_window(self):
        binary_out_of_preferred_window = []
        if self.preferred is None:
            for pat in self.schemes:
                binary_out_of_preferred_window.append([0] * len(pat))
        else:
            pattern_preferred = self.schemes[self.schemes_names.index(self.preferred)]
            for pat in self.schemes:
                binary_out = []
                for i in range(len(pat)):
                    binary_out.append(0 if not pattern_preferred[i].is_null() or pat[i].is_null() else 1)
                binary_out_of_preferred_window.append(binary_out)
        return binary_out_of_preferred_window
    
    def build(self):
        # Defining constants    
        self.schemes_names, self.schemes, self.schemes_binary, self.preferred = self.st.generate_schemes(self.ts)
        arr_schemes = pleiad.formal.Array(self.schemes)
        arr_operating = pleiad.formal.Array(self.schemes_binary)
        arr_on_out_of_window = pleiad.formal.Array(self.generate_binaries_out_of_preferred_window())
        
        # Defining variables
        self.variables.used_schemes = pleiad.formal.Array([
            pleiad.formal.Variable("UsedScheme(t_" + str(self.idx_start) + "_" + str(self.idx_end) + ",Scheme_" + str(i) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="I", quantity_type=int, mini=0, maxi=1)
            for i in range(len(self.schemes))
        ])
        self.variables.energies = pleiad.formal.Array([
            pleiad.formal.Variable("E(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.operating = pleiad.formal.Array([
            pleiad.formal.Variable("o(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="I", quantity_type=int, mini=0, maxi=1)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.costs = pleiad.formal.Array([
            pleiad.formal.Variable("C(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.virtual_costs = pleiad.formal.Array([
            pleiad.formal.Variable("VC(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price)
            for t in range(self.idx_start, self.idx_end)
        ])
    
        # Defining constraints
        self.constraints.one_scheme = (self.variables.used_schemes.sum() == 1.0)                                                                                                                                          % ("OnePatternSelected(t_" + str(self.idx_start) + "_" + str(self.idx_end) + "," + self.st.connectable.name + "_" + str(self.st.id) + ")")
        self.constraints.virtual_costs = (-self.variables.virtual_costs + (self.st.preferred_price * (self.ts.delta * arr_on_out_of_window)).T.dot(self.variables.used_schemes) == pleiad.quantity.Price(0))     % ["VirtualCostDependsOnShiftedLoad(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.cost_zero = (self.variables.costs == pleiad.quantity.Price(0))                                                                                                                                   % ["CostIsZero(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.energy_scheme = ((-1) * self.variables.energies + (self.ts.delta * arr_schemes).T.dot(self.variables.used_schemes) == pleiad.quantity.Energy(0))                                                 % ["EnergyDependsOnScheme(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.is_operating = (-self.variables.operating + arr_operating.T.dot(self.variables.used_schemes) == 0)                                                                                            % ["OperationDependsOnScheme(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
            
    def used_pattern_name(self):
        for i in range(len(self.schemes_names)):
            if self.variables.used_schemes.get(i).value is not None and self.variables.used_schemes.get(i).value == 1:
                return self.schemes_names[i]
        raise Exception("No used pattern found")

class ActivityBasedSchedulerTask(SchedulerTask):
    
    def generate_shifted_loads(self, schemes, preferred_scheme):
        shifted_load = []
        if preferred_scheme is not None:
            for pat in schemes:
                shifted = []
                for i in range(len(pat)):
                    shifted.append(max(pleiad.quantity.Power(0), pat[i] - preferred_scheme[i]))
                shifted_load.append(shifted)
        else:
            for pat in schemes:
                shifted_load.append([pleiad.quantity.Power(0)] * len(pat))
        return shifted_load
    
    def generate_binaries_out_of_preferred_window(self, schemes, preferred_scheme):
        out_of_window = []
        if preferred_scheme is None:
            for pat in schemes:
                out_of_window.append([0] * len(pat))
        else:
            for pat in schemes:
                out = []
                for i in range(len(pat)):
                    out.append(0 if not preferred_scheme[i].is_null() or pat[i].is_null() else 1)
                out_of_window.append(out)
            
        return out_of_window
    
    def build(self):
        # Defining constants
        self.schemes, self.schemes_binary = self.st.generate_schemes(self.ts)
        self.preferred_scheme = self.st.generate_preferred_scheme(self.ts)
        arr_schemes = pleiad.formal.Array(self.schemes)
        arr_schemes_binary = pleiad.formal.Array(self.schemes_binary)
        arr_shifted_loads = pleiad.formal.Array(self.generate_binaries_out_of_preferred_window(self.schemes, self.preferred_scheme))
        
        # Defining variables
        self.variables.used_schemes = pleiad.formal.Array([
            pleiad.formal.Variable("UsedScheme(t_" + str(self.idx_start) + "_" + str(self.idx_end) + ",Scheme_" + str(i) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="I", quantity_type=int, mini=0, maxi=1)
            for i in range(len(self.schemes))
        ])
        self.variables.energies = pleiad.formal.Array([
            pleiad.formal.Variable("E(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy) 
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.costs = pleiad.formal.Array([
            pleiad.formal.Variable("C(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.virtual_costs = pleiad.formal.Array([
            pleiad.formal.Variable("VC(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.operating = pleiad.formal.Array([
            pleiad.formal.Variable("o(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="I", quantity_type=int, mini=0, maxi=1)
            for t in range(self.idx_start, self.idx_end)
        ])
        
        # Defining constraints
        self.constraints.one_scheme = (self.variables.used_schemes.sum() == 1)                                                                                                                                              % ("OneSchemeSelected(t_" + str(self.idx_start) + "_" + str(self.idx_end) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")")
        self.constraints.virtual_cost_shifted_load = (-self.variables.virtual_costs + (self.st.preferred_price * (self.ts.delta * arr_shifted_loads)).T.dot(self.variables.used_schemes) == pleiad.quantity.Price(0))       % ["VirtualCostDependsOnShiftedLoad(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.cost_zero = (self.variables.costs == pleiad.quantity.Price(0))                                                                                                                                     % ["CostIsZero(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.energy_scheme = ((-1) * self.variables.energies + (self.ts.delta * arr_schemes).T.dot(self.variables.used_schemes) == pleiad.quantity.Energy(0))                                                   % ["EnergyDependsOnScheme(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.is_operating = (-self.variables.operating + arr_schemes_binary.T.dot(self.variables.used_schemes) == 0)                                                                                         % ["OperationDependsOnScheme(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
            
    def elapsed_operating_time_at(self, t):
        idx_t = self.ts.time_to_idx(t)
        time_elapsed = datetime.timedelta(0)
        
        for i in range(self.idx_start, self.idx_end):
            e = self.variables.energies.get(i - self.idx_start)
            if i < idx_t and not(e.value.is_null()):
                time_elapsed += self.ts.delta
        
        return time_elapsed

class FlexibleSchedulerTask(SchedulerTask):
    """
    Class used for Flexible Appliances (type C)
    """
    
    def build(self):
        # Defining variables
        self.variables.energies = pleiad.formal.Array([
            pleiad.formal.Variable("E(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy) 
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.costs = pleiad.formal.Array([
            pleiad.formal.Variable("C(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price, mini=pleiad.quantity.Price(0))
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.virtual_costs = pleiad.formal.Array([
            pleiad.formal.Variable("VC(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price, mini=pleiad.quantity.Price(0))
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.whichpower = pleiad.formal.Array([
            [
                pleiad.formal.Variable("delta_p_t(t_" + str(t) + ",p=" + str(p) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="I", quantity_type=int, mini=0, maxi=1)
                for t in range(self.idx_start, self.idx_end)
            ]
            for p in self.st.possible_powers
        ])
        self.variables.operating = pleiad.formal.Array([
            pleiad.formal.Variable("o(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="I", quantity_type=int, mini=0, maxi=1)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.is_on = pleiad.formal.Array([
            pleiad.formal.Variable("nonzero(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="I", quantity_type=int, mini=0, maxi=1)
            for t in range(self.idx_start, self.idx_end)
        ])
        
        # Defining constants
        pows = pleiad.formal.Array(self.st.possible_powers)  
        nonzero = pleiad.formal.Array([0 if p.is_null() else 1 for p in self.st.possible_powers])
        arr_preferred_operation_binaries = pleiad.formal.Array(self.st.generate_preferred_operation_binaries(self.ts))
        
        
        # Defining constraints
        self.constraints.one_power_per_t = (self.variables.whichpower.sum(axis=0) == 1)                                                                                                                 % ["OnePowerLevel(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.cost_zero = (self.variables.costs == pleiad.quantity.Price(0))                                                                                                                    % ["CostIsZero(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.cost_shifted_load = ((self.variables.virtual_costs - (self.st.preferred_price * self.ts.delta) * self.variables.operating) >= - self.st.preferred_price * (self.ts.delta * arr_preferred_operation_binaries))  % ["VirtualCostDependsOnShiftedLoad(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.energies_power = ((-1) * self.variables.energies + ((self.ts.delta * pows).T.dot(self.variables.whichpower)).T == pleiad.quantity.Energy(0))                                                 % ["EnergyDependsOnPowerScheme(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.sum_energies = (self.variables.energies.sum() == self.st.e_sum)                                                                                                   % ("SumEnergiesShouldReachTarget(t_" + str(self.idx_start) + "_" + str(self.idx_end) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")")
        self.constraints.is_on = (-self.variables.is_on.T + nonzero.T.dot(self.variables.whichpower) == 0)                                                                                                         % ["OnWhenPowerNotZero(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.is_operating_left = (-self.variables.is_on[0]+self.variables.operating[0] == 0) % ("OperatingWhenOn(t_" + str(self.idx_start) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")")
        self.constraints.is_operating_right = (-self.variables.is_on[-1]+self.variables.operating[-1] == 0) % ("OperatingWhenOn(t_" + str(self.idx_end - 1) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")")
        
        # Implementing w=1 <=> x=1 or (y=1 and z=1)
        x = self.variables.is_on[1:-1]
        y = self.variables.operating[:-2]
        z = self.variables.operating[2:]
        w = self.variables.operating[1:-1]
        self.constraints.c1 = (x-y+z-w<=1) % ["OperatingWhenOnORHasBeenOnAndWillBeOn1(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start + 1, self.idx_end - 1)]
        self.constraints.c2 = (x+y-z-w<=1) % ["OperatingWhenOnORHasBeenOnAndWillBeOn2(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start + 1, self.idx_end - 1)]
        self.constraints.c3 = (-x+y+z-w<=1) % ["OperatingWhenOnORHasBeenOnAndWillBeOn3(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start + 1, self.idx_end - 1)]
        self.constraints.c4 = (x-y-z-w<=0) % ["OperatingWhenOnORHasBeenOnAndWillBeOn4(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start + 1, self.idx_end - 1)]
        self.constraints.c5 = (-x+y-z+w<=1) % ["OperatingWhenOnORHasBeenOnAndWillBeOn5(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start + 1, self.idx_end - 1)]
        self.constraints.c6 = (-x-y+z+w<=1) % ["OperatingWhenOnORHasBeenOnAndWillBeOn6(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start + 1, self.idx_end - 1)]
        self.constraints.c7 = (x+y+z-w<=2) % ["OperatingWhenOnORHasBeenOnAndWillBeOn7(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start + 1, self.idx_end - 1)]
        self.constraints.c8 = (-x-y-z+w<=0) % ["OperatingWhenOnORHasBeenOnAndWillBeOn8(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start + 1, self.idx_end - 1)]
    
    def energy_consumed_at(self, t):
        idx_t = self.ts.time_to_idx(t)
        e_consumed = pleiad.quantity.Energy(0)
        
        for i in range(self.idx_start, self.idx_end):
            e = self.variables.energies.get(i - self.idx_start)
            if i < idx_t:
                e_consumed += e.value
                
        return e_consumed
    
class PowerSourceSchedulerTask(SchedulerTask):
    
    def make_variables_price_structure(self, struct, sell = False):
        # Buy = 0, sell = 1
        if sell:
            name = "sales"
        else:
            name = "purchase"
            
        if isinstance(struct, pleiad.utils.TLOUPriceStructure):
            return pleiad.formal.Array([
                [
                    pleiad.formal.Variable("E_" + name + "_L1(t_" + str(t) + "," + str(self.st.connectable.name) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy, **{("mini" if sell else "maxi"):pleiad.quantity.Energy(0), ("maxi" if sell else "mini"):(1 if sell else -1) * self.st.connectable.price_structure_purchases.p_lim * self.ts.delta}),
                    pleiad.formal.Variable("E_" + name + "_L2(t_" + str(t) + "," + str(self.st.connectable.name) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy, **{("mini" if sell else "maxi"):pleiad.quantity.Energy(0)})
                ]
                for t in range(self.idx_start, self.idx_end)
            ])
        elif isinstance(struct, (pleiad.utils.ConstantPriceStructure, pleiad.utils.TOUPriceStructure)):
            return pleiad.formal.Array([
                [
                    pleiad.formal.Variable("E_" + name + "(t_" + str(t) + "," + str(self.st.connectable.name) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy, **{("mini" if sell else "maxi"):pleiad.quantity.Energy(0)})
                ]
                for t in range(self.idx_start, self.idx_end)
            ])
        elif isinstance(struct, pleiad.utils.EmptyPriceStructure):
            return pleiad.formal.Array()
            
        raise Exception("Unknown price structure")
        
    def build(self):
        # Defining variables
        self.variables.costs = pleiad.formal.Array([
            pleiad.formal.Variable("C(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.energies = pleiad.formal.Array([
            pleiad.formal.Variable("E(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy)
            for t in range(self.idx_start, self.idx_end)
        ])
        
        # Case 1 : purchase and sales structures are a level-based (TLOU, TOU, Constant, Empty)
        if (isinstance(self.st.connectable.price_structure_sales, (pleiad.utils.TLOUPriceStructure, pleiad.utils.ConstantPriceStructure, pleiad.utils.TOUPriceStructure, pleiad.utils.EmptyPriceStructure)) and
            isinstance(self.st.connectable.price_structure_purchases, (pleiad.utils.TLOUPriceStructure, pleiad.utils.ConstantPriceStructure, pleiad.utils.TOUPriceStructure, pleiad.utils.EmptyPriceStructure))):
            self.variables.energy_levels = self.make_variables_price_structure(self.st.connectable.price_structure_sales, sell=True).concat(self.make_variables_price_structure(self.st.connectable.price_structure_purchases), axis = 1)
            
            # Defining constants
            arr_costs = pleiad.formal.Array([
                list(self.st.connectable.price_structure_sales.pricing_function(self.ts.idx_to_time(t))) + list(self.st.connectable.price_structure_purchases.pricing_function(self.ts.idx_to_time(t)))
                for t in range(self.idx_start, self.idx_end)
            ])
            if self.st.connectable.amount is not None: 
                arr_fc = self.ts.delta * pleiad.formal.Array([
                    self.st.connectable.amount(self.ts.idx_to_time(t))
                    for t in range(self.idx_start, self.idx_end)
                ])
                self.constraints.amount = (self.variables.energies == (-1) * arr_fc)                                                                           % ["EnergyAmountIsAsRequested(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
            
            # Defining constraints
            self.constraints.sum_energy_levels = (self.variables.energy_levels.sum(axis=1) - self.variables.energies == pleiad.quantity.Energy(0))                                                                   % ["EnergyIsSumOfEachEnergyLevel(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)] 
            self.constraints.cost_price_energy = ((-1) * self.variables.costs + ((-1 * arr_costs) * (self.variables.energy_levels)).sum(axis=1) == pleiad.quantity.Price(0))                                         % ["CostIsSumOfCostsOfEachEnergyLevel(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        # Case 2 : power can be bought with a Block price structure, no sales
        elif isinstance(self.st.connectable.price_structure_purchases, pleiad.utils.BlockPriceStructure) and isinstance(self.st.connectable.price_structure_sales, pleiad.utils.EmptyPriceStructure):
            name = "purchase"
            sell = False
            self.variables.energy_levels = pleiad.formal.Array([
                [
                    pleiad.formal.Variable("E_" + name + "_L1(t_" + str(t) + "," + str(self.st.connectable.name) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy, **{("mini" if sell else "maxi"):pleiad.quantity.Energy(0)}),
                    pleiad.formal.Variable("E_" + name + "_L2(t_" + str(t) + "," + str(self.st.connectable.name) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy, **{("mini" if sell else "maxi"):pleiad.quantity.Energy(0)})
                ]
                for t in range(self.idx_start, self.idx_end)
            ])
            
            arr_costs = pleiad.formal.Array([
                [self.st.connectable.price_structure_purchases.price1,
                self.st.connectable.price_structure_purchases.price2]
                for t in range(self.idx_start, self.idx_end)
            ])
            
            # Defining constraints : the sum of all energy levels is the total energy drawn
            self.constraints.sum_energy_levels = (self.variables.energy_levels.sum(axis=1) - self.variables.energies == pleiad.quantity.Energy(0))                                                                   % ["EnergyIsSumOfEachEnergyLevel(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)] 
            # The cost is the cost of each energy level multiplied by its price, at each time step
            self.constraints.cost_price_energy = ((-1) * self.variables.costs + ((-1 * arr_costs) * (self.variables.energy_levels)).sum(axis=1) == pleiad.quantity.Price(0))                                         % ["CostIsSumOfCostsOfEachEnergyLevel(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
            
            # Block tariff-specific constraint : the rolling sum over 24 hours is maximally equal to some specific value
            # First computing the number of indices over which each sum must be done
            delta_step = self.ts.delta_to_idxr(self.st.connectable.price_structure_purchases.duration)
            number_of_periods = math.ceil((self.idx_end - self.idx_start) / delta_step)
            
            for i in range(number_of_periods):
                istart = self.idx_start + i * delta_step
                iend = min(self.idx_start + (i + 1) * delta_step, self.idx_end)
                tmp_constraint = (self.variables.energy_levels[istart:iend,0].sum() <= self.st.connectable.price_structure_purchases.amount)  % ["MaximumBlock(t_" + str(istart) + "-" + str(iend) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")"]
                setattr(self.constraints, "block_" + str(i), tmp_constraint)
            
        # Case 3 : to be implemented
        else:
            raise NotImplementedError("This combination of price structures is not implemented.")
        
class StorageSchedulerTask(SchedulerTask):
    
    def build(self):
        # Defining constants
        dict_operator_type = {"E": operator.eq, "G": operator.ge, "L": operator.le}
        battery_soc_constraints = self.st.build_soc_constraints(self.ts)
        self.storing_efficiency_rate_per_ts_delta = (1 - self.st.connectable.self_discharge_rate_per_day) ** (self.ts.delta / datetime.timedelta(days=1))
        
        # Defining variables
        self.variables.energy_states = pleiad.formal.Array([
            pleiad.formal.Variable("Q(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy, mini=pleiad.quantity.Energy(0), maxi=self.st.connectable.e_max)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.energies = pleiad.formal.Array([
            pleiad.formal.Variable("E(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy, mini=-self.st.connectable.p_max * self.ts.delta, maxi=self.st.connectable.p_max * self.ts.delta)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.energies_grid_in = pleiad.formal.Array([
            pleiad.formal.Variable("E_in(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy, mini=pleiad.quantity.Energy(0), maxi=self.st.connectable.p_max * self.ts.delta)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.energies_grid_out = pleiad.formal.Array([
            pleiad.formal.Variable("E_out(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy, mini=-self.st.connectable.p_max * self.ts.delta, maxi=pleiad.quantity.Energy(0))
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.energies_batt = pleiad.formal.Array([
            pleiad.formal.Variable("q(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy, mini=-self.st.connectable.p_max * self.ts.delta / self.st.connectable.discharging_efficiency, maxi=self.st.connectable.p_max * self.ts.delta * self.st.connectable.charging_efficiency)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.charge_states = pleiad.formal.Array([
            pleiad.formal.Variable("z(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="I", quantity_type=int, mini=0, maxi=1)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.cycle_start = pleiad.formal.Array([
            pleiad.formal.Variable("w(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="I", quantity_type=int, mini=0, maxi=1)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.costs = pleiad.formal.Array([
            pleiad.formal.Variable("C(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price)
            for t in range(self.idx_start, self.idx_end)
        ])
        
        # Defining constraints
        self.constraints.cost_zero = (self.variables.costs == pleiad.quantity.Price(0))                                                                                                                                               % ["CostIsZero(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.energies_in_out = (self.variables.energies - self.variables.energies_grid_in - self.variables.energies_grid_out == pleiad.quantity.Energy(0))                                                                                      % ["EnergyInOut(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.upper_limit_energies = (self.variables.energies_grid_in - (self.st.connectable.p_max * self.ts.delta) * self.variables.charge_states <= pleiad.quantity.Energy(0))                                                       % ["UpperBoundEnergy(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.lower_limit_energies = ((self.st.connectable.p_max * self.ts.delta) * self.variables.charge_states - self.variables.energies_grid_out <= self.st.connectable.p_max * self.ts.delta)                                  % ["LowerBoundEnergy(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        self.constraints.energies_grid_battery = (self.variables.energies_batt - self.st.connectable.charging_efficiency * self.variables.energies_grid_in - (1 / self.st.connectable.discharging_efficiency) * self.variables.energies_grid_out == pleiad.quantity.Energy(0))                                  % ["EnergyGridBattery(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]
        
        # Lower bounds are defined for each energy variable but not for the last one because the variable does not exist: only E(N-1) + F(N-1) does exist
        self.constraint_last_energy_larger_zero = (self.variable_stored_energy(self.idx_end) >= pleiad.quantity.Energy(0))                                                                                                                            % ("LastEnergyStateLargerThanZero(" + str(self.st.connectable.name) + "_" + str(self.st.id) + ")")
        
        self.constraints.state_evolution = (self.variables.energy_states[1:] - self.storing_efficiency_rate_per_ts_delta * self.variables.energy_states[:-1] - self.variables.energies_batt[:-1] == pleiad.quantity.Energy(0))                         % ["EnergyConservationInBattery(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end - 1)]
        self.constraints.cycle_start = (self.variables.charge_states[1:] - self.variables.charge_states[:-1] - self.variables.cycle_start[1:] <= 0)                                                                                            % ["CycleCounterIncrementation(t_" + str(t) + ","+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start + 1, self.idx_end)]
#       self.constraints.initial_cycle_start = (self.variables.charge_states[0] - self.variables.cycle_start[0] <= 0)                                                                                                                     % ("CycleCounterIncrementation(t_0,"+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")")
        
        if self.st.same_soc_start_end:
            self.constraints.battery_conservation = (self.variable_stored_energy(self.idx_start) - self.variable_stored_energy(self.idx_end) == pleiad.quantity.Energy(0))                                                        % ("EnergyConservationBetweenStartAndEnd(Battery_"+ str(self.st.connectable.name) + "_" + str(self.st.id) + ")")
        
        self.constraints.nb_cycles = []
        nb_cycl = self.st.cycle_limit
        idxr_cycle_limit_duration = self.ts.delta_to_idxr(self.st.duration_for_cycle_limit)
        for i in range(self.idx_start, self.idx_end - idxr_cycle_limit_duration):
            self.constraints.nb_cycles.extend(((self.variables.cycle_start[i:i+idxr_cycle_limit_duration].sum() <= nb_cycl) % ("MaxNbCycles(t_" + str(i) + "/" + str(i + idxr_cycle_limit_duration) + ",Battery_" + str(self.st.connectable.name) + "_" + str(self.st.id) + ")")).flatten())
        if len(self.constraints.nb_cycles) == 0:
            self.constraints.nb_cycles = (self.variables.cycle_start.sum() <= nb_cycl)                                                          % ("MaxNbCycles(Battery_" + str(self.st.connectable.name) + "_" + str(self.st.id) + ")")
            
        
        self.constraints.energy_level = []
        for c in battery_soc_constraints:
            dt = c["datetime"]
            lvl = c["energy"]
            op = dict_operator_type[c["type"]]
            dt_idx = self.ts.time_to_idx(dt)
            self.constraints.energy_level.append((op(self.variable_stored_energy(dt_idx), lvl))                                                                                                                              % ("FixedEnergyState(t_" + str(dt_idx) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")"))
    
    def variable_stored_energy(self, idx_t):
        if idx_t < self.idx_start or idx_t > self.idx_end:
            raise Exception("Index out of bounds")
        if idx_t < self.idx_end:
            return self.variables.energy_states.get(idx_t - self.idx_start)
        if idx_t == self.idx_end:
            return self.storing_efficiency_rate_per_ts_delta * self.variables.energy_states.get(self.idx_end - 1 - self.idx_start) + self.variables.energies_batt.get(self.idx_end - 1 - self.idx_start)
        
    def stored_energy_at(self, t):
        return self.variable_stored_energy(self.ts.time_to_idx(t)).value
    
    def charge_state_before(self, t):
        if t == self.st.start:
            return None
        return self.variables.charge_states.get(self.ts.time_to_idx(t) - 1 - self.idx_start).value
    
class ExchangeNodeSchedulerTask(SchedulerTask):
        
    def build(self):
        # Defining variables
        self.variables.costs = pleiad.formal.Array([
            pleiad.formal.Variable("C(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price)
            for t in range(self.idx_start, self.idx_end)
        ])
        self.variables.energies = pleiad.formal.Array([
            pleiad.formal.Variable("E(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy)
            for t in range(self.idx_start, self.idx_end)
        ])
        
        # Defining constraints
        self.constraints.cost_energy = (-self.variables.costs - self.st.connectable.enode.price * self.variables.energies == pleiad.quantity.Price(0)) % ["CostOfExchangeNodeDependsOnEnergy(t_" + str(t) + "," + str(self.st.connectable.name) + "_" + str(self.st.id) + ")" for t in range(self.idx_start, self.idx_end)]