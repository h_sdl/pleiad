#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 10:00:11 2019

@author: Hugues Souchard de Lavoreille
"""

import pleiad.model
import pleiad.physicalobject
from .baseschedulertask import SchedulerTask, PowerSourceSchedulerTask, StorageSchedulerTask, ActivityBasedSchedulerTask, FlexibleSchedulerTask, ThermalSchedulerTask, ExchangeNodeSchedulerTask


class Model(pleiad.model.Model):
    """
    This class embodies a specific type of Model called BaseSchedulerModel (which is the built-in Model provided with Pleiad)
    It is derived from the research of M. Restrepo et al (2017)
    It can be used as an example to create other models
    """    
    
    # This dictionary associates ConnectableTask objects to the model-specific task objects
    association_tasks = {
            pleiad.physicalobject.PowerSourceTask : PowerSourceSchedulerTask,
            pleiad.physicalobject.StorageTask : StorageSchedulerTask,
            pleiad.physicalobject.ActivityBasedApplianceTask : ActivityBasedSchedulerTask,
            pleiad.physicalobject.FlexibleApplianceTask : FlexibleSchedulerTask,
            pleiad.physicalobject.ThermalApplianceTask : ThermalSchedulerTask
#            pleiad.physicalobject.ExchangeNodeConnectionTask : ExchangeNodeSchedulerTask
    }
    
    
    # This dictionary defines the names and types of the columns which will be created in the results table, for each type of connectable
    # The default case will be used for all the objects not specified in this dictionary
    results_columns = {
            pleiad.physicalobject.Storage: [("energy_drawn", pleiad.quantity.Energy), ("cost", pleiad.quantity.Price), ("virtual_cost", pleiad.quantity.Price), ("Energy_stored_beginning", pleiad.quantity.Energy)],
            "default": [("energy_drawn", pleiad.quantity.Energy), ("cost", pleiad.quantity.Price), ("virtual_cost", pleiad.quantity.Price)]
    }
    
    def create_model(self):        
        """
        This function creates all the variables and the constraints of the system
        It transforms the system structure (contained in self.localnetwork) into lists of variables and constraints, stored in self.constraints and self.variables
        Variables are using the pleiad.formal.Variable type and Constraints are using the pleiad.formal.Constraint type
        """
        
        # Initializing variable and constraint lists
        self.variables = []
        self.constraints = []
        
        # Variable 1 : Total Cost
        self.vCost = pleiad.formal.Variable("TotalCost", integer_or_continuous="C", quantity_type=pleiad.quantity.Price)
        self.variables.append(self.vCost)
        
        # In Pleiad, each Connectable and each Node possesses three member energy and cost variables.
        # They must be initialized and accessible under the 'costs', 'energies' and 'virtual_costs' member names
        # The following lines do initialize member energy and cost variables for each connectable
        # as well as tasks_on (one task operating at a time) variables for each non-nodes
        for s in self.localnetwork.get_all_nodes_and_connectables():
            s.energies = pleiad.formal.Array([
                pleiad.formal.Variable("ENERGY(t_" + str(t) + "," + str(s.name) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy)
                for t in range(self.ts.n)
            ])
            s.energies_cst = [
                (-s.energies.get(t) == pleiad.quantity.Energy(0)) % ("SumOfEnergies(t_" + str(t) + "," + s.name + ")")
                for t in range(self.ts.n)
            ]
            s.costs = pleiad.formal.Array([
                pleiad.formal.Variable("COST(t_" + str(t) + "," + str(s.name) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price)
                for t in range(self.ts.n)
            ])
            s.costs_cst = [
                (-s.costs.get(t) == pleiad.quantity.Price(0)) % ("SumOfCosts(t_" + str(t) + "," + s.name + ")")
                for t in range(self.ts.n)
            ]
            s.virtual_costs = pleiad.formal.Array([
                pleiad.formal.Variable("VIRTUAL_COST(t_" + str(t) + "," + str(s.name) + ")", integer_or_continuous="C", quantity_type=pleiad.quantity.Price)
                for t in range(self.ts.n)
            ])
            s.virtual_costs_cst = [
                (-s.virtual_costs.get(t) == pleiad.quantity.Price(0)) % ("SumOfVirtualCosts(t_" + str(t) + "," + s.name + ")")
                for t in range(self.ts.n)
            ]
            self.variables += s.energies.flatten() + s.costs.flatten() + s.virtual_costs.flatten()
            self.constraints += s.energies_cst + s.costs_cst + s.virtual_costs_cst
        for s in self.localnetwork.get_all_connectables():
            s.operating = pleiad.formal.Array([
                pleiad.formal.Variable("NB_ACTIVE_TASKS(t_" + str(t) + "," + str(s.name) + ")", integer_or_continuous="I", quantity_type=int, mini=0, maxi=1)
                for t in range(self.ts.n)
            ])
            s.operating_cst = [
                (-s.operating.get(t) == 0) % ("NoMoreThanOneTask(t_" + str(t) + "," + s.name + ")")
                for t in range(self.ts.n)
            ]
            self.variables += s.operating.flatten()
            self.constraints += s.operating_cst
        
        # For each task
        for t in self.tasks:
            # Building variables and constraints
            t.build()
            
            # Adding them to the global list
            self.variables += t.variables.tolist()
            self.constraints += t.constraints.tolist()
            
            # Treating energies/costs/operation
            # Size checks
            n = t.idx_end - t.idx_start
            for ts in range(t.idx_start, t.idx_end):
                if n == t.variables.energies.h:
                    t.st.connectable.energies_cst[ts].lhs.add_pair(t.variables.energies.get(ts - t.idx_start))
                elif t.variables.energies.h != 0:
                    raise Exception("Bad size for required variable 'energies' of task " + t.st.connectable.name + "_" + str(t.st.id))
                    
                if n == t.variables.costs.h:
                    t.st.connectable.costs_cst[ts].lhs.add_pair(t.variables.costs.get(ts - t.idx_start))
                elif t.variables.costs.h != 0:
                    raise Exception("Bad size for required variable 'costs' of task " + t.st.connectable.name + "_" + str(t.st.id))
                    
                if n == t.variables.virtual_costs.h:
                    t.st.connectable.virtual_costs_cst[ts].lhs.add_pair(t.variables.virtual_costs.get(ts - t.idx_start))
                elif t.variables.virtual_costs.h != 0:
                    raise Exception("Bad size for required variable 'virtual_costs' of task " + t.st.connectable.name + "_" + str(t.st.id))
                    
                if n == t.variables.operating.h:
                    t.st.connectable.operating_cst[ts].lhs.add_pair(t.variables.operating.get(ts - t.idx_start))
                elif t.variables.operating.h != 0:
                    raise Exception("Bad size for required variable 'operating' of task " + t.st.connectable.name + "_" + str(t.st.id))
        
        # For each connectablenode
        for n in self.localnetwork.get_nodes():
            for i in range(self.ts.n):
                for c in n.get_connectables():
                    n.costs_cst[i].lhs.add_pair(c.costs.get(i))
                    n.energies_cst[i].lhs.add_pair(c.energies.get(i))
                    n.virtual_costs_cst[i].lhs.add_pair(c.virtual_costs.get(i))
                    
            self.constraints.extend(((n.energies == pleiad.quantity.Energy(Wh=0.0)) % ["Equilibrium(t_" + str(t) + "," + str(n.name) + ")" for t in range(self.ts.n)]).flatten())
        
        # Equilibrium in exchange nodes
        self.cEq = []
        for en in self.localnetwork.get_exchange_nodes():
            ceq_en = []
            for t in range(self.ts.n):
                ceq_en.append((pleiad.formal.LeftHandSide() == pleiad.quantity.Energy(Wh=0.0)) % ("Equilibrium(t_" + str(t) + "," + str(en.name) + ")"))
            for enc in en.connectables:
                for task in enc.tasks:
                    mtask = self.mt(task)
                    if mtask is not None:
                        for i in range(self.ts.n):
                            ceq_en[i].lhs.add_pair(mtask.variables.energies.get(i))
            self.cEq.extend(ceq_en)
        self.constraints += self.cEq
        
    def define_objective(self, obj):
        cst = (-pleiad.formal.Array(self.vCost) + obj == pleiad.quantity.Price(0)) % "DefinitionOfObjective"
        self.constraints += cst.flatten()
    
    def assemble_results(self):
        for s in self.localnetwork.get_all_nodes_and_connectables():
            for t in range(self.ts.n):
                self.results.loc[self.ts.idx_to_time(t),(s.name,"energy_drawn")] = s.energies.get(t).value
                self.results.loc[self.ts.idx_to_time(t),(s.name,"cost")] = s.costs.get(t).value
                self.results.loc[self.ts.idx_to_time(t),(s.name,"virtual_cost")] = s.virtual_costs.get(t).value
                  
            if isinstance(s, pleiad.physicalobject.Storage):
                for i in range(self.ts.n + 1):
                    for t in s.tasks:
                        try:
                            self.results.loc[self.ts.idx_to_time(i),(s.name,"Energy_stored_beginning")] = Model.mt(t).variable_stored_energy(i).value
                        except:
                            pass
    
    @staticmethod
    def mt(connectabletask):
        """
        This function returns the modeltask associated to a connectabletask
        """
        if connectabletask.id in SchedulerTask.association_connectabletask.keys():
            return SchedulerTask.association_connectabletask[connectabletask.id]
        else:
            return None