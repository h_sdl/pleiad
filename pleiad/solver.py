# -*- coding: utf-8 -*-
"""
Created on Mon May  6 09:54:20 2019

@author: Hugues Souchard de Lavoreille
"""

import numpy as np

class Solver:  
    
    @classmethod
    def run(cls, model, verbose=None):
        if model.solved:
            raise Exception("Model has already been solved")
        x = cls.solve(model=model, verbose=verbose)
        model.solved = True
        return x
    
class CplexSolver(Solver):
    
    @classmethod
    def solve(cls, model, verbose=None):
        import cplex
        
        prob = cplex.Cplex()
        prob.objective.set_sense(prob.objective.sense.minimize)
        
        obj = []
        ub = []
        lb = []
        ctype = []
        varnames = []
        dictvarnames = {}
        for j in range(len(model.variables)):
            maxi = model.variables[j].get_upper_bound_numeric()
            mini = model.variables[j].get_lower_bound_numeric()
            ub.append(maxi if maxi is not None else cplex.infinity)
            lb.append(mini if mini is not None else -cplex.infinity)
            ctype.append(model.variables[j].integer_or_continuous)
            varnames.append(model.variables[j].name)
            dictvarnames[model.variables[j].name] = j
            if model.variables[j].value is not None:
                model.constraints.append((model.variables[j] == model.variables[j].value) % (model.variables[j].name + "_value"))
        obj += [1] + [0] * (len(ub) - 1)
        prob.variables.add(obj=obj, lb=lb, ub=ub, types=ctype, names=varnames)
        
        senses = []
        rhs = []
        cnames = []
        dictcnames = {}
        rows = []
        cols = []
        vals = []
        for j in range(len(model.constraints)):
            senses.append(model.constraints[j].constraint_type)
            rhs.append(model.constraints[j].get_rhs_numeric())
            cnames.append(model.constraints[j].name)
            dictcnames[model.constraints[j].name] = j
            for p in model.constraints[j].lhs.pairs:
                rows.append(j)
                cols.append(dictvarnames[p.variable.name])
                vals.append(p.get_coefficient_numeric())
        
        prob.linear_constraints.add(rhs=rhs, senses=senses, names=cnames)
        prob.linear_constraints.set_coefficients(zip(rows, cols, vals))
        if verbose is not None: prob.write(verbose)
        prob.solve()        
        
        for i in range(len(model.variables)):
            tmp = prob.solution.get_values()[i]
            if abs(tmp) < 1e-6:
                tmp = tmp * tmp # In order to avoid keeping negative zeros -0.0
            model.variables[i].set_value(tmp)
        
        model.solved = True
        return model.variables[0]
        
class GurobiSolver(Solver):
    
    @classmethod
    def solve(cls, model, verbose=None):
        import gurobipy as grb
        
        m = grb.Model("pleiad")
        
        gvars = np.empty(len(model.variables), dtype=object)
        dictvarnames = {}
        
        for j in range(len(model.variables)):
            v = model.variables[j]
            params = {}
            params['vtype'] = grb.GRB.INTEGER if v.integer_or_continuous == 'I' else grb.GRB.CONTINUOUS
            params['name'] = v.name
            params['ub'] = v.get_upper_bound_numeric() if v.get_upper_bound_numeric() is not None else grb.GRB.INFINITY
            params['lb'] = v.get_lower_bound_numeric() if v.get_lower_bound_numeric() is not None else -grb.GRB.INFINITY
            gvars[j] = m.addVar(**params)
            dictvarnames[model.variables[j].name] = j
            if model.variables[j].value is not None:
                model.constraints.append((model.variables[j] == model.variables[j].value) % (model.variables[j].name + "_value"))
            
        for j in range(len(model.constraints)):
            c = model.constraints[j]
            params = {}
            params['sense'] = grb.GRB.EQUAL if c.constraint_type == "E" else (grb.GRB.LESS_EQUAL if c.constraint_type == "L" else grb.GRB.GREATER_EQUAL)
            params['rhs'] = c.get_rhs_numeric()
            params['name'] = c.name
            params['lhs'] = grb.quicksum(p.get_coefficient_numeric() * gvars[dictvarnames[p.variable.name]] for p in c.lhs.pairs)
            m.addConstr(**params)
        
        m.setObjective(gvars[0], grb.GRB.MINIMIZE)
        m.update()
        m.optimize()
        
        if verbose is not None:
            m.write(verbose)
        
        if m.Status != 2:
            raise Exception('No solution')
        
        for v in m.getVars():
            model.variables[dictvarnames[v.varName]].set_value(v.x)
        
        return model.variables[0]