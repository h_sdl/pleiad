#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 23 15:26:09 2019

@author: Hugues Souchard de Lavoreille
"""

import pleiad.quantity
from datetime import timedelta

class TestQuantity:
    
    def test_addition_quantity(self):
        e = pleiad.quantity.Energy(J=1800)
        e2 = pleiad.quantity.Energy(kWh=0.001)
        assert e+e2 == pleiad.quantity.Energy(Wh=1.5)
        assert e-e2 == pleiad.quantity.Energy(Wh=-0.5)
    
    def test_scalar_mult_quantity(self):
        e = pleiad.quantity.Energy(J=5)
        e2 = pleiad.quantity.Energy(J=35)
        assert 7 * e == e2
        assert e * 7 == e2
    
    def test_scalar_div_quantity(self):
        e = pleiad.quantity.Energy(J=5)
        e2 = pleiad.quantity.Energy(J=35)
        e3 = pleiad.quantity.Energy(J=6)
        assert e2 / 7 == e
        assert e2 % e3 == e
        assert e2 // e3 == 5
        assert e2 / e == 7
        
    def test_comparison_quantity(self):
        e1 = pleiad.quantity.Energy(J=1800)
        e2 = pleiad.quantity.Energy(kWh=0.001)
        assert e1 < e2
        assert e2 > e1
        assert e1 <= e2
        assert e2 >= e1
        assert e1 == e1
        assert e1 <= e1
        assert e1 >= e1
    
    def test_neg_quantity(self):
        e1 = pleiad.quantity.Energy(J=1800)
        e2 = pleiad.quantity.Energy(J=-1800)
        
        assert -e1 == e2
        assert -e2 == e1
    
    def test_abs_quantity(self):
        e1 = pleiad.quantity.Energy(J=1800)
        e2 = pleiad.quantity.Energy(J=-1800)
        
        assert abs(e1) == e1
        assert abs(e2) == e1
    
    def test_energy(self):
        e = pleiad.quantity.Energy(J=3600000)
        assert e.J == 3600000
        assert e.Wh == 1000
        assert e.kWh == 1
        
    def test_power(self):
        p = pleiad.quantity.Power(W=1000)
        assert p.W == 1000
        assert p.kW == 1
        
    def test_energyprice(self):
        pr = pleiad.quantity.EnergyPrice(DpMWh=1)
        assert pr.DpMWh == 1
        
    def test_price(self):
        pr = pleiad.quantity.Price(D=10)
        assert pr.D == 10
        
    def test_energy_to_power(self):
        e = pleiad.quantity.Energy(J=7200)
        td = timedelta(hours=2)
        assert e/td == pleiad.quantity.Power(W=1)
        
    def test_power_to_energy(self):
        p = pleiad.quantity.Power(W=5.2)
        td = timedelta(hours=2)
        e = pleiad.quantity.Energy(Wh=10.4)
        assert p * td == e
        assert td * p == e
        
    def test_energy_to_timedelta(self):
        e = pleiad.quantity.Energy(Wh=2)
        p = pleiad.quantity.Power(W=5)
        td = timedelta(hours=0.4)
        assert e/p == td