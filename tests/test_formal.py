# -*- coding: utf-8 -*-

import pytest
import pleiad.formal
import pleiad.quantity

class TestFormal:
    
    def test_variable_create(self):
        v = pleiad.formal.Variable("a", "I")
        v = pleiad.formal.Variable("b", "C")
        v = pleiad.formal.Variable("a2", integer_or_continuous="I", quantity_type=float)
        v = pleiad.formal.Variable("a2", integer_or_continuous="I", quantity_type=int)
        v = pleiad.formal.Variable("a2", integer_or_continuous="C", quantity_type=pleiad.quantity.Energy)
        
        assert v.value is None
        
        with pytest.raises(Exception):
            v = pleiad.formal.Variable("c", "E")
        
        v = pleiad.formal.Variable("d", "C", quantity_type=pleiad.quantity.Energy, maxi=pleiad.quantity.Energy(kWh=2), mini=pleiad.quantity.Energy(kWh=1))
        assert v.mini.kWh == 1
        assert v.maxi.kWh == 2
        assert v.get_lower_bound_numeric() == 1000
        assert v.get_upper_bound_numeric() == 2000
        
        v.set_value(1500)
        
        assert v.value.kWh == 1.5
        
        with pytest.raises(Exception):
            v.set_value(5000)
            
    def test_variable_operations(self):
        v = pleiad.formal.Variable("a", "C")
        v2 = pleiad.formal.Variable("a", "C")
        v3 = pleiad.formal.Variable("a", "C", quantity_type=int)
        v4 = pleiad.formal.Variable("a", "C", quantity_type=pleiad.quantity.Energy)
        
        b = 1 * v
        b2 = 2 * v2
        b3 = 3 * v3
        b4 = 4 * v4
        
        assert (b + b2).coefficients == [1, 2]
        assert (b + b3).coefficients == [1, 3]
        
        with pytest.raises(Exception):
            b + b4
            
        assert (-v).coefficient == -1
        assert -(1*v).coefficient == -1
        
        assert (+v).coefficient == +1 and id((+v).variable) == id(v)
        assert (v + v).coefficients == [1, 1]
        assert (v - v).coefficients == [1, -1]
        assert id((v == 4).lhs.variables[0]) == id(v)
        
    def test_coefficientvariablepair(self):
        v = pleiad.formal.Variable("E", "C", quantity_type=pleiad.quantity.Energy)
        p = pleiad.quantity.EnergyPrice(DpMWh=0.1) * v
        c = pleiad.formal.Variable("C", "C", quantity_type=pleiad.quantity.Price)
        
        with pytest.raises(Exception):
            p = pleiad.quantity.Power(W=0.1) * v
            
        assert (p + c).coefficients == [pleiad.quantity.EnergyPrice(DpMWh=0.1), 1]
            
    