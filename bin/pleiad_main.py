#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 15:42:00 2019

@author: Hugues Souchard de Lavoreille
"""
import math
import json
import datetime

import pleiad.structure
import pleiad.physicalobject
import pleiad.basescheduler
import pleiad.timedivision
import pleiad.quantity
import pleiad.solver
import pleiad.results
import pleiad.utils
import pleiad.samples


# ---
# Discretization of time
# ---
ts = pleiad.timedivision.TimeDivision("2017-01-01T00:00/2017-01-02T10:00/15m")


def forecast(time: datetime.datetime) -> pleiad.quantity.Power:
    """
    returns a GHI forecast for the given time
    """
    if not isinstance(time, datetime.datetime):
        raise Exception()

    clear_sky = None
    if time.hour < 7 or time.hour > 19:
        clear_sky = pleiad.quantity.Power(0)
    else:
        clear_sky = pleiad.quantity.Power(W=math.exp(-((time.hour + time.minute/60-13)/4) ** 2) * 1200)

    # Météo
    if time.hour < 7:
        mto = 0.5
    elif time.hour < 9:
        mto = 0.9
    elif time.hour < 11:
        mto = 0.5
    elif time.hour < 13:
        mto = 0.7
    elif time.hour < 17:
        mto = 0.5
    elif time.hour < 20:
        mto = 0.1
    else:
        mto = 0.1

    return clear_sky * mto

BUILDING = pleiad.structure.ConnectableNode("Building")
DWELLING = pleiad.structure.ConnectableNode("Dwelling")

NODE = pleiad.structure.ExchangeNode(name="B<->D", price_structure=pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(cpkWh=10)))
NODE.add(BUILDING, DWELLING)

m = pleiad.structure.LocalNetwork()
m.nodes = [DWELLING, BUILDING]
m.exchange_nodes = [NODE]

# Type A appliances
with open("../example/space_heater.json", "r") as jsonfile:
    SHschedules = json.load(jsonfile)

SH = pleiad.physicalobject.ThermalAppliance("Space_heater") \
        .add_task(SHschedules).set_preferred("hotfirst,0.01Dph")

## Type B appliances
DW = pleiad.samples.appliance.gen_dishwasher(ts.__str_time_range__())
WMD = pleiad.samples.appliance.gen_washingmachine_dryer(ts.__str_time_range__())
TV = pleiad.physicalobject.ActivityBasedAppliance("TV") \
        .add_task("[2017-01-01T19:00/2017-01-01T21:00]2h@0.1kW")
OVEN = pleiad.samples.appliance.gen_oven(ts.__str_time_range__())
FRIDGE = pleiad.samples.appliance.gen_fridge(ts.__str_time_range__())
STOVE = pleiad.samples.appliance.gen_stove(ts.__str_time_range__())

# Plaques
# Hotte
# Eclairage
# TV
# PC
# Clim
# Ventilation sdb
# Space heater
# Appareils bonus : mixeur, ..., console, lampe halogène, ...
# EV battery...

# Type C appliances
GH = pleiad.samples.appliance.gen_garageheater(ts.__str_time_range__())
WH = pleiad.samples.appliance.gen_waterheater(ts.__str_time_range__())

# Battery + Grid + PV
PS = pleiad.physicalobject.PowerSource("Grid", price_structure_purchases=pleiad.samples.pricestructure.EDFConstant, price_structure_sales=pleiad.samples.pricestructure.EDFAutoSales)
#PS2 = pleiad.physicalobject.PowerSource("Grid2", price_structure_purchase=pleiad.utils.TLOUPPriceStructure(tloupprice, pleiad.quantity.Power(kW=3)), price_structure_sales=None)
PV = pleiad.physicalobject.PowerSource("Photovoltaic", amount=forecast)
BAT = pleiad.samples.appliance.PowerWall()

EVBATT = pleiad.physicalobject.Storage("EV_battery", \
        e_max=pleiad.quantity.Energy(kWh=40), \
        p_max=pleiad.quantity.Power(kW=8)).add_task(time_range="2017-01-01T17:00/2017-01-02T08:00", \
            cycle_limit=2, \
            soc_constraints=["2017-01-01T17:00=15%", "2017-01-01T18:00/2017-01-01T20:00>10%", "2017-01-02T08:00>70%", "2017-01-01T20:00/2017-01-01T21:00>30%"], \
            same_soc_start_end=False)

# ---
# Associating all the connectable objects together
# ---
#DWELLING.add(SH)
DWELLING.add(DW)
#DWELLING.add(WMD)
DWELLING.add(FRIDGE)
#DWELLING.add(PS)
#DWELLING.add(TV)
DWELLING.add(OVEN)
DWELLING.add(STOVE)
DWELLING.add(GH)
DWELLING.add(WH)
#DWELLING.add(EVBATT)

#BUILDING.add(DWELLING)
BUILDING.add(PV)
#BUILDING.add(PS2)
BUILDING.add(PS)
BUILDING.add(BAT)


# ---
# Load a snapshot if needed
# ---
#with open("../example/instantane.json", "r") as jsonfile:
#    origin_snapshot = json.load(jsonfile)
#    b.load_snapshot(origin_snapshot)

# ---
# Adding the building and time discretization to create a model
# And then generate all the variables and constraints
# ---
sc = pleiad.basescheduler.Model(ts, m)
sc.create_model()
sc.define_objective(BUILDING.costs.sum() + DWELLING.costs.sum())

# ---
# Solve using Gurobi or Cplex
# ---
OBJ = pleiad.solver.GurobiSolver.run(sc, verbose="test.lp")
sc.assemble_results()
TOTCOST = round(OBJ.value, 6)

# Taking a snapshot and saving it
#sn = b.take_snapshot("2017-01-01T09:00", sc)
#f=open("../example/instantane.json", "w")
#json.dump(sn, f, indent="  ")
#f.close()

# %% Display

#r = sc.results
#pleiad.results.plotting.initialize(sc)
#pleiad.results.plotting.price_chart((PS,))
#pleiad.results.plotting.battery_price_chart((BAT,), (PS,))
#pleiad.results.plotting.power_schedule_chart((PS, d, PV, BAT))
#pleiad.results.plotting.bigplot((PS, d, BAT), (PS, BAT, d), (BAT,), (PS,))

# %% Tk
pleiad.results.plotting.initialize(sc)
CONFIG = pleiad.results.guiplotting.PlotConfigurator()
CONFIG.render()
