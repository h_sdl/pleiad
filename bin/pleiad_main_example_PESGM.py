#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 15:42:00 2019

@author: Hugues Souchard de Lavoreille
"""

import pleiad.results
import pleiad.timedivision
import pleiad.structure
import pleiad.library
import pleiad.basescheduler
import pleiad.solver
import pleiad.utils
import pleiad.quantity


# ---
# Step 1
# Defining time parameters
# September 1st at midnight to September 4th at 10am
# Time step (resolution) is 15 minutes
# ---
ts = pleiad.timedivision.TimeDivision("2020-11-12T00:00/2020-11-15T00:00/15m")


# ---
# Step 2
# Defining our network
# ---

# Two nodes : a building and a dwelling
aggregator = pleiad.structure.ConnectableNode("A")
dwelling_1 = pleiad.structure.ConnectableNode("D1")
dwelling_2 = pleiad.structure.ConnectableNode("D2")

# They can exchange electricity at fixed price 0.1$/kWh
exchange_node = pleiad.structure.ExchangeNode(name="XN", price_structure=pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(cUSDpkWh=10)))
exchange_node.add(aggregator)
exchange_node.add(dwelling_1)
exchange_node.add(dwelling_2)

# We assemble everything in a LocalNetwork object
m = pleiad.structure.LocalNetwork(nodes=(dwelling_1, dwelling_2, aggregator), exchange_nodes = (exchange_node,))

# To finish with, we create connectables ...
#SH1 = pleiad.library.appliance.gen_space_heater(ts)
SH1 = pleiad.library.appliance.gen_flexible_space_heater(ts)
#SH2 = pleiad.library.appliance.gen_space_heater(ts)
SH2 = pleiad.library.appliance.gen_flexible_space_heater(ts)
GH = pleiad.library.appliance.gen_garageheater(ts)
WH1 = pleiad.library.appliance.gen_waterheater(ts)
WH2 = pleiad.library.appliance.gen_waterheater(ts)
DW1 = pleiad.library.appliance.gen_dishwasher(ts)
DW2 = pleiad.library.appliance.gen_dishwasher(ts)
TV = pleiad.library.appliance.gen_tv(ts)
OVEN = pleiad.library.appliance.gen_oven(ts)
FRIDGE = pleiad.library.appliance.gen_fridge(ts)
STOVE = pleiad.library.appliance.gen_stove(ts)

# GRID = pleiad.library.grid.gen_purchaseonly_grid()
# GRID = pleiad.library.grid.gen_ontario_grid()
GRID = pleiad.library.grid.gen_ontario_grid_modified()
# GRID = pleiad.library.grid.gen_quebec_grid()

PV = pleiad.library.pv.gen_pv_pvlib(ts)
BATT = pleiad.library.battery.PowerWall()

# ... and we associate all the connectable objects to nodes
dwelling_1.add(SH1)
dwelling_1.add(DW1)
dwelling_1.add(WH1)
dwelling_1.add(GH)
dwelling_1.add(TV)

dwelling_2.add(SH2)
dwelling_2.add(WH2)
dwelling_2.add(DW2)
dwelling_2.add(FRIDGE)
dwelling_2.add(OVEN)
dwelling_2.add(STOVE)

aggregator.add(PV)
aggregator.add(GRID)
aggregator.add(BATT)

# ---
# Adding the building and time division to create a model
# And then generate all the variables and constraints
# ---
scheduler = pleiad.basescheduler.Model(ts, m)
scheduler.create_model()
scheduler.define_objective(dwelling_1.virtual_costs.sum() + dwelling_2.virtual_costs.sum() + aggregator.virtual_costs.sum() + dwelling_1.costs.sum() + dwelling_2.costs.sum() + aggregator.costs.sum())

# ---
# Solve using Gurobi or Cplex
# ---
obj = pleiad.solver.GurobiSolver.run(scheduler, verbose="test.lp")
scheduler.assemble_results() # Write results
totcost = round(obj.value, 6) # Total cost after minimization


# %% Analysis
curve = scheduler.results.loc[:, ("Grid#1", "energy_drawn")]
mean = curve.sum() / len(curve)
mini = min(curve)
print("Peak/average : " + str(round(mini / mean, 2)))


ecost = scheduler.results.loc[:, ("Grid#1", "cost")]
print("Total cost : " + str(round(ecost.sum(), 2)))

idx_off = ((curve.index.hour < 7) | (curve.index.hour > 19) | (curve.index.weekday == 5) | (curve.index.weekday == 6))
idx_on = (curve.index.hour >= 11) & (curve.index.hour <= 17) & ~idx_off
print("Consumed off-peak : " + str(round(-curve[idx_off].sum(), 2)))
print("Consumed mid-peak : " + str(round(-curve[~(idx_on | idx_off)].sum(), 2)))
print("Consumed peak : " + str(round(-curve[idx_on].sum(), 2)))

print("Total : " + str(round(-curve.sum(), 2)))

#for c in m.get_all_connectables():
#    print(c.name + " : " + str(round(c.energies.sum().build()[0][0].value, 2)))

res = scheduler.results

print("\n-- AGG")
res2 = res.loc[:, [i.name for i in aggregator.get_connectables()]].xs("energy_drawn", axis = 1, level = 1)
print(res2.sum(axis = 0).apply(lambda l: round(l, 2)), round(res2.sum().sum(), 2))

print("\n-- D1")
res2 = res.loc[:, [i.name for i in dwelling_1.get_connectables()]].xs("energy_drawn", axis = 1, level = 1)
print(res2.sum(axis = 0).apply(lambda l: round(l, 2)), round(res2.sum().sum(), 2))

print("\n-- D2")
res2 = res.loc[:, [i.name for i in dwelling_2.get_connectables()]].xs("energy_drawn", axis = 1, level = 1)
print(res2.sum(axis = 0).apply(lambda l: round(l, 2)), round(res2.sum().sum(), 2))



# %% Tk
#pleiad.results.plotting.initialize(scheduler)
#config = pleiad.results.guiplotting.PlotConfigurator()
#config.render()
