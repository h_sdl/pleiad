#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 15:42:00 2019

@author: Hugues Souchard de Lavoreille
"""

import pleiad.results
import pleiad.timedivision
import pleiad.structure
import pleiad.library
import pleiad.basescheduler
import pleiad.solver
import pleiad.utils
import pleiad.quantity


# ---
# Step 1
# Defining time parameters
# September 1st at midnight to September 4th at 10am
# Time step (resolution) is 15 minutes
# ---
ts = pleiad.timedivision.TimeDivision("2019-09-12T00:00/2019-09-15T00:00/15m")


# ---
# Step 2
# Defining our network
# ---

# Two nodes : a building and a dwelling
building = pleiad.structure.ConnectableNode("Building")
dwelling = pleiad.structure.ConnectableNode("Dwelling")

# They can exchange electricity at fixed price 0.1$/kWh
exchange_node = pleiad.structure.ExchangeNode(name="B<->D", price_structure=pleiad.utils.ConstantPriceStructure(pleiad.quantity.EnergyPrice(cUSDpkWh=10)))
exchange_node.add(building)
exchange_node.add(dwelling)

# We assemble everything in a LocalNetwork object
m = pleiad.structure.LocalNetwork(nodes=(dwelling, building), exchange_nodes = (exchange_node,))

# To finish with, we create connectables ...
SH = pleiad.library.appliance.gen_space_heater(ts)
GH = pleiad.library.appliance.gen_garageheater(ts)
WH = pleiad.library.appliance.gen_waterheater(ts)
DW = pleiad.library.appliance.gen_dishwasher(ts)
TV = pleiad.library.appliance.gen_tv(ts)
OVEN = pleiad.library.appliance.gen_oven(ts)
FRIDGE = pleiad.library.appliance.gen_fridge(ts)
STOVE = pleiad.library.appliance.gen_stove(ts)

GRID = pleiad.library.grid.gen_ontario_grid()

PV = pleiad.library.pv.gen_pv_pvlib(ts)

BATT = pleiad.library.battery.PowerWall()
EVBATT = pleiad.library.battery.gen_ev_battery(ts)

# ... and we associate all the connectable objects to nodes
dwelling.add(SH)
dwelling.add(DW)
dwelling.add(FRIDGE)
dwelling.add(TV)
dwelling.add(OVEN)
dwelling.add(STOVE)
dwelling.add(GH)
dwelling.add(WH)
dwelling.add(EVBATT)

building.add(PV)
building.add(GRID)
building.add(BATT)


# ---
# Load a snapshot if needed
# ---
#with open("../example/instantane.json", "r") as jsonfile:
#    origin_snapshot = json.load(jsonfile)
#    b.load_snapshot(origin_snapshot)

# ---
# Adding the building and time division to create a model
# And then generate all the variables and constraints
# ---
scheduler = pleiad.basescheduler.Model(ts, m)
scheduler.create_model()
scheduler.define_objective(building.costs.sum() + dwelling.costs.sum())

# ---
# Solve using Gurobi or Cplex
# ---
obj = pleiad.solver.GurobiSolver.run(scheduler, verbose="test.lp")
scheduler.assemble_results() # Write results
totcost = round(obj.value, 6) # Total cost after minimization

# Taking a snapshot and saving it
#sn = b.take_snapshot("2017-01-01T09:00", sc)
#f=open("../example/instantane.json", "w")
#json.dump(sn, f, indent="  ")
#f.close()

# %% Tk
pleiad.results.plotting.initialize(scheduler)
config = pleiad.results.guiplotting.PlotConfigurator()
config.render()
